import numpy as np
def get_relative_weight(uuu, grid, base):
  d = grid['d']
  ind = np.zeros(grid['ne'])
  nbas_pol = np.array([1, 4, 10, 20, 35])
  ntest = nbas_pol[base['k']-1]
  um = uuu[2:2+d,:,:]
  for ie in range(0,grid['ne']):  #cyce on elements
    small_e = (np.sum(np.power(um[0,ntest:,ie],2)) +
               np.sum(np.power(um[1,ntest:,ie],2)) +
               np.sum(np.power(um[2,ntest:,ie],2)))
    tot_e = (np.sum(np.power(um[0,1:,ie],2)) +
             np.sum(np.power(um[1,1:,ie],2)) +
             np.sum(np.power(um[2,1:,ie],2)))
    ind[ie] = np.sqrt(small_e/tot_e)
  return ind
  
def get_relative_weight_mean(uuu, grid, base):
  d = grid['d']
  ind = np.zeros(grid['ne'])
  nbas_pol = np.array([1, 4, 10, 20, 35])
  ntest = nbas_pol[base['k']-1]
  um = uuu[2:2+d,:,:]
  for ie in range(0,grid['ne']):  #cyce on elements
    small_e = (np.sum(np.power(um[0,ntest:,ie],2)) +
               np.sum(np.power(um[1,ntest:,ie],2)) +
               np.sum(np.power(um[2,ntest:,ie],2)))
    tot_e = (np.sum(np.power(um[0,:,ie],2)) +
             np.sum(np.power(um[1,:,ie],2)) +
             np.sum(np.power(um[2,:,ie],2)))
    ind[ie] = np.sqrt(small_e/tot_e)
  return ind
  
def get_inv_e(uuu, grid, base):
  d = grid['d']
  ind = np.zeros(grid['ne'])
  nbas_pol = np.array([1, 4, 10, 20, 35])
  ntest = nbas_pol[base['k']-1]
  um = uuu[2:2+d,:,:]
  for ie in range(0,grid['ne']):  #cyce on elements
    small_e = 1
    tot_e = (np.sum(np.power(um[0,:,ie],2)) +
             np.sum(np.power(um[1,:,ie],2)) +
             np.sum(np.power(um[2,:,ie],2)))
    ind[ie] = np.sqrt(small_e/tot_e)
  return ind
  