import numpy as np
def get_structure_function(uuu, grid, intmat):
  DD = np.zeros((3,3,6,grid['ne']))
  rr = np.zeros((3,6,grid['ne']))
  vertex_coord = np.array([[0.0, 0.0, 0.0, 1.0],
                           [0.0, 0.0, 1.0, 0.0],
                           [0.0, 1.0, 0.0, 0.0]])
  
  for ie in range(0,grid['ne']):  #cyce on elements
    u_g = np.dot(uuu[2:5,:,ie],intmat)
    rho_g = np.dot(uuu[0,:,ie],intmat)
    u_g[0,:] = u_g[0,:]/rho_g
    u_g[1,:] = u_g[1,:]/rho_g
    u_g[2,:] = u_g[2,:]/rho_g # I know it is horrible, but I don't want to think
                              # at another solution
    for ip in range(0,6):
      (DD[:,:,ip,ie],rr[:,ip,ie]) = structfun_on_elem(u_g, vertex_coord, grid['e'][ie]['b'], ip)
  
  
  return(DD, rr)
  
  
def structfun_on_elem(u_g, point_coord, B, combination):
  
  comb_table = np.array([[1,1,1,2,2,3],
                         [2,3,4,3,4,4]])-1 #to account for python indexing
  corr_nodes = point_coord[:,comb_table[:,combination]]
  corr_u = u_g[:,comb_table[:,combination]]
  udiff = corr_u[:,1]-corr_u[:,0]
  D = np.outer(udiff, udiff.T)
  r = np.dot(B,corr_nodes[:,1]-corr_nodes[:,0])
  return(D,r)

def ind_structfun_no_isotropy(DD, rr, ne):
  ind = np.zeros(ne)
  for ie in range(0,ne):

#    Q1 = structfun_no_isotropy(DD[:,:,0,ie],rr[:,0,ie])
#    Q2 = structfun_no_isotropy(DD[:,:,1,ie],rr[:,1,ie])
#    Q3 = structfun_no_isotropy(DD[:,:,2,ie],rr[:,2,ie])
#    Q4 = structfun_no_isotropy(DD[:,:,3,ie],rr[:,3,ie])
#    Q5 = structfun_no_isotropy(DD[:,:,4,ie],rr[:,4,ie])    
#    Q6 = structfun_no_isotropy(DD[:,:,5,ie],rr[:,5,ie])
#    ind[ie] = (Q1+Q2+Q3+Q4+Q5+Q6)/6.0
    for ip in range(0,6):
      oneind = structfun_no_isotropy(DD[:,:,ip,ie],rr[:,ip,ie])
      ind[ie] = ind[ie]+oneind/6.0
  return ind
  
def structfun_no_isotropy(D,r):
  normR2 = np.power(r,2).sum()
  RiRj = np.outer(r,r.T)/normR2
  trD = D.trace()
  Dnn = (trD -(D*RiRj).sum())/2
  Dll = -2*Dnn +trD
  Q = np.power((Dnn*np.eye(3) + (Dll-Dnn)*RiRj -D),2).sum()
  return Q

