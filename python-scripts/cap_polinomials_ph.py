#first try in porting my matlab scripts into python
import numpy as np
#from read_octxt import read_var, read_all_vars
import scipy.io as sio
import h5py
#from structure_function import get_structure_function, ind_structfun_no_isotropy
#from relative_weight import get_relative_weight, get_relative_weight_mean, get_inv_e
#from firstmode import get_first_mode
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from check_mat_file import check_mat_file
import argparse

pars = argparse.ArgumentParser()
pars.add_argument("filename",help='Adaptivity file to be modified')
pars.add_argument("gridname",help='Output grid name w/o extension')
pars.add_argument("outname",help='New output name')
pars.add_argument('-s','--start', nargs='+', 
                     help='<Required> starting y over which apply cap', 
                     required=True)
pars.add_argument('-k','--deg', nargs='+', 
                     help='<Required> Max degree at which to cap', 
                     required=True)

args = pars.parse_args()
filename = args.filename
gridname = args.gridname
outname = args.outname
start = float(args.start[0])
k_min = int(args.deg[0])
#print(indname)
#filename = '/home/matteo/DG/SIMULATIONS/SQCYL/ref3/Output/ref3_1'
#res1 = 123
#res2 = 123
#k_base = 4
#indname = 'RW'


#fh = open(filename+'-grid.octxt', mode='r')
#grid = read_var(fh,'grid')
#fh.close()
check_mat_file(gridname)
griddat = sio.loadmat(gridname+'.mat',variable_names='grid',squeeze_me=True)
grid= griddat['grid'][()]
del griddat
position_elems = np.array(grid['e']['xb'].tolist())
ne = grid['ne']


#check_mat_file(filename+'-base')
#basedat = sio.loadmat(filename+'-base.mat',variable_names='base',squeeze_me=True)
#base= basedat['base']
#del basedat

#indicator = np.zeros(ne)
#DDmean = np.zeros((3,3,6,ne))
#dat = sio.loadmat('intmat_vertex.mat',squeeze_me=True)
#intmat_vert = dat['intmat']

#load the polynomial degree  from hdf5 file
h5file = h5py.File(filename,'r')
dset = h5file.get('edeg')
edeg = np.array(dset).T #transposed because it is loaded c-style from hdf5
del dset
dset = h5file.get('indicator')
indicator = np.array(dset).T #transposed because it is loaded c-style from hdf5
del dset
dset = h5file.get('tresholds')
tresolds = np.array(dset).T #transposed because it is loaded c-style from hdf5
del dset


h5file.close()





#thresholds


quitme = False

while not quitme:
  #cycle to cap
  print('k_min',k_min, 'start', start)
  
  edeg_new = edeg
  for ie in range(0,ne):
      if position_elems[ie,1] > start:
          if edeg[ie] > k_min:
              edeg_new[ie] = k_min
  
  #3dplot
  up = np.argwhere(edeg_new == 4)
  lo = np.argwhere(edeg_new == 2)
  me = np.argwhere(edeg_new == 3)
  fig = plt.figure()
  ax = fig.add_subplot(111, projection = '3d')
  #ax.scatter(position_elems[up, 0], position_elems[up, 1], position_elems[up, 2],
  #           s=4, linewidth=0, c = 'r', marker = '^',depthshade=False)
  #ax.scatter(position_elems[me, 0], position_elems[me, 1], position_elems[me, 2],
  #          s=4, linewidth=0, c = 'g', marker = '^',depthshade=False)
  #ax.scatter(position_elems[lo, 0], position_elems[lo, 1], position_elems[lo, 2],
  #          s=4, linewidth=0, c = 'b', marker = '^',depthshade=False)
  ax.scatter(position_elems[up, 0], position_elems[up, 1], position_elems[up, 2],
             s=4, linewidth=0, c = 'r', marker = '^')
  ax.scatter(position_elems[me, 0], position_elems[me, 1], position_elems[me, 2],
            s=4, linewidth=0, c = 'g', marker = '^')
  ax.scatter(position_elems[lo, 0], position_elems[lo, 1], position_elems[lo, 2],
            s=4, linewidth=0, c = 'b', marker = '^')
  plt.draw()
  plt.show(block=False)
  
  
  #generate the degree map
  
  dof_deg = np.array([4, 10, 20, 35, 56, 84, 120])
  full_dofs = dof_deg*ne
  elems_degs = np.zeros(7)
  elems_degs_old = np.zeros(7)
  for i in range(0,7):
    elems_degs[i] = (edeg_new == i+1).sum()
    elems_degs_old[i] = (edeg == i+1).sum()
    
  dofs_adapt = (elems_degs*dof_deg).sum()
  dofs_adapt_old = (elems_degs_old*dof_deg).sum()
  print('number of old degrees of freedom: ', dofs_adapt_old)
  print('number of new degrees of freedom: ', dofs_adapt)
  print('number of tetra at each degree, old:', elems_degs_old)
  print('number of tetra at each degree, new:', elems_degs)
  instring  = input('Insert the new start level, q to quit, w to write to file: ')
  try:  
    start = float(instring)
    instring  = input('Insert the new min k: ') 
    k_min = int(instring)
  except ValueError:
    quitme = True

#Output
if instring == 'w':
  print('Writing to file')
  #outfilename = filename+'python_test.h5'
  outfilename = outname+'.h5'
  h5file = h5py.File(outfilename,'w')
  dset = h5file.create_dataset('indicator',data=indicator)
  dset = h5file.create_dataset('edeg',data=edeg_new,dtype='i')
  dset = h5file.create_dataset('tresholds',data=tresolds)
  h5file.close()
