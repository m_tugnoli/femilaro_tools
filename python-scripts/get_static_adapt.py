#first try in porting my matlab scripts into python
import numpy as np
#from read_octxt import read_var, read_all_vars
import scipy.io as sio
import h5py
from structure_function import get_structure_function, ind_structfun_no_isotropy
from relative_weight import get_relative_weight, get_relative_weight_mean, get_inv_e
from firstmode import get_first_mode
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from check_mat_file import check_mat_file
import argparse

pars = argparse.ArgumentParser()
pars.add_argument("filename",help='Results basename')
pars.add_argument("outname",help='output appendix')
pars.add_argument('-s','--start', nargs='+', 
                     help='<Required> starting result', required=True)
pars.add_argument('-e','--end', nargs='+', 
                     help='<Required> end result', required=True)
pars.add_argument('-k','--deg', nargs='+', 
                     help='<Required> base degree', required=True)
pars.add_argument('-i','--ind', nargs='+', 
                     help='<Required> indicator', required=True)

args = pars.parse_args()
filename = args.filename
res1=int(args.start[0])
res2=int(args.end[0])
k_base = int(args.deg[0])
indname = args.ind[0]
#print(indname)
#filename = '/home/matteo/DG/SIMULATIONS/SQCYL/ref3/Output/ref3_1'
#res1 = 123
#res2 = 123
#k_base = 4
#indname = 'RW'


#fh = open(filename+'-grid.octxt', mode='r')
#grid = read_var(fh,'grid')
#fh.close()
check_mat_file(filename+'-grid')
griddat = sio.loadmat(filename+'-grid.mat',variable_names='grid',squeeze_me=True)
grid= griddat['grid'][()]
del griddat
position_elems = np.array(grid['e']['xb'].tolist())
ne = grid['ne']
check_mat_file(filename+'-base')
basedat = sio.loadmat(filename+'-base.mat',variable_names='base',squeeze_me=True)
base= basedat['base']
del basedat
indicator = np.zeros(ne)
DDmean = np.zeros((3,3,6,ne))
dat = sio.loadmat('intmat_vertex.mat',squeeze_me=True)
intmat_vert = dat['intmat']

#Results cycle
for res in range(res1,res2+1):
  #load the results from hdf5 file
  res_filename = filename+'-res-'+str(res).zfill(4)+'.h5'
  h5file = h5py.File(res_filename,'r')
  dset = h5file.get('uuu')
  uuu = np.array(dset).T #transposed because it is loaded c-style from hdf5
  del dset
  h5file.close()
  if indname == 'SF':
     #calculate the structure function
    (DD,rr) = get_structure_function(uuu, grid, intmat_vert[k_base][0])
    DDmean = DDmean + DD
  elif indname == 'RW':
    indicator = indicator + get_relative_weight(uuu, grid, base)
  elif indname == 'RWmean':
    indicator = indicator + get_relative_weight_mean(uuu, grid, base)
  elif indname == 'inve':
    indicator = indicator + get_inv_e(uuu, grid, base)
  elif indname == 'firstmode':
    indicator = indicator + get_first_mode(uuu, grid, base)
  else:
    raise RuntimeError('Indicator not known')
# post cycle calculations
if indname == 'SF':
  #calculate and remove anisotropy
  DDmean = DDmean/(res2-res1+1)
  indicator = ind_structfun_no_isotropy(DDmean, rr, ne)
elif indname == 'RW' or indname == 'RWmean' or indname == 'inve' or indname == 'firstmode':
  indicator = indicator/(res2-res1+1)
else:
  raise RuntimeError('Indicator not known')   


#thresholds
lo_trs = 4.7e-4
hi_trs = 1e-2

lo_string  = input('Insert the lower treshold: ') 
lo_trs = float(lo_string)
hi_string  = input('Insert the higher treshold: ') 
hi_trs = float(hi_string)

quitme = False

while not quitme:
  
  #3dplot
  up = np.argwhere(indicator >= hi_trs)
  lo = np.argwhere(indicator <= lo_trs)
  me = np.argwhere(np.logical_and(indicator < hi_trs, indicator > lo_trs))
  fig = plt.figure()
  ax = fig.add_subplot(111, projection = '3d')
  #ax.scatter(position_elems[up, 0], position_elems[up, 1], position_elems[up, 2],
  #           s=4, linewidth=0, c = 'r', marker = '^',depthshade=False)
  #ax.scatter(position_elems[me, 0], position_elems[me, 1], position_elems[me, 2],
  #          s=4, linewidth=0, c = 'g', marker = '^',depthshade=False)
  #ax.scatter(position_elems[lo, 0], position_elems[lo, 1], position_elems[lo, 2],
  #          s=4, linewidth=0, c = 'b', marker = '^',depthshade=False)
  ax.scatter(position_elems[up, 0], position_elems[up, 1], position_elems[up, 2],
             s=4, linewidth=0, c = 'r', marker = '^')
  ax.scatter(position_elems[me, 0], position_elems[me, 1], position_elems[me, 2],
            s=4, linewidth=0, c = 'g', marker = '^')
  ax.scatter(position_elems[lo, 0], position_elems[lo, 1], position_elems[lo, 2],
            s=4, linewidth=0, c = 'b', marker = '^')
  plt.draw()
  plt.show(block=False)
  
  
  #generate the degree map
  deg_lim = np.ones(ne)*3
  deg_lim[indicator<lo_trs] = 2
  deg_lim[indicator>hi_trs] = 4
  
  dof_deg = np.array([4, 10, 20, 35, 56, 84, 120])
  full_dofs = dof_deg*ne
  elems_degs = np.zeros(7)
  for i in range(0,7):
    elems_degs[i] = (deg_lim == i+1).sum()
    
  dofs_adapt = (elems_degs*dof_deg).sum()
  print('number of degrees of freedom: ', dofs_adapt)
  print('number of tetra at each degree:', elems_degs)
  instring  = input('Insert the lower treshold, q to quit, w to write to file: ')
  try:  
    lo_trs = float(instring)
    instring  = input('Insert the higher treshold: ') 
    hi_trs = float(instring)
  except ValueError:
    quitme = True

#Output
if instring == 'w':
  print('Writing to file')
  #outfilename = filename+'python_test.h5'
  outfilename = filename+args.outname+'.h5'
  h5file = h5py.File(outfilename,'w')
  dset = h5file.create_dataset('indicator',data=indicator)
  dset = h5file.create_dataset('edeg',data=deg_lim,dtype='i')
  dset = h5file.create_dataset('tresholds',data=[lo_trs, hi_trs])
  h5file.close()
