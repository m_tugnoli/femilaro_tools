import numpy as np
def get_first_mode(uuu, grid, base):
  d = grid['d']
  ind = np.zeros(grid['ne'])
  nbas_pol = np.array([1, 4, 10, 20, 35])
  ntest = nbas_pol[base['k']-1]
  um = uuu[2:2+d,:,:]
  for ie in range(0,grid['ne']):  #cyce on elements
    ind[ie] = (np.sum(np.power(um[0,2:5,ie],2)) +
               np.sum(np.power(um[1,2:5,ie],2)) +
               np.sum(np.power(um[2,2:5,ie],2)))
  return ind  
  
  
