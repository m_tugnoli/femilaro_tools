#With this script one can load a previous indicator file, load the 
#indicator value, change the tresholds and write again a new file 
#with the new polynomial degree formulation
import numpy as np
#from read_octxt import read_var, read_all_vars
import scipy.io as sio
import h5py
from structure_function import get_structure_function, ind_structfun_no_isotropy
from relative_weight import get_relative_weight, get_relative_weight_mean, get_inv_e
from firstmode import get_first_mode
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from check_mat_file import check_mat_file
import argparse

pars = argparse.ArgumentParser()
pars.add_argument("filename",help='Input filename')
pars.add_argument("outname",help='Output filename')

args = pars.parse_args()
filename = args.filename



print('Reading from file')
#outfilename = filename+'python_test.h5'
h5file = h5py.File(filename,'r')
ind_load = h5file['indicator'][:]
ne = len(ind_load)
indicator = np.asarray(ind_load)
#indicator = h5file['indicator'][:]
h5file.close()

#thresholds

lo_string  = input('Insert the lower treshold: ') 
lo_trs = float(lo_string)
hi_string  = input('Insert the higher treshold: ') 
hi_trs = float(hi_string)

quitme = False

while not quitme:
  
  #3dplot
  #up = np.argwhere(indicator >= hi_trs)
  #lo = np.argwhere(indicator <= lo_trs)
  #me = np.argwhere(np.logical_and(indicator < hi_trs, indicator > lo_trs))
  #fig = plt.figure()
  #ax = fig.add_subplot(111, projection = '3d')
  ##ax.scatter(position_elems[up, 0], position_elems[up, 1], position_elems[up, 2],
  ##           s=4, linewidth=0, c = 'r', marker = '^',depthshade=False)
  ##ax.scatter(position_elems[me, 0], position_elems[me, 1], position_elems[me, 2],
  ##          s=4, linewidth=0, c = 'g', marker = '^',depthshade=False)
  ##ax.scatter(position_elems[lo, 0], position_elems[lo, 1], position_elems[lo, 2],
  ##          s=4, linewidth=0, c = 'b', marker = '^',depthshade=False)
  #ax.scatter(position_elems[up, 0], position_elems[up, 1], position_elems[up, 2],
  #           s=4, linewidth=0, c = 'r', marker = '^')
  #ax.scatter(position_elems[me, 0], position_elems[me, 1], position_elems[me, 2],
  #          s=4, linewidth=0, c = 'g', marker = '^')
  #ax.scatter(position_elems[lo, 0], position_elems[lo, 1], position_elems[lo, 2],
  #          s=4, linewidth=0, c = 'b', marker = '^')
  #plt.draw()
  #plt.show(block=False)
  
  
  #generate the degree map
  deg_lim = np.ones(ne)*3
  deg_lim[indicator<lo_trs] = 2
  deg_lim[indicator>hi_trs] = 4
  
  dof_deg = np.array([4, 10, 20, 35, 56, 84, 120])
  full_dofs = dof_deg*ne
  elems_degs = np.zeros(7)
  for i in range(0,7):
    elems_degs[i] = (deg_lim == i+1).sum()
    
  dofs_adapt = (elems_degs*dof_deg).sum()
  print('number of degrees of freedom: ', dofs_adapt)
  print('number of tetra at each degree:', elems_degs)
  instring  = input('Insert the lower treshold, q to quit, w to write to file: ')
  try:  
    lo_trs = float(instring)
    instring  = input('Insert the higher treshold: ') 
    hi_trs = float(instring)
  except ValueError:
    quitme = True

#Output
if instring == 'w':
  print('Writing to file')
  #outfilename = filename+'python_test.h5'
  outfilename = filename+args.outname+'.h5'
  h5file = h5py.File(outfilename,'w')
  dset = h5file.create_dataset('indicator',data=indicator)
  dset = h5file.create_dataset('edeg',data=deg_lim,dtype='i')
  dset = h5file.create_dataset('tresholds',data=[lo_trs, hi_trs])
  h5file.close()
