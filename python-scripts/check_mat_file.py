import os
def check_mat_file(filename):
  filename_mat = filename+'.mat'
  if not os.path.isfile(filename_mat):
    filename_octxt = filename+'.octxt'
    if os.path.isfile(filename_octxt):
      #convert file
      print('Converting the octave file ',filename_octxt,'in matlab format to be loaded by python =)')
      os.system('octave -qf --eval "load('+"'"+filename_octxt+"'); save('-v7','"+filename_mat+"')"+'"')
    else:
      raise RuntimeError('octave file '+filename_octxt+' not present')