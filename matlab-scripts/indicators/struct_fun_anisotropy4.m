function [ Q ] = struct_fun_anisotropy4( D, r)
%STRUCT_FUN_ANISOTROPY4 Determine the norm of the structure function,
%subtracted the isotropic form. This version should held identical results
%than the struct_fun_anisotropy2 but is just written in a simpler way,
%exploting the fact that some of the older terms used are 1
%  Takes as input a structure function tensor D and the vector used to make
%  the correlation r and returns the quadratic error Q. r should be a
%  column vector

normR2 = norm(r)^2;
RiRj = (r *  r')/normR2;
trD = trace(D);

Dnn = ( trD - sum(sum(D.*RiRj)) )/2;
Dll = -2*Dnn + trD;

Q = sum(sum((Dnn*eye(3)+(Dll-Dnn)*RiRj-D).^2));


end

