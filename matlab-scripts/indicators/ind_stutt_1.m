function [ csi ] = ind_stutt_1( uuu, grid, base )
%function to calculate the indicator from the guys of stuttgart 

nbas_pol = [1 4 10 20 35];
ntest = nbas_pol(base.k); %this is the base deg, but since nbas_pol is indexed
                          %with i+1, it is actually the number of basis at 
                          %degree k-1
vel = uuu(3:2+grid.d,:,:);
%vel_2 = vel(:,end,:);    %taking only the last mode as "small scale"
%vel_1 = vel(:,1:end-1,:);

csi = zeros(1,grid.ne);


%operation "ignorance is strength" and pushing all to the gauss nodes
for ie = 1:grid.ne
  %on gauss nodes
  vel_2_g = vel(:,ntest+1:end, ie) * base.p(ntest+1:end,:);
  vel_1_g = vel(:,1:ntest,ie) * base.p(1:ntest,:);
  
  %         integral on volume  / volume     replicate on all gauss nodes
  vel_ave = (vel_1_g * base.wg')/sum(base.wg)*ones(1,length(base.wg));
  
  %     integral over volume of the square
  E_2 = sum(vel_2_g.^2 * base.wg');
  E_1 = sum((vel_1_g - vel_ave).^2 * base.wg');
  
  csi(ie) = E_2/E_1;
  %c_cell = ( (double(base.k)-1+1)^(-2/3)-2^(-2/3))/((double(base.k)+1)^(-2/3)-(double(base.k)-1+1)^(-2/3));
  c_cell = ( (double(base.k)-1+1)^(-2/3)-1^(-2/3))/((double(base.k)+1)^(-2/3)-(double(base.k)-1+1)^(-2/3));
  csi(ie) = csi(ie)*c_cell;
end





end


