function [DD,rr] = ind_struct_fun_3(uuu,grid,interpmat,base)
%modified verison to use vertex interpolated values
DD = zeros(3,3,6,grid.ne);
rr = zeros(3,6,grid.ne); %6 are the possible combinations

vertexes = [0.0 0.0 0.0 1.0
          0.0 0.0 1.0 0.0
          0.0 1.0 0.0 0.0];
        
        
for ie = 1:grid.ne
   u_g = uuu(3:5,:,ie)*interpmat;
   rho_g = (uuu(1,:,ie)*interpmat);
   u_g(1,:) = u_g(1,:)./rho_g;
   u_g(2,:) = u_g(2,:)./rho_g;
   u_g(3,:) = u_g(3,:)./rho_g;
   
%    [DD(:,:,1,ie),rr(:,1,ie)] = ...
%      structfun_on_element_2( u_g, base.xig, grid.e(ie).b, 1);
%    [DD(:,:,2,ie),rr(:,2,ie)] = ... 
%      structfun_on_element_2( u_g, base.xig, grid.e(ie).b, 2);
%    [DD(:,:,3,ie),rr(:,3,ie)] = ... 
%      structfun_on_element_2( u_g, base.xig, grid.e(ie).b, 3);
%    [DD(:,:,4,ie),rr(:,4,ie)] = ... 
%      structfun_on_element_2( u_g, base.xig, grid.e(ie).b, 4);
%    [DD(:,:,5,ie),rr(:,5,ie)] = ... 
%      structfun_on_element_2( u_g, base.xig, grid.e(ie).b, 5);
%    [DD(:,:,6,ie),rr(:,6,ie)] = ... 
%      structfun_on_element_2( u_g, base.xig, grid.e(ie).b, 6);
   for ip = 1:6
   [DD(:,:,ip,ie),rr(:,ip,ie)] = ...
     structfun_on_element_2( u_g, vertexes, grid.e(ie).b, ip);
   end
end
end
