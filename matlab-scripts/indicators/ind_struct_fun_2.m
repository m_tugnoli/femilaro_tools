function [DD,rr] = ind_struct_fun_2(uuu,grid,base,edeg)
%modified verison to operate on already adaptive results
DD = zeros(3,3,6,grid.ne);
rr = zeros(3,6,grid.ne); %6 are the possible combinations

for ie = 1:grid.ne
   u_g = uuu(3:5,:,ie)*base.p;
   rho_g = (uuu(1,:,ie)*base.p);
   u_g(1,:) = u_g(1,:)./rho_g;
   u_g(2,:) = u_g(2,:)./rho_g;
   u_g(3,:) = u_g(3,:)./rho_g;
   
   [DD(:,:,1,ie),rr(:,1,ie)] = ...
     structfun_on_element( u_g, base.xig, grid.e(ie).b, 1, edeg);
   [DD(:,:,2,ie),rr(:,2,ie)] = ... 
     structfun_on_element( u_g, base.xig, grid.e(ie).b, 2, edeg);
   [DD(:,:,3,ie),rr(:,3,ie)] = ... 
     structfun_on_element( u_g, base.xig, grid.e(ie).b, 3, edeg);
   [DD(:,:,4,ie),rr(:,4,ie)] = ... 
     structfun_on_element( u_g, base.xig, grid.e(ie).b, 4, edeg);
   [DD(:,:,5,ie),rr(:,5,ie)] = ... 
     structfun_on_element( u_g, base.xig, grid.e(ie).b, 5, edeg);
   [DD(:,:,6,ie),rr(:,6,ie)] = ... 
     structfun_on_element( u_g, base.xig, grid.e(ie).b, 6, edeg);
end
end
