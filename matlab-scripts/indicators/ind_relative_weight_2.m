function ind = ind_relative_weight_2(uu,grid,base)
%function to calculate a refinement indicator based on the relative weight
%of pseudo-energy contained at the different modes. The input is the modal
%solution at all elements and number of elements, and the output is the 
%scalar indicator at the elements. 
%At the moment the indicator is implemented using only the
%momentum, with the weight of the last mode with respect to the weight of 
%all the other modes
 u_m = uu(3:2+grid.d,:,:);
 ind = zeros(1,grid.ne);
 nbas_pol = [1 4 10 20 35];
 ntest = nbas_pol(base.k);
 for ie = 1:grid.ne
%     uxw=sqrt(sum(u_m(1,ntest+1:end,ie).^2)/sum(u_m(1,2:end,ie).^2));
%     uyw=sqrt(sum(u_m(2,ntest+1:end,ie).^2)/sum(u_m(2,2:end,ie).^2));
%     uzw=sqrt(sum(u_m(3,ntest+1:end,ie).^2)/sum(u_m(3,2:end,ie).^2)); 

    uxw=sqrt(sum(u_m(1,ntest+1:end,ie).^2)/sum(u_m(1,1:end,ie).^2));
    uyw=sqrt(sum(u_m(2,ntest+1:end,ie).^2)/sum(u_m(2,1:end,ie).^2));
    uzw=sqrt(sum(u_m(3,ntest+1:end,ie).^2)/sum(u_m(3,1:end,ie).^2)); 
    ind(ie) = (uxw+uyw+uzw)/3;
 end