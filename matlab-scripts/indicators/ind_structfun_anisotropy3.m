function ind = ind_structfun_anisotropy3(DD,diss_mean,umean2,rr,ne)

%used for various experiments

ind = zeros(1,ne);
for ie =1:ne
%    Q1 = struct_fun_anisotropy2(DD(:,:,1,ie),rr(:,1,ie))/norm(DD(:,:,1,ie),'fro');
%    Q2 = struct_fun_anisotropy2(DD(:,:,2,ie),rr(:,2,ie))/norm(DD(:,:,2,ie),'fro');
%    Q3 = struct_fun_anisotropy2(DD(:,:,3,ie),rr(:,3,ie))/norm(DD(:,:,3,ie),'fro');
%    Q4 = struct_fun_anisotropy2(DD(:,:,4,ie),rr(:,4,ie))/norm(DD(:,:,4,ie),'fro');
%    Q5 = struct_fun_anisotropy2(DD(:,:,5,ie),rr(:,5,ie))/norm(DD(:,:,5,ie),'fro');
%    Q6 = struct_fun_anisotropy2(DD(:,:,6,ie),rr(:,6,ie))/norm(DD(:,:,6,ie),'fro');
      
%    Q1 = struct_fun_anisotropy_norm1(DD(:,:,1,ie),rr(:,1,ie));
%    Q2 = struct_fun_anisotropy_norm1(DD(:,:,2,ie),rr(:,2,ie));
%    Q3 = struct_fun_anisotropy_norm1(DD(:,:,3,ie),rr(:,3,ie));
%    Q4 = struct_fun_anisotropy_norm1(DD(:,:,4,ie),rr(:,4,ie));
%    Q5 = struct_fun_anisotropy_norm1(DD(:,:,5,ie),rr(:,5,ie));
%    Q6 = struct_fun_anisotropy_norm1(DD(:,:,6,ie),rr(:,6,ie));
   
%    Q1 = isoform_norm(DD(:,:,1,ie),rr(:,1,ie));
%    Q2 = isoform_norm(DD(:,:,2,ie),rr(:,2,ie));
%    Q3 = isoform_norm(DD(:,:,3,ie),rr(:,3,ie));
%    Q4 = isoform_norm(DD(:,:,4,ie),rr(:,4,ie));
%    Q5 = isoform_norm(DD(:,:,5,ie),rr(:,5,ie));
%    Q6 = isoform_norm(DD(:,:,6,ie),rr(:,6,ie));
% 
   Q1 = struct_fun_anisotropy3(DD(:,:,1,ie),rr(:,1,ie),diss_mean(ie),umean2(ie));
   Q2 = struct_fun_anisotropy3(DD(:,:,2,ie),rr(:,2,ie),diss_mean(ie),umean2(ie));
   Q3 = struct_fun_anisotropy3(DD(:,:,3,ie),rr(:,3,ie),diss_mean(ie),umean2(ie));
   Q4 = struct_fun_anisotropy3(DD(:,:,4,ie),rr(:,4,ie),diss_mean(ie),umean2(ie));
   Q5 = struct_fun_anisotropy3(DD(:,:,5,ie),rr(:,5,ie),diss_mean(ie),umean2(ie));
   Q6 = struct_fun_anisotropy3(DD(:,:,6,ie),rr(:,6,ie),diss_mean(ie),umean2(ie));

%    Q1 = struct_fun_anisotropy2(DD(:,:,1,ie),rr(:,1,ie));
%    Q2 = struct_fun_anisotropy2(DD(:,:,2,ie),rr(:,2,ie));
%    Q3 = struct_fun_anisotropy2(DD(:,:,3,ie),rr(:,3,ie));
%    Q4 = struct_fun_anisotropy2(DD(:,:,4,ie),rr(:,4,ie));
%    Q5 = struct_fun_anisotropy2(DD(:,:,5,ie),rr(:,5,ie));
%    Q6 = struct_fun_anisotropy2(DD(:,:,6,ie),rr(:,6,ie));
%    
   ind(ie) = (Q1+Q2+Q3+Q4+Q5+Q6)/6;
   %ind(ie) = max([Q1, Q2, Q3, Q4, Q5, Q6]);
end
end