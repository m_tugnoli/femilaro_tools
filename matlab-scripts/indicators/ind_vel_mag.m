function ind = ind_vel_mag(uu,grid,base)
%simple test indicator, magnitude of mean momentum on each cell
 u_m = uu(3:2+grid.d,:,:);
 ind = zeros(1,grid.ne);
 nbas_pol = [1 4 10 20 35];
 ntest = nbas_pol(base.k);
 for ie = 1:grid.ne
   mag_u_2 = u_m(1,1,ie)^2+u_m(2,1,ie)^2+u_m(3,1,ie)^2;
          
    ind(ie) = sqrt(1/mag_u_2);
 end