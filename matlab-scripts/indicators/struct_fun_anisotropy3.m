function [ Q ] = struct_fun_anisotropy3( D, r, diss, umean2)
%STRUCT_FUN_ANISOTROPY quantify how much the structure function is not an
%isotropic function
%  Takes as input a structure function tensor D and the vector used to make
%  the correlation r and returns the quadratic error Q. r should be a
%  column vector

%EXPERIMENTAL version with removal of norms before subtraction

normR2 = norm(r)^2;
RiRj = (r *  r')/normR2;
trRiRj = 1;
RiRj2 = sum(sum(RiRj.^2));
trD = trace(D);

Dnn = (sum(sum(D.*RiRj))*trRiRj-trD*RiRj2)/(trRiRj-3*RiRj2);
Dll = Dnn*(trRiRj-3)/(trRiRj)+trD/trRiRj;

isoD = Dnn*eye(3)+(Dll-Dnn)*RiRj;

%Q = sum(sum((   (Dnn*eye(3)+(Dll-Dnn)*RiRj-D).^2)./((Dnn*eye(3)+(Dll-Dnn)*RiRj+1e-10).^2)   ));
%Q = sum(sum(   sqrt((isoD-D).^2)./(norm(r)*diss)^(2/3) ));
Q = sum(sum(   sqrt((isoD-D).^2)./umean2 ));
%Q = sum(sum((isoD/norm(isoD,'fro')-D/norm(D,'fro')).^2));

% Q = 0;
% for i = 1:3
%   for j = 1:3
%     diff = isoD(i,j) - D(i,j);
%     %ordnum = floor(log10(diff));
%     %ordden = floor(log10(D(i,j)));
%     if (D(i,j) < 1e-4)
%       %Q = Q + diff^2;
%       Q = Q + 0;
%     else
%       Q = Q + diff^2/D(i,j);
%     end
%   end
% end



end

