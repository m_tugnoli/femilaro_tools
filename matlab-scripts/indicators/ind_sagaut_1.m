function [ind] = ind_sagaut_1(uuu,grid,interpmat)
%modified verison to use vertex interpolated values
ind = zeros(1,grid.ne);

points = [1/4 1/3 1/3 0.0 1/3
          1/4 1/3 0.0 1/3 1/3
          1/4 0.0 1/3 1/3 1/3];
comb_table = [1 1 1 1  
              2 3 4 5];        
        
for ie = 1:grid.ne
   u_g = uuu(3:5,:,ie)*interpmat;
   rho_g = (uuu(1,:,ie)*interpmat);
   u_g(1,:) = u_g(1,:)./rho_g;
   u_g(2,:) = u_g(2,:)./rho_g;
   u_g(3,:) = u_g(3,:)./rho_g;
   
   indvect = zeros(4,1);
   for ip = 1:4
   corr_nodes = points(:,comb_table(:,ip));
   corr_U = u_g(:,comb_table(:,ip));
   Udiff = corr_U(:,2)-corr_U(:,1);
   r =  grid.e(ie).b*(corr_nodes(:,2)-corr_nodes(:,1));
   grad = Udiff/norm(r);
   indvect(ip) = max(grad);
   end
   ind(ie) = max(indvect);
end
end
