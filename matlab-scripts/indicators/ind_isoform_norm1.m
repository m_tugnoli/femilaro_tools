function [ ind ] = ind_isoform_norm1(DD,rr,ne)
%IND_ISOFORM_NORM calculation of the norm of the isotropic form of the 
%structure function

ind = zeros(1,ne);
for ie =1:ne
   Q1 = isoform_norm(DD(:,:,1,ie),rr(:,1,ie));
   Q2 = isoform_norm(DD(:,:,2,ie),rr(:,2,ie));
   Q3 = isoform_norm(DD(:,:,3,ie),rr(:,3,ie));
   Q4 = isoform_norm(DD(:,:,4,ie),rr(:,4,ie));
   Q5 = isoform_norm(DD(:,:,5,ie),rr(:,5,ie));
   Q6 = isoform_norm(DD(:,:,6,ie),rr(:,6,ie));
  
   ind(ie) = (Q1+Q2+Q3+Q4+Q5+Q6)/6;
end

end

