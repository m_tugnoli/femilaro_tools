close all; clc

%Program to build degree limiters for adaptive channel simulations, in the
%framework of a three reference simulations. In a single program the data
%from three different simulations at different degrees will be analyzed and
%compared to obtain a single set of treshold values to adapt all three
%simulations.
%The same as the original version but only for the more refined mesh series
filename_2 = '/home/matteo/DG/SIMULATIONS/CHANNEL_2/chan_deg2mr_aniso_ref1/Output/chan_deg2mr_aniso_ref1_1';
filename_3 = '/home/matteo/DG/SIMULATIONS/CHANNEL_2/chan_deg3mr_aniso_ref1/Output/chan_deg3mr_aniso_ref1_1';
filename_4 = '/home/matteo/DG/SIMULATIONS/CHANNEL_2/chan_deg4mr_aniso_ref1/Output/chan_deg4mr_aniso_ref1_1';
res1 = 3;
res2 = 34;


%% Degree 2
% Loading
%think about converting the mat files here
clear grid base
load ([filename_2,'-base.mat'])
load ([filename_2,'-grid.mat'])
position_2 = [grid.e.xb];
indicator = zeros(1,grid.ne);
format = '%04d';

DDmean = zeros(3,3,6,grid.ne);

% Indicator calculation
for it = res1:res2
   uuu = h5read([filename_2,'-res-',num2str(it,format),'.h5'], '/uuu');
   
   %==== relative weight indicator  ====
   indicator = indicator + ind_relative_weight_2(uuu,grid,base);
   %indicator = indicator + ind_relative_weight_3(uuu,grid,base);
   
   %==== structure function anisotropy ====
%    [DD,rr] = ind_struct_fun_1(uuu,grid,base);
%    DDmean = DDmean + DD;
   
   %==== stuttgart relative weight ====
%    indicator = indicator + ind_stutt_1(uuu, grid, base);
end
% DDmean = DDmean/(res2-res1);
% indicator_2 = ind_structfun_anisotropy1(DDmean,rr,grid.ne);

indicator_2 = indicator/(res2-res1);

%% Degree 3
% Loading
%think about converting the mat files here
clear grid base
load ([filename_3,'-base.mat'])
load ([filename_3,'-grid.mat'])
position_3 = [grid.e.xb];
indicator = zeros(1,grid.ne);
format = '%04d';

DDmean = zeros(3,3,6,grid.ne);

% Indicator calculation
for it = res1:res2
   uuu = h5read([filename_3,'-res-',num2str(it,format),'.h5'], '/uuu');
   
   %==== relative weight indicator  ====
   indicator = indicator + ind_relative_weight_2(uuu,grid,base);
   %indicator = indicator + ind_relative_weight_3(uuu,grid,base);
   
   %==== structure function anisotropy ====
%    [DD,rr] = ind_struct_fun_1(uuu,grid,base);
%    DDmean = DDmean + DD;
   
   %==== stuttgart relative weight ====
%    indicator = indicator + ind_stutt_1(uuu, grid, base);
end
% DDmean = DDmean/(res2-res1);
% indicator = ind_structfun_anisotropy1(DD,rr,grid.ne);

indicator_3 = indicator/(res2-res1);

%% Degree 4
% Loading
%think about converting the mat files here
clear grid base
load ([filename_4,'-base.mat'])
load ([filename_4,'-grid.mat'])
position_4 = [grid.e.xb];
indicator = zeros(1,grid.ne);
format = '%04d';

DDmean = zeros(3,3,6,grid.ne);

% Indicator calculation
for it = res1:res2
   uuu = h5read([filename_4,'-res-',num2str(it,format),'.h5'], '/uuu');
   
   %==== relative weight indicator  ====
   indicator = indicator + ind_relative_weight_2(uuu,grid,base);
   %indicator = indicator + ind_relative_weight_3(uuu,grid,base);
   
   %==== structure function anisotropy ====
%    [DD,rr] = ind_struct_fun_1(uuu,grid,base);
%    DDmean = DDmean + DD;
   
   %==== stuttgart relative weight ====
%    indicator = indicator + ind_stutt_1(uuu, grid, base);
end
% DDmean = DDmean/(res2-res1);
% indicator = ind_structfun_anisotropy1(DD,rr,grid.ne);

indicator_4 = indicator/(res2-res1);

%% Processing

%--- 2 ---
[y_uniq_2, ia, ic] = unique(position_2(2,:));
set_mean_2 = zeros(size(y_uniq_2));
set_rms_2 = zeros(size(y_uniq_2));
for iu=1:length(y_uniq_2)
   subset = indicator_2(ic==iu);
   set_mean_2(iu) = mean(subset);
   set_rms_2(iu) = rms(subset-set_mean_2(iu));
end

%--- 3 ---
[y_uniq_3, ia, ic] = unique(position_3(2,:));
set_mean_3 = zeros(size(y_uniq_3));
set_rms_3 = zeros(size(y_uniq_3));
for iu=1:length(y_uniq_3)
   subset = indicator_3(ic==iu);
   set_mean_3(iu) = mean(subset);
   set_rms_3(iu) = rms(subset-set_mean_3(iu));
end

%--- 4 ---
[y_uniq_4, ia, ic] = unique(position_4(2,:));
set_mean_4 = zeros(size(y_uniq_4));
set_rms_4 = zeros(size(y_uniq_4));
for iu=1:length(y_uniq_4)
   subset = indicator_4(ic==iu);
   set_mean_4(iu) = mean(subset);
   set_rms_4(iu) = rms(subset-set_mean_4(iu));
end

figure
hold on
plot(y_uniq_2, set_mean_2,'*-b')
plot(y_uniq_3, set_mean_3,'*-g')
plot(y_uniq_4, set_mean_4,'*-r')

legend('2^{nd}','3^{rd}','4^{th}')
title('Mean over y')

figure
hold on
plot(y_uniq_2, set_rms_2,'*-b')
plot(y_uniq_3, set_rms_3,'*-g')
plot(y_uniq_4, set_rms_4,'*-r')
title('Rms in y')

%% set the tresholds


%I think ind_relative_weight_2 values, used for the first adapt1 series
lo_trs = 0.07;
hi_trs = 0.13;

%ind_relative_weight_3 values
%lo_trs = 0.015;
%hi_trs = 0.028;


% - 2 -
up = find(indicator_2>hi_trs);
lo = find(indicator_2<lo_trs);
me = find(indicator_2<hi_trs & indicator_2 > lo_trs);

figure
plot3(position_2(1,lo),position_2(2,lo),position_2(3,lo),'^b')
hold on
plot3(position_2(1,me),position_2(2,me),position_2(3,me),'^g')
plot3(position_2(1,up),position_2(2,up),position_2(3,up),'^r')
title('Deg 2')

% - 3 -
up = find(indicator_3>hi_trs);
lo = find(indicator_3<lo_trs);
me = find(indicator_3<hi_trs & indicator_3 > lo_trs);

figure
plot3(position_3(1,lo),position_3(2,lo),position_3(3,lo),'^b')
hold on
plot3(position_3(1,me),position_3(2,me),position_3(3,me),'^g')
plot3(position_3(1,up),position_3(2,up),position_3(3,up),'^r')
title('Deg 3')


% - 4 -
up = find(indicator_4>hi_trs);
lo = find(indicator_4<lo_trs);
me = find(indicator_4<hi_trs & indicator_4 > lo_trs);

figure
plot3(position_4(1,lo),position_4(2,lo),position_4(3,lo),'^b')
hold on
plot3(position_4(1,me),position_4(2,me),position_4(3,me),'^g')
plot3(position_4(1,up),position_4(2,up),position_4(3,up),'^r')
title('Deg 4')



%% set the degree and number of dofs statistics
dof_deg = [4 10 20 35];
full_dofs = dof_deg*double(grid.ne);

%- 2 -
deg_lim_2 = 2 * ones(grid.ne,1); %assuming number of elements equal for all degree
deg_lim_2 (indicator_2<lo_trs) = 2;
deg_lim_2 (indicator_2>hi_trs) = 2+1;
for i = 1:4
elems_degs_2(i) = sum(deg_lim_2 == i);
end
dofs_2 = sum(elems_degs_2.*dof_deg);

%- 3 -
deg_lim_3 = 3 * ones(grid.ne,1); %assuming number of elements equal for all degree
deg_lim_3 (indicator_3<lo_trs) = 3-1;
deg_lim_3 (indicator_3>hi_trs) = 3+1;
for i = 1:4
elems_degs_3(i) = sum(deg_lim_3 == i);
end
dofs_3 = sum(elems_degs_3.*dof_deg);

%- 4 -
deg_lim_4 = 4 * ones(grid.ne,1); %assuming number of elements equal for all degree
deg_lim_4 (indicator_4<lo_trs) = 4-1;
deg_lim_4 (indicator_4>hi_trs) = 4;
for i = 1:4
elems_degs_4(i) = sum(deg_lim_4 == i);
end
dofs_4 = sum(elems_degs_4.*dof_deg);




%% Print out

%- 2 -
h5create([filename_2,'_ref_deg.h5'],'/deg_lim',length(deg_lim_2),....
                                                        'Datatype','int32')
h5write([filename_2,'_ref_deg.h5'],'/deg_lim',deg_lim_2)

%- 3 -
h5create([filename_3,'_ref_deg.h5'],'/deg_lim',length(deg_lim_3),....
                                                        'Datatype','int32')
h5write([filename_3,'_ref_deg.h5'],'/deg_lim',deg_lim_3)

%- 4 -
h5create([filename_4,'_ref_deg.h5'],'/deg_lim',length(deg_lim_4),....
                                                        'Datatype','int32')
h5write([filename_4,'_ref_deg.h5'],'/deg_lim',deg_lim_4)



