function [ D, r ] = structfun_on_element( U_g, xig, B, combination )
%CORRELATE_ON_ELEMENT function to calculate the structure function (not the
%expected value, only the correlation)
%   This function executes the correlation for the calculation of the
%   structure function. Inputs are the value of velocity field at the 
%   vertex interpolation nodes U_g, the position in the reference element 
%   of the vertexes xig and the trasformation matrix x = B*xi. The last 
%   argument is to choose which combination of vertex to use to make the 
%   correlation. 
%   The output is the structure function tensor D (not yet meaned) and the
%   vector used for the correlation (in physical coordinates)
comb_table = [1 1 1 2 2 3
                     2 3 4 3 4 4];
corr_nodes = xig(:,comb_table(:,combination));
corr_U = U_g(:,comb_table(:,combination));

Udiff = corr_U(:,2)-corr_U(:,1);

D = Udiff * Udiff';

r = B*(corr_nodes(:,2)-corr_nodes(:,1));
end

