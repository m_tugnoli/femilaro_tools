%attempt to process and compare the different vortex configurations
clear
close all

path='/home/matteo/remote_galileo/VORTEX/longer_series_1/';

tests(1).file_F = 'Dc_Iw_P1_Fmax_long/Output/Dc_Iw_P1_Fmax_1-forces-cylinder_sides.h5';
tests(1).leg = 'Dc Iw P1 Fmax';
tests(1).offset = 3.5; %L max
tests(2).file_F = 'Dc_Iw_P1_Fmax_long/Output/Dc_Iw_P1_Fmax_2-forces-cylinder_sides.h5';
tests(2).leg = 'Dc Iw P0 Fmax longer';
tests(2).offset = 3.5; %L max
tests(3).file_F = 'Dc_P1_Fmax_statlong/Output/Dc_P1_Fmax_statlong_1-forces-cylinder_sides.h5';
tests(3).leg = 'Dc Iw P0 Fmax statlong';
tests(3).offset = 3.5; %L max

%adding also the reference in the tests, to load more segments

tests(4).file_F= '../series1/ref_adapt/Output/ref_adapt1_1-forces-cylinder_sides.h5';
tests(4).leg = 'ref 1';
tests(4).offset = 0;
tests(5).file_F= '../series1/ref_adapt/Output/ref_adapt2_1-forces-cylinder_sides.h5';
tests(5).leg = 'ref 2';
tests(5).offset = 0;




ref.file_F = '../series1/ref_adapt/Output/ref_adapt1_1-forces-cylinder_sides.h5';
ref.leg = 'ref';
%SAVEFIG = true;
SAVEFIG = false;
ntests = length(tests);
%% Load
legendtext{1} = 'ref';
%ref
ref.t = h5read([path,ref.file_F],'/time');
ref.forces_coeff = h5read([path,ref.file_F],'/forces_coeff');

for i=1:ntests
   tests(i).t =  h5read([path,tests(i).file_F],'/time');
   tests(i).tstart = tests(i).t(1); 
   tests(i).tend = tests(i).t(end);
   %tests(i).rstart = find(ref.t == tests(i).tstart+tests(i).offset);
   %tests(i).rend = find(ref.t == tests(i).tend+tests(i).offset);
   [dum,tests(i).rstart] = min(abs(ref.t - (tests(i).tstart+tests(i).offset)));
   [dum,tests(i).rend] = min(abs(ref.t - (tests(i).tend+tests(i).offset)));
   tests(i).forces_coeff =  h5read([path,tests(i).file_F],'/forces_coeff');
   legendtext{i+1} = tests(i).leg;
end



%resize different data length (for the bloody moment assuming all equal)
%len_t = length(tests(1).t);
%ref.t = ref.t(1:len_t);
%ref.forces_coeff = ref.forces_coeff(:,1:len_t);

%% Slice the data

for i=1:ntests
   tests(i).cl_press  =  squeeze(tests(i).forces_coeff(1,:));
   tests(i).cd_press  =  squeeze(tests(i).forces_coeff(2,:));
   tests(i).cl_visc   =  squeeze(tests(i).forces_coeff(10,:));
   tests(i).cd_visc   =  squeeze(tests(i).forces_coeff(11,:));
   tests(i).cl = tests(i).cl_press + tests(i).cl_visc;
   tests(i).cd = tests(i).cd_press + tests(i).cd_visc;
   tests(i).cd = smooth(tests(i).cd,10)';
end

   ref.cl_press  =  squeeze(ref.forces_coeff(1,:));
   ref.cd_press  =  squeeze(ref.forces_coeff(2,:));
   ref.cl_visc   =  squeeze(ref.forces_coeff(10,:));
   ref.cd_visc   =  squeeze(ref.forces_coeff(11,:));
   ref.cl = ref.cl_press + ref.cl_visc;
   ref.cd = ref.cd_press + ref.cd_visc;
   ref.cd = smooth(ref.cd,10)';

   tests(5).t = tests(5).t+ref.t(end);
   
%% Process the data

for i=1:ntests
  tests(i).f = sqrt(tests(i).cl.^2+tests(i).cd.^2);
  tests(i).phi = rad2deg(atan2(tests(i).cl,tests(i).cd));
  tests(i).intf = trapz(tests(i).t,tests(i).f);
  tests(i).fmean = tests(i).intf/(tests(i).t(end)-tests(i).t(1));
  tests(i).fmin = min(tests(i).f);
  tests(i).fmax = max(tests(i).f);
  tests(i).intcd = trapz(tests(i).t,tests(i).cd);
  tests(i).cdmean = tests(i).intcd/(tests(i).t(end)-tests(i).t(1));
  tests(i).cdmin = min(tests(i).cd);
  tests(i).cdmax = max(tests(i).cd);
  tests(i).intcl = trapz(tests(i).t,tests(i).cl);
  tests(i).clmean = tests(i).intcl/(tests(i).t(end)-tests(i).t(1));
  tests(i).clmin = min(tests(i).cl);
  tests(i).clmax = max(tests(i).cl);
  %tests(i).cl_noref = tests(i).cl-ref.cl(tests(i).rstart:tests(i).rend);
  %tests(i).cd_noref = tests(i).cd-ref.cd(tests(i).rstart:tests(i).rend);
  %tests(i).f_noref = sqrt(tests(i).cl_noref.^2+tests(i).cd_noref.^2);
  %tests(i).intf_noref = trapz(tests(i).t,tests(i).f_noref);
end
ref.f = sqrt(ref.cl.^2+ref.cd.^2);
ref.phi = rad2deg(atan2(ref.cl,ref.cd));
ref.intf = trapz(ref.t,ref.f);
ref.fmean = ref.intf/(ref.t(end)-ref.t(1));
ref.fmin = min(ref.f);
ref.fmax = max(ref.f);
ref.intcd = trapz(ref.t,ref.cd);
ref.cdmean = ref.intcd/(ref.t(end)-ref.t(1));
ref.cdmin = min(ref.cd);
ref.cdmax = max(ref.cd);
ref.intcl = trapz(ref.t,ref.cl);
ref.clmean = ref.intcl/(ref.t(end)-ref.t(1));
ref.clmin = min(ref.cl);
ref.clmax = max(ref.cl);



%% Longer figures

figure('name','Cl - very long run')
plot([ref.t(tests(1).rstart:end),tests(5).t]-tests(1).offset, [ref.cl(tests(1).rstart:end),tests(5).cl],'k'), hold on

plot([tests(1).t,(tests(3).t+tests(1).t(end))], [tests(1).cl,tests(3).cl])

figure('name','Cd - very long run')
%plot(ref.t(tests(1).rstart:end)-tests(1).offset, ref.cd(tests(1).rstart:end),'k'), hold on
plot([ref.t(tests(1).rstart:end),tests(5).t]-tests(1).offset, [ref.cd(tests(1).rstart:end),tests(5).cd],'k'), hold on

plot([tests(1).t,(tests(3).t+tests(1).t(end))], [tests(1).cd,tests(3).cd])

% figure('name','F - very long run')
% plot(ref.t(tests(1).rstart:end)-tests(1).offset, ref.f(tests(1).rstart:end),'k'), hold on
% 
% plot([tests(1).t,(tests(3).t+tests(1).t(end))], [tests(1).f,tests(3).f])
% 
% figure('name','Angle - very long run')
% plot(ref.t(tests(1).rstart:end)-tests(1).offset, ref.phi(tests(1).rstart:end),'k'), hold on
% 
% plot([tests(1).t,(tests(3).t+tests(1).t(end))], [tests(1).phi,tests(3).phi])



