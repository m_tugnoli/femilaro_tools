%function get_indicator_test1(filename,res)
close all; clc, clear
%filename_xyz = '/home/matteo/DG/SIMULATIONS/CHANNEL/chan180_m07_mpo_wronggrad_temp/Output/chan180_m07_mpo_wronggrad_temp';
%filename_xyz = '/home/matteo/DG/SIMULATIONS/SQCYL/meshtest2/Output/meshtest2_12';
%filename_xyz = '/home/matteo/DG/SIMULATIONS/SQCYL/ref2/Output/ref2_1';
filename_xyz = '/home/matteo/DG/SIMULATIONS/SQCYL/ref3/Output/ref3_1';
%res1 = 5;
%res2 = 34;
% res1 = 1;
% res2 = 63;
res1 = 120;
res2 = 123;
k_base = 4;
% similar to previous runs, but to test visualization and workflow of
% indicators

%% Loading
%think about converting the mat files here
load ([filename_xyz,'-grid.mat'])
load ([filename_xyz,'-base.mat'])

position = [grid.e.xb];
indicator = zeros(1,grid.ne);
format_out = '%04d';

DDmean = zeros(3,3,6,grid.ne);
load intmat.mat
intmat_sagaut = load ('intmat_sagaut.mat');

%% Indicator calculation
for it = res1:res2
   uuu = h5read([filename_xyz,'-res-',num2str(it,format_out),'.h5'], '/uuu');
   
   %==== relative weight indicator  ====
%    indicator = indicator + ind_relative_weight_1(uuu,grid.ne);
%    indicator = indicator + ind_relative_weight_3(uuu,grid,base);
   
   %==== structure function anisotropy ====
   [DD,rr] = ind_struct_fun_3(uuu,grid,intmat(k_base+1).m,base);
%    [DD,rr] = ind_struct_fun_experimental(uuu,grid,intmat(k_base+1).m,base);
   DDmean = DDmean + DD;
   
   %==== stuttgart relative weight ====
%    indicator = indicator + ind_stutt_1(uuu, grid, base);
   
   %==== modelled energy ====
   %tau = h5read([filename,'-res-',num2str(it,format_out),'.h5'], '/td_diags');
   %tau = tau(3:11,:,:);
   %indicator = indicator + ind_model_energy_1( uuu, tau, grid);
   
   %==== Burbeau & Sagaut ===
   %indicator = indicator + ind_sagaut_1(uuu,grid,intmat_sagaut.intmat(k_base+1).m);
end
%% 
 DDmean = DDmean/(res2-res1+1);
 indicator = ind_structfun_anisotropy2(DDmean,rr,grid.ne);
%  indicator = ind_structfun_norm1(DDmean,grid.ne);

% indicator = indicator/(res2-res1+1);



%% set the tresholds

%with all the base
% lo_trs = 0.06;
% hi_trs = 0.08;

%RW2 used for adapt5
% lo_trs = 0.05;
% hi_trs = 0.130;
% nm_lo_trs = 1e-16;
% super_hi_trs = 1e16;

%SF1 adapt4
% nm_lo_trs = 1e-16;
% lo_trs = 6e-4;
% hi_trs = 5e-2;
% super_hi_trs = 1e10;

%SF1 adapt3
% nm_lo_trs = 1e-16;
% lo_trs = 5e-4;
% hi_trs = 1e-2;
% super_hi_trs = 1e10;

%stuttgart relative weight
% nm_lo_trs = 1e-10;
% lo_trs = 1.7e-3;
% hi_trs = 2.5e-3;
% super_hi_trs = 1e10;

%model energy
%lo_trs = 0.025;
%hi_trs = 0.045;

%SF3 adapt8
% nm_lo_trs = 1e-16;
% lo_trs = 3.7e-3;
% hi_trs = 0.75e-1;
% super_hi_trs = 1e16;

%SF3 adapt9
% nm_lo_trs = 1e-16;
% lo_trs = 8.55e-3;
% hi_trs = 0.38e-1;
% super_hi_trs = 1e16;

%SF4 adapt10
%  nm_lo_trs = 1e-16;
%  lo_trs = 1.70e-2;
%  hi_trs = 0.77e-1;
%  super_hi_trs = 1e16;
 
 
%SF4 adapt11 (starting from 2)
%  nm_lo_trs = 1e-16;
%  lo_trs = 2.10e-2;
%  hi_trs = 0.9e-1;
%  super_hi_trs = 1e16;
 
%RW2 used for adapt12
% lo_trs = 0.075;
% hi_trs = 0.113;
% nm_lo_trs = 1e-16;
% super_hi_trs = 1e16;

%SF1 fixed  adapt13
%  nm_lo_trs = 1e-16;
%  lo_trs = 5e-4;
%  hi_trs = 1e-2;
%  super_hi_trs = 1e16;
 
%BS1  adapt14
%  nm_lo_trs = 1e-16;
%  lo_trs = 3.76e-1;
%  hi_trs = 1.1e0;
%  super_hi_trs = 1e16;

%SF1 fixed adapt15 (starting from 2)
%  nm_lo_trs = 1e-16;
%  lo_trs = 5e-4;
%  hi_trs = 1e-2;
%  super_hi_trs = 1e16;

%RW2 with mean adapt16
% lo_trs = 0.004;
% hi_trs = 0.022;
% nm_lo_trs = 1e-16;
% super_hi_trs = 1e16;

%SF3 for adapt17
 nm_lo_trs = 1e-16;
 lo_trs = 4.7e-4;
 hi_trs = 1e-2;
 super_hi_trs = 1e16;

up = find(indicator>hi_trs & indicator<super_hi_trs);
lo = find(indicator<lo_trs & indicator> nm_lo_trs);
me = find(indicator<hi_trs & indicator > lo_trs);
nm = find(indicator<nm_lo_trs);
su_up = find(indicator>super_hi_trs);

figure
plot3(position(1,lo),position(2,lo),position(3,lo),'^b')
hold on
plot3(position(1,me),position(2,me),position(3,me),'^g')
plot3(position(1,up),position(2,up),position(3,up),'^r')
plot3(position(1,nm),position(2,nm),position(3,nm),'^k')
plot3(position(1,su_up),position(2,su_up),position(3,su_up),'^m')

%% set the degree
deg_lim = 3 * ones(grid.ne,1);
use_model = 1 * ones(grid.ne,1);

deg_lim (indicator<lo_trs) = 2;
deg_lim (indicator>hi_trs) = 4;
deg_lim (indicator>super_hi_trs) = k_base+2;
use_model (indicator<nm_lo_trs) = 0;

% set the degree and number of dofs statistics
dof_deg = [4 10 20 35 56 84 120];
full_dofs = dof_deg*double(grid.ne);

for i = 1:7
elems_degs(i) = sum(deg_lim == i);
end
dofs_adapt = sum(elems_degs.*dof_deg);


%% Print out


%TODO: fix the fact that if the file exists it goes mad
%file_append = '_adaptdata14.h5';
file_append = '_test_matlab.h5';
h5create([filename_xyz,file_append],'/edeg',length(deg_lim),....
                                                        'Datatype','int32')
h5write([filename_xyz,file_append],'/edeg',deg_lim)
h5create([filename_xyz,file_append],'/use_model',length(use_model),....
                                                        'Datatype','int32')
h5write([filename_xyz,file_append],'/use_model',use_model)
h5create([filename_xyz,file_append],'/indicator',length(indicator),....
                                                       'Datatype','double')
h5write([filename_xyz,file_append],'/indicator',indicator)



