%function get_indicator_test1(filename,res)
close all; clc
filename = '/home/matteo/DG/SIMULATIONS/CHANNEL/chan180_m07_mpo_wronggrad_adapt2/Output/chan180_m07_mpo_wronggrad_adapt2';
testnum ='_5';
res1 = 3;
res2 = 23;
k_base = 4;
% really similar to number 1, but taylored to refine several times
%wronggrad_adapt2

%% Loading
%think about converting the mat files here
load ([filename,'-base.mat'])
load ([filename,'-grid.mat'])
base = base(k_base+1);
position = [grid.e.xb];
%indicator = zeros(1,grid.ne);
format = '%04d';

DDmean = zeros(3,3,6,grid.ne);

%% Indicator calculation
for it = res1:res2
   uuu = h5read([filename,testnum,'-res-',num2str(it,format),'.h5'], '/uuu');
   
   %relative weight indicator
%    indicator = indicator + ind_relative_weight_1(uuu,grid.ne);
   
   %structure function anisotropy
   [DD,rr] = ind_struct_fun_2(uuu,grid,base,k_base);
   DDmean = DDmean + DD;
end
edeg = h5read([filename,testnum,'-res-',num2str(it,format),'.h5'], '/edeg');
DDmean = DDmean/(res2-res1);
indicator = ind_structfun_anisotropy1(DD,rr,grid.ne);

% indicator = indicator/(res2-res1);

% %fake, keep everything 3 and then lower in the center
% for ie = 1:grid.ne
%    if (abs(position(2,ie)) <= 0.7)
%       indicator(ie) = 0.0;
%    else
%       indicator(ie) = 0.07;
%    end
% end


%% Processing
[y_uniq, ia, ic] = unique(position(2,:));

set_mean = zeros(size(y_uniq));
set_rms = zeros(size(y_uniq));
for iu=1:length(y_uniq)
   subset = indicator(ic==iu);
   set_mean(iu) = mean(subset);
   set_rms(iu) = rms(subset-set_mean(iu));
end
figure
plot(y_uniq, set_mean,'*-')
title('Mean over y')
figure
plot(y_uniq, set_rms,'*-')
title('Rms in y')

%% set the tresholds

up = find(edeg==4);
lo = find(edeg==2);
me = find(edeg==3);

figure
plot3(position(1,lo),position(2,lo),position(3,lo),'^b')
hold on
plot3(position(1,me),position(2,me),position(3,me),'^g')
plot3(position(1,up),position(2,up),position(3,up),'^r')
title('degrees before refining')

%with all the base
% lo_trs = 0.06;
% hi_trs = 0.08;

%without the mean
% lo_trs = 0.175;
% hi_trs = 0.19;

%structure function anisotropy
lo_trs = 1e-2;
hi_trs = 9e-2;


up = find(indicator>hi_trs);
lo = find(indicator<lo_trs);
me = find(indicator<hi_trs & indicator > lo_trs);

figure
plot3(position(1,lo),position(2,lo),position(3,lo),'^b')
hold on
plot3(position(1,me),position(2,me),position(3,me),'^g')
plot3(position(1,up),position(2,up),position(3,up),'^r')
title('cells to refine/coarsen')

%% set the degree
deg_lim = edeg;
deg_lim (indicator<lo_trs) = deg_lim (indicator<lo_trs)-1;
deg_lim (indicator>hi_trs) = deg_lim (indicator>hi_trs)+1;
%cap the poly value
deg_lim (deg_lim>4) = 4;
deg_lim (deg_lim<2) = 2;
%print results
up = find(deg_lim==4);
lo = find(deg_lim==2);
me = find(deg_lim==3);
figure
plot3(position(1,lo),position(2,lo),position(3,lo),'^b')
hold on
plot3(position(1,me),position(2,me),position(3,me),'^g')
plot3(position(1,up),position(2,up),position(3,up),'^r')
title('degrees after refining')

%% Print out
%fake stuff to force everything to 2
% deg_4 = 4*ones(size(deg_lim));
% h5create([filename,'_deg_4.h5'],'/deg_lim',length(deg_4),....
%                                                         'Datatype','int32')
% h5write([filename,'_deg_4.h5'],'/deg_lim',deg_4)

%fake stuff to force everything to 2
% deg_2 = 2*ones(size(deg_lim));
% h5create([filename,'_deg_2.h5'],'/deg_lim',length(deg_2),....
%                                                         'Datatype','int32')
% h5write([filename,'_deg_2.h5'],'/deg_lim',deg_2)


%TODO: fix the fact that if the file exists it goes mad
h5create([filename,'_ref_deg6.h5'],'/deg_lim',length(deg_lim),....
                                                        'Datatype','int32')
h5write([filename,'_ref_deg6.h5'],'/deg_lim',deg_lim)



