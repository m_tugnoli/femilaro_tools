%This is just a script similar to all the previous, but just to test the
%parameters for possible new indicators. In a way, to test the building
%blocks 

close all; clc, clear
%filename_xyz = '/home/matteo/DG/SIMULATIONS/CHANNEL/chan180_m07_mpo_wronggrad_temp/Output/chan180_m07_mpo_wronggrad_temp';
%filename_xyz = '/home/matteo/DG/SIMULATIONS/SQCYL/meshtest2/Output/meshtest2_12';
%filename_xyz = '/home/matteo/DG/SIMULATIONS/SQCYL/ref2/Output/ref2_1';
filename_xyz = '/home/matteo/DG/SIMULATIONS/SQCYL/ref3/Output/ref3_1';
%res1 = 5;
%res2 = 34;
% res1 = 1;
% res2 = 63;
res1 = 3;
res2 = 123;
k_base = 4;


%% Loading
%think about converting the mat files here
load ([filename_xyz,'-grid.mat'])
load ([filename_xyz,'-base.mat'])

position = [grid.e.xb];
%indicator = zeros(1,grid.ne);
format = '%04d';

%DDmean = zeros(3,3,6,grid.ne);
load intmat.mat
snorm_mean = zeros(grid.ne,1);
diss_mean = zeros(grid.ne,1);
norm_reystress = zeros(grid.ne,1);
norm_sgsstress = zeros(grid.ne,1);

%% Indicator calculation
for it = res1:res2
   uuu = h5read([filename_xyz,'-res-',num2str(it,format),'.h5'], '/uuu');
   
   td = h5read([filename_xyz,'-res-',num2str(it,format),'.h5'], '/td_diags');
   
   uu = uuu(3:5,:,:);
   
   snorm = td(1,:,:);
   diss  = td(2,:,:);
   
   for ie = 1:grid.ne
     uu = uuu(3:5,:,ie);
     sgs = td(3:11,:,ie);
     uu_mean = uu(:,1)*base.p(1,1);
     uu_mean = uu_mean *uu_mean';
     mean_uu = zeros(3,3);
     sgs_mean = zeros(3,3);
     for i=1:3
       for j=1:3
         mean_uu(i,j) = sum(uu(i,:).*uu(j,:))/sum(base.wg);
         sgs_mean(i,j) = sum(sgs(i+(i-1)*j,:).*uuu(1,:,ie))/sum(base.wg);
       end
     end
     norm_reystress(ie) = norm_reystress(ie) + norm(mean_uu-uu_mean,'fro');
     snorm_mean(ie) = snorm_mean(ie) + snorm(1,1,ie)*base.p(1,1);
     diss_mean(ie) = diss_mean(ie) + diss(1,1,ie)*base.p(1,1);
     %sgs_mean = td(3:11,1,ie)*base.p(1,1);
     %norm_sgsstress(ie) = norm_sgsstress(ie) + sqrt(sum(sgs_mean.^2));
     norm_sgsstress(ie) = norm_sgsstress(ie) + norm(sgs_mean,'fro');
   end
end
%%

norm_reystress = norm_reystress/(res2-res1+1);
snorm_mean = snorm_mean/(res2-res1+1);
diss_mean = diss_mean/(res2-res1+1);
norm_sgsstress = norm_sgsstress/(res2-res1+1);




%% Print out


%TODO: fix the fact that if the file exists it goes mad
file_output = '/home/matteo/DG/SIMULATIONS/SQCYL/ref3/indicator_tests/some_param3.h5';

h5create(file_output,'/snorm_mean',length(snorm_mean),....
                                                       'Datatype','double')
h5write(file_output,'/snorm_mean',snorm_mean)

h5create(file_output,'/diss_mean',length(diss_mean),....
                                                       'Datatype','double')
h5write(file_output,'/diss_mean',diss_mean)

h5create(file_output,'/norm_reystress',length(norm_reystress),....
                                                       'Datatype','double')
h5write(file_output,'/norm_reystress',norm_reystress)

h5create(file_output,'/norm_sgsstress',length(norm_sgsstress),....
                                                       'Datatype','double')
h5write(file_output,'/norm_sgsstress',norm_sgsstress)

