function [ ind ] = ind_structfun_norm1(DD,ne)
%IND_STRUCTFUN_NORM simple implementation of SF2 indicator as the norm of
%the structure function

ind = zeros(1,ne);
for ie =1:ne
   Q1 = norm(DD(:,:,1,ie),'fro');
   Q2 = norm(DD(:,:,2,ie),'fro');
   Q3 = norm(DD(:,:,3,ie),'fro');
   Q4 = norm(DD(:,:,4,ie),'fro');
   Q5 = norm(DD(:,:,5,ie),'fro');
   Q6 = norm(DD(:,:,6,ie),'fro');
  
   ind(ie) = (Q1+Q2+Q3+Q4+Q5+Q6)/6;
end

end

