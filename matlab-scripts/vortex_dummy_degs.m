%program to create a dummy division of polynomial order in the vortex test
%case to test if the variable degree is effective
%casename='/home/matteo/DG/SIMULATIONS/VARIOUS/federico_vortex_sponges';
%casename='/home/matteo/DG/SIMULATIONS/VORTBOX/configA/deg2';
casename='/home/matteo/DG/SIMULATIONS/VORTBOX/configB/deg2';

%load([casename,'/Output/test32-grid.mat'])
load([casename,'/Output/deg2_2-grid.mat'])

deg_lim = ones(1,grid.ne);

for ie=1:grid.ne
  if (grid.e(ie).xb(1)>10)
  %if (grid.e(ie).xb(1)<10)
    deg_lim(ie) = 2;
  else
    deg_lim(ie) = 4;
  end
end

h5create([casename,'/Input/dummy_deg_2.h5'],'/edeg',length(deg_lim),....
                                                        'Datatype','int32')
h5write([casename,'/Input/dummy_deg_2.h5'],'/edeg',deg_lim)