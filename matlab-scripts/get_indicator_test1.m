%function get_indicator_test1(filename,res)
close all; clc
filename = '/home/matteo/DG/SIMULATIONS/CHANNEL/chan180_m07_mpo_wronggrad_temp/Output/chan180_m07_mpo_wronggrad_temp';
res1 = 5;
res2 = 34;
k_base = 3;
% first program to try to implement external indicator and treshold
% computation for statical p-adaptive simulation

%% Loading
%think about converting the mat files here
load ([filename,'-base.mat'])
load ([filename,'-grid.mat'])
position = [grid.e.xb];
indicator = zeros(1,grid.ne);
format = '%04d';

DDmean = zeros(3,3,6,grid.ne);

%% Indicator calculation
for it = res1:res2
   uuu = h5read([filename,'-res-',num2str(it,format),'.h5'], '/uuu');
   
   %==== relative weight indicator  ====
%    indicator = indicator + ind_relative_weight_1(uuu,grid.ne);
   
   %==== structure function anisotropy ====
%    [DD,rr] = ind_struct_fun_1(uuu,grid,base);
%    DDmean = DDmean + DD;
   
   %==== stuttgart relative weight ====
   %indicator = indicator + ind_stutt_1(uuu, grid, base);
   
   %==== modelled energy ====
   tau = h5read([filename,'-res-',num2str(it,format),'.h5'], '/td_diags');
   tau = tau(3:11,:,:);
   indicator = indicator + ind_model_energy_1( uuu, tau, grid);
end
% DDmean = DDmean/(res2-res1);
% indicator = ind_structfun_anisotropy1(DD,rr,grid.ne);

indicator = indicator/(res2-res1);

%fake, 2 in the center and 4 in the sides
% for ie = 1:grid.ne
%    if (abs(position(2,ie)) <= 0.7)
%       indicator(ie) = 0.0;
%    else
%       indicator(ie) = 1.00;
%    end
% end


%% Processing
[y_uniq, ia, ic] = unique(position(2,:));

set_mean = zeros(size(y_uniq));
set_rms = zeros(size(y_uniq));
for iu=1:length(y_uniq)
   subset = indicator(ic==iu);
   set_mean(iu) = mean(subset);
   set_rms(iu) = rms(subset-set_mean(iu));
end
figure
plot(y_uniq, set_mean,'*-')
title('Mean over y')
figure
plot(y_uniq, set_rms,'*-')
title('Rms in y')

%% set the tresholds

%with all the base
% lo_trs = 0.06;
% hi_trs = 0.08;

%without the mean
% lo_trs = 0.175;
% hi_trs = 0.19;

%structure function anisotropy
%lo_trs = 1e-4;
%hi_trs = 1e-3;

%stuttgart relative weight
lo_trs = 1.7e-3
hi_trs = 2.5e-3


up = find(indicator>hi_trs);
lo = find(indicator<lo_trs);
me = find(indicator<hi_trs & indicator > lo_trs);

figure
plot3(position(1,lo),position(2,lo),position(3,lo),'^b')
hold on
plot3(position(1,me),position(2,me),position(3,me),'^g')
plot3(position(1,up),position(2,up),position(3,up),'^r')

%% set the degree
deg_lim = k_base * ones(grid.ne,1);
deg_lim (indicator<lo_trs) = k_base-1;
deg_lim (indicator>hi_trs) = k_base+1;

%% Print out
%fake stuff to force everything to 2
% deg_4 = 4*ones(size(deg_lim));
% h5create([filename,'_deg_4.h5'],'/deg_lim',length(deg_4),....
%                                                         'Datatype','int32')
% h5write([filename,'_deg_4.h5'],'/deg_lim',deg_4)

%fake stuff to force everything to 2
% deg_2 = 2*ones(size(deg_lim));
% h5create([filename,'_deg_2.h5'],'/deg_lim',length(deg_2),....
%                                                         'Datatype','int32')
% h5write([filename,'_deg_2.h5'],'/deg_lim',deg_2)

%fake stuff to force everything to 3
deg_3 = 3*ones(size(deg_lim));
h5create([filename,'_deg_3.h5'],'/deg_lim',length(deg_3),....
                                                        'Datatype','int32')
h5write([filename,'_deg_3.h5'],'/deg_lim',deg_3)


%TODO: fix the fact that if the file exists it goes mad
% h5create([filename,'_ref_deg_strat.h5'],'/deg_lim',length(deg_lim),....
%                                                         'Datatype','int32')
% h5write([filename,'_ref_deg_strat.h5'],'/deg_lim',deg_lim)



