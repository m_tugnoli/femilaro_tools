%Script to see the phase of the lift, to decide where to start 
%and where to hit the vortex
close all;
clear;

filename = ['/home/matteo/DG/SIMULATIONS/VORTEX/series1/ref_adapt/Output/' ...
            'ref_adapt1_1-forces-cylinder_sides.h5'];

forces_coeff =  h5read(filename,'/forces_coeff');
t = h5read(filename,'/time');

cl_press  =  squeeze(forces_coeff(1,:));
cd_press  =  squeeze(forces_coeff(2,:));
cl_visc   =  squeeze(forces_coeff(10,:));
cd_visc   =  squeeze(forces_coeff(11,:));
cl = cl_press + cl_visc;
cd = cd_press + cd_visc;
tt = 0:0.25:30;
clintp = interp1(t,cl,tt);

x_vort_start = -6;
figure
plot(t,cl)
hold on
plot(tt,clintp,'rO')

figure 
plot(tt,clintp,'rO')

figure
plot(t,cd)
