%program to compare the forces coefficients of a pair of simulations of the
%cylinder
close all; clc, clear

filename1 = '/home/matteo/DG/SIMULATIONS/SQCYL/meshtest2/Output/meshtest2_12-forces-cylinder_sides.h5';
filename2 = '/home/matteo/DG/SIMULATIONS/SQCYL/meshtest2/Output/meshtest2_16-forces-cylinder_sides.h5';
forces_coeff1 = h5read(filename1,'/forces_coeff');
time1 = h5read(filename1,'/time');
forces_coeff2 = h5read(filename2,'/forces_coeff');
time2 = h5read(filename2,'/time');