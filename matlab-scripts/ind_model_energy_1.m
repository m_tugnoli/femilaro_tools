function [ ind ] = ind_model_energy_1( uuu, tau, grid)
%indicator calculating the ratio between modelled and flow kinetic energy

mom = uuu(3:2+grid.d,:,:);
rho = uuu(1,:,:);
ind = zeros(1,grid.ne);
tau_diag(1,:,:) = tau(1,:,:);
tau_diag(2,:,:) = tau(5,:,:);
tau_diag(3,:,:) = tau(9,:,:);

for ie = 1:grid.ne
  
  kmod = 0; k_flow = 0;
  for id = 1:grid.d
    kmod = kmod + tau_diag(id,:,ie)*rho(1,:,ie)';
    k_flow = k_flow + mom(id,2:end,ie)*mom(id,2:end,ie)';
  end
  ind(ie) = kmod/k_flow;
  %ind(ie) = kmod;
  
end



end

