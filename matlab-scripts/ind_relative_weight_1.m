function ind = ind_relative_weight_1(uu,ne)
%function to calculate a refinement indicator based on the relative weight
%of pseudo-energy contained at the different modes. The input is the modal
%solution at all elements and number of elements, and the output is the 
%scalar indicator at the elements. 
%At the moment the indicator is implemented using only the
%momentum, with the weight of the 3rd and more degree with respect to all
%the modes
 u_m = uu(3:5,:,:);
 ind = zeros(1,ne);
 for ie = 1:ne
    uxw=sqrt(sum(u_m(1,11:end,ie).^2)/sum(u_m(1,2:end,ie).^2));
    uyw=sqrt(sum(u_m(2,11:end,ie).^2)/sum(u_m(2,2:end,ie).^2));
    uzw=sqrt(sum(u_m(3,11:end,ie).^2)/sum(u_m(3,2:end,ie).^2)); 
    ind(ie) = (uxw+uyw+uzw)/3;
 end