function merge_files(dt_out, varargin)

nfiles = nargin-1;
files = varargin;
%for ifl = 1:nfiles
%  minlen = length(files(ifl));
%end
minlen = min([cellfun(@length,files)]);
lens = cellfun(@length,files);

i=1;
equal = true;
while(equal && i<=minlen)
  chars = cellfun(@(x) x(i), files);
  equal = all(chars == chars(1));
  i = i+1;
end

begindex = i-2;

i=1;
equal = true;
while(equal && i<=minlen)
  chars = cellfun(@(x,y) x(y - i), files,num2cell(lens));
  equal = all(chars == chars(1));
  i = i+1;
end

endindex = lens(1)-i+2;

outfilename = [files{1}(1:begindex),'merged',files{1}(endindex:end)];


t_time = [];
t_data = [];
delay = 0;
for ifile = 1:nfiles
  time = h5read(files{ifile},'/time');
  [~,exact] = findpeaks(-mod(time,dt_out));
  ex = exact(end);
  t_time = [t_time, time(1:ex)+delay];
  delay = time(ex);
  data = h5read(files{ifile},'/forces_coeff');
  t_data = [t_data, data(:,1:ex)];
end

h5create(outfilename,'/time',length(t_data),'Datatype','double')
h5write(outfilename,'/time',t_time)

h5create(outfilename,'/forces_coeff',size(t_data),'Datatype','double')
h5write(outfilename,'/forces_coeff',t_data)