% trying to plot the width of the filter used  with different polynomial
% orders
close all
clear all
clc

figure(1)
hold on
grid on
%title(' Main filter width along y direction')
xlabel('y')
ylabel('\Delta')


figure(2)
hold on
grid on
%title(' Test filter width along y direction')
xlabel('y')
ylabel('\Delta_{test}')

figure(4)
hold on
grid on
%title(' Main-Test filter width ratio along y direction')
xlabel('y')
ylabel('\Delta_{test}/\Delta')
N_phi=[4 10 20 35];

%% LPO
y1_lpo=0.013831314;   %distance of the first point from the wall*2
Nx_lpo=24;
Ny_lpo=24;
Nz_lpo=18;
Lx_lpo=4*pi;
Lz_lpo=2*pi;

Op_lpo = 2;
Nt_lpo = 6;
Nq_lpo = N_phi(Op_lpo);

Op_t_lpo = 1;
Nq_t_lpo = N_phi(Op_t_lpo);

gamma_lpo = find_gamma(y1_lpo,Ny_lpo);
jj_lpo = 0:Ny_lpo;
y_nodes_lpo = -tanh(gamma_lpo*(1-2*jj_lpo/Ny_lpo))/tanh(gamma_lpo);
%y_nodes_lpo = linspace(-1,1,length(jj_lpo));
y_centers_lpo = (y_nodes_lpo(2:end)+y_nodes_lpo(1:end-1))/2;

%x_nodes = linspace(-1,1,length(y_nodes));



y_width_lpo = y_nodes_lpo(2:end)-y_nodes_lpo(1:end-1);

x_width_lpo = Lx_lpo/Nx_lpo;

z_width_lpo = Lz_lpo/Nz_lpo;

V_lpo = x_width_lpo*z_width_lpo*y_width_lpo;

Delta_lpo = V_lpo.^(1/3)/(Nt_lpo*Nq_lpo)^(1/3);
Delta_t_lpo = V_lpo.^(1/3)/(Nt_lpo*Nq_t_lpo)^(1/3);

figure(1)
plot(y_centers_lpo, Delta_lpo, 'o-b')

figure(2)
plot(y_centers_lpo, Delta_t_lpo, 'o-b')


figure(4)
plot(y_centers_lpo, Delta_t_lpo./Delta_lpo, 'o-b')
%% HPO
y1_hpo=0.013831314;   %distance of the first point from the wall*2
Nx_hpo=16;
Ny_hpo=16;
Nz_hpo=12;
Lx_hpo=4*pi;
Lz_hpo=2*pi;

Op_hpo = 4;
Nt_hpo = 6;
Nq_hpo = N_phi(Op_hpo);

Op_t_hpo = 2;
Nq_t_hpo = N_phi(Op_t_hpo);

gamma_hpo = find_gamma(y1_hpo,Ny_hpo);
jj_hpo = 0:Ny_hpo;
y_nodes_hpo = -tanh(gamma_hpo*(1-2*jj_hpo/Ny_hpo))/tanh(gamma_hpo);
%y_nodes_hpo = linspace(-1,1,length(jj_hpo));
y_centers_hpo = (y_nodes_hpo(2:end)+y_nodes_hpo(1:end-1))/2;

%x_nodes = linspace(-1,1,length(y_nodes));



y_width_hpo = y_nodes_hpo(2:end)-y_nodes_hpo(1:end-1);

x_width_hpo = Lx_hpo/Nx_hpo;

z_width_hpo = Lz_hpo/Nz_hpo;

V_hpo = x_width_hpo*z_width_hpo*y_width_hpo;

Delta_hpo = V_hpo.^(1/3)/(Nt_hpo*Nq_hpo)^(1/3);
Delta_t_hpo = V_hpo.^(1/3)/(Nt_hpo*Nq_t_hpo)^(1/3);

figure(1)
plot(y_centers_hpo, Delta_hpo, '*-g')

figure(2)
plot(y_centers_hpo, Delta_t_hpo, '*-g')


figure(4)
plot(y_centers_hpo, Delta_t_hpo./Delta_hpo, '*-g')

%%
figure(1)
legend('Low polynomial order', 'High polynomial order')
figure(2)
legend('Low polynomial order', 'High polynomial order')
figure(4)
legend('Low polynomial order', 'High polynomial order')