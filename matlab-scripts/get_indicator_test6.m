%function get_indicator_test1(filename,res)
close all; clc, clear
%filename_xyz = '/home/matteo/DG/SIMULATIONS/CHANNEL/chan180_m07_mpo_wronggrad_temp/Output/chan180_m07_mpo_wronggrad_temp';
%filename_xyz = '/home/matteo/DG/SIMULATIONS/SQCYL/meshtest2/Output/meshtest2_12';
%filename_xyz = '/home/matteo/DG/SIMULATIONS/SQCYL/ref2/Output/ref2_1';
filename_xyz = '/home/matteo/DG/SIMULATIONS/SQCYL/ref3/Output/ref3_1';
%res1 = 5;
%res2 = 34;
% res1 = 1;
% res2 = 63;
res1 = 3;
res2 = 123;
k_base = 4;
% identical as the number 3, for the cylinder, but in this case I insert 
% geometrical tricks to manually set the polynomial order in some zones

%% Loading
%think about converting the mat files here
load ([filename_xyz,'-grid.mat'])
load ([filename_xyz,'-base.mat'])

position = [grid.e.xb];
indicator = zeros(1,grid.ne);
format = '%04d';

DDmean = zeros(3,3,6,grid.ne);
load intmat.mat

%% Indicator calculation
for it = res1:res2
   uuu = h5read([filename_xyz,'-res-',num2str(it,format),'.h5'], '/uuu');
   
   %==== relative weight indicator  ====
%    indicator = indicator + ind_relative_weight_1(uuu,grid.ne);
%   indicator = indicator + ind_relative_weight_3(uuu,grid,base);
   
   %==== structure function anisotropy ====
   [DD,rr] = ind_struct_fun_3(uuu,grid,intmat(k_base+1).m,base);
   DDmean = DDmean + DD;
   
   %==== stuttgart relative weight ====
%    indicator = indicator + ind_stutt_1(uuu, grid, base);
   
   %==== modelled energy ====
   %tau = h5read([filename,'-res-',num2str(it,format),'.h5'], '/td_diags');
   %tau = tau(3:11,:,:);
   %indicator = indicator + ind_model_energy_1( uuu, tau, grid);
end
%%

 %DDmean1 = DDmean/(res2-res1+1);
 DDmean1 = DDmean/(res2-res1);
 indicator = ind_structfun_anisotropy2(DDmean1,rr,grid.ne);
%  indicator = ind_structfun_norm1(DDmean,grid.ne);

% indicator = indicator/(res2-res1+1);



%% set the tresholds

%with all the base
% lo_trs = 0.06;
% hi_trs = 0.08;

%RW2 used for adapt5
% lo_trs = 0.05;
% hi_trs = 0.130;
% nm_lo_trs = 1e-16;
% super_hi_trs = 1e16;

%RW2 removing manually front hy deg
% lo_trs = 0.05;
% hi_trs = 0.1215;
% nm_lo_trs = 1e-16;
% super_hi_trs = 1e16;

%SF2 adapt4
% nm_lo_trs = 1e-16;
% lo_trs = 6e-4;
% hi_trs = 5e-2;
% super_hi_trs = 1e10;

%SF2 adapt3
% nm_lo_trs = 1e-16;
% lo_trs = 5e-4;
% hi_trs = 1e-2;
% super_hi_trs = 1e10;

%SF2 with manually pumped degree around the cylinder
nm_lo_trs = 1e-16;
lo_trs = 5e-4;
hi_trs = 1.68e-2;
super_hi_trs = 1e10;

%stuttgart relative weight
% nm_lo_trs = 1e-10;
% lo_trs = 1.7e-3;
% hi_trs = 2.5e-3;
% super_hi_trs = 1e10;

%model energy
%lo_trs = 0.025;
%hi_trs = 0.045;



% set the degree
deg_lim = 3 * ones(grid.ne,1);
use_model = 1 * ones(grid.ne,1);

deg_lim (indicator<lo_trs) = 2;
deg_lim (indicator>hi_trs) = 4;
deg_lim (indicator>super_hi_trs) = k_base+2;
use_model (indicator<nm_lo_trs) = 0;

%geometrical override
%remove front sponges layer
deg_lim(position(1,:)<-8) = 2;
%pump deg around the cylinder
eps = 1.5e-1;
deg_lim(abs(position(1,:))<(0.5+eps) & abs(position(2,:))<(0.5+eps)) = 4;

%plot
up = find(deg_lim == 4 );
lo = find(deg_lim == 2 );
me = find(deg_lim == 3 );

figure
plot3(position(1,lo),position(2,lo),position(3,lo),'^b')
hold on
plot3(position(1,me),position(2,me),position(3,me),'^g')
plot3(position(1,up),position(2,up),position(3,up),'^r')



% set the degree and number of dofs statistics
dof_deg = [4 10 20 35 56 84 120];
full_dofs = dof_deg*double(grid.ne);

for i = 1:7
elems_degs(i) = sum(deg_lim == i);
end
dofs_adapt = sum(elems_degs.*dof_deg);


%% Print out


%TODO: fix the fact that if the file exists it goes mad
file_append = '_adaptdata_sf1_manual.h5';
h5create([filename_xyz,file_append],'/deg_lim',length(deg_lim),....
                                                        'Datatype','int32')
h5write([filename_xyz,file_append],'/deg_lim',deg_lim)
h5create([filename_xyz,file_append],'/use_model',length(use_model),....
                                                        'Datatype','int32')
h5write([filename_xyz,file_append],'/use_model',use_model)
h5create([filename_xyz,file_append],'/indicator',length(indicator),....
                                                       'Datatype','double')
h5write([filename_xyz,file_append],'/indicator',indicator)



