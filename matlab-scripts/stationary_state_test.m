%series of test to asssess the convergence of statistics...
close all; clear
%file_root = '/home/matteo/DG/SIMULATIONS/SQCYL/adapt20/Output/';
%file_names = {'adapt20_1-forces-cylinder_sides.h5', 'adapt20_2-forces-cylinder_sides.h5'};
file_root = '/home/matteo/DG/SIMULATIONS/SQCYL/startup_deg2/Output/';
file_names = {'startup_deg2_1-forces-cylinder_sides.h5'};
forces_name = '/forces_coeff';
time_name = '/time';
f_data = []; t = []; tend=0;
for i_f = 1:length(file_names)
  f_data_t = h5read([file_root,file_names{i_f}], forces_name);
  t_t = h5read([file_root,file_names{i_f}], time_name);
  f_data = [f_data, f_data_t];
  t = [t, tend+t_t];
  tend = t(end);
end

dt = t(2) - t(1);
c_l = (f_data(1,:)+f_data(10,:));
c_d = (f_data(2,:)+f_data(11,:));


plot(t,c_l)
xlabel('t')
ylabel('C_L')

[corr, lag] = xcorr(c_l-mean(c_l),'coeff');
corr = corr(length(c_l)-1:end);
lag = lag(length(c_l)-1:end)*dt;
[peaks, ploc] = findpeaks(corr);
T = lag(ploc(2))-lag(ploc(1));
St = 1/T;

figure
plot( lag, corr,'b')
hold on 
plot(lag(ploc), peaks,'^b')


wind_size = 5;
nsampl = floor(wind_size/dt);
[corr, lag] = xcorr(c_l,c_l(1:nsampl+1));
corr = corr(length(c_l)-1:end);
corr = corr/corr(1); %rescale
lag = lag(length(c_l)-1:end)*dt;
figure
plot( lag, corr,'b')
