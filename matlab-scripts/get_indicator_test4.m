%function get_indicator_test1(filename,res)
close all; clc, clear
%filename_xyz = '/home/matteo/DG/SIMULATIONS/CHANNEL/chan180_m07_mpo_wronggrad_temp/Output/chan180_m07_mpo_wronggrad_temp';
%filename_xyz = '/home/matteo/DG/SIMULATIONS/SQCYL/meshtest2/Output/meshtest2_12';
%filename_xyz = '/home/matteo/DG/SIMULATIONS/SQCYL/ref2/Output/ref2_1';
filename_xyz = '/home/matteo/DG/SIMULATIONS/CHANNEL_2/chan_deg4_aniso_ref1/Output//chan_deg4_aniso_ref1_1';
%res1 = 5;
%res2 = 34;
% res1 = 1;
% res2 = 63;
res1 = 3;
res2 = 32;
k_base = 4;
%FOR CHANNEL_3
% similar to previous runs, aimed at testing the new indicators in the more
% refined verisons of the channel_2 simulations for the channel_3 ones.

%% Loading
%think about converting the mat files here
load ([filename_xyz,'-grid.mat'])
load ([filename_xyz,'-base.mat'])

position = [grid.e.xb];
indicator = zeros(1,grid.ne);
format = '%04d';

DDmean = zeros(3,3,6,grid.ne);
load intmat.mat
intmat_sagaut = load ('intmat_sagaut.mat');

%% Indicator calculation
for it = res1:res2
   uuu = h5read([filename_xyz,'-res-',num2str(it,format),'.h5'], '/uuu');
   
   %==== relative weight indicator  ====
%    indicator = indicator + ind_relative_weight_1(uuu,grid.ne);
%   indicator = indicator + ind_relative_weight_3(uuu,grid,base);
   
   %==== structure function anisotropy ====
   [DD,rr] = ind_struct_fun_3(uuu,grid,intmat(k_base+1).m,base);
   DDmean = DDmean + DD;
   
   %==== stuttgart relative weight ====
%    indicator = indicator + ind_stutt_1(uuu, grid, base);
   
   %==== modelled energy ====
   %tau = h5read([filename,'-res-',num2str(it,format),'.h5'], '/td_diags');
   %tau = tau(3:11,:,:);
   %indicator = indicator + ind_model_energy_1( uuu, tau, grid);
   
   %==== Burbeau & Sagaut ===
%    indicator = indicator + ind_sagaut_1(uuu,grid,intmat_sagaut.intmat(k_base+1).m);
end
 DDmean = DDmean/(res2-res1);
 indicator = ind_structfun_anisotropy2(DDmean,rr,grid.ne);
%  indicator = ind_structfun_norm1(DDmean,grid.ne);

% indicator = indicator/(res2-res1+1);


%% Processing
%van driest un damping at the wall

%indicator_damp = indicator./(1-exp(-(1-abs(position(2,:)))*180/20));

[y_uniq, ia, ic] = unique(position(2,:));

set_mean = zeros(size(y_uniq));
set_rms = zeros(size(y_uniq));
for iu=1:length(y_uniq)
   subset = indicator(ic==iu);
   set_mean(iu) = mean(subset);
   set_rms(iu) = rms(subset-set_mean(iu));
end
figure
semilogy(y_uniq, set_mean,'*-')
%plot(y_uniq, set_mean,'*-')
title('Mean over y')
figure
plot(y_uniq, set_rms,'*-')
title('Rms in y')



%% set the tresholds
%indicator = indicator_damp;
%with all the base
% lo_trs = 0.06;
% hi_trs = 0.08;

%SF1 for adapt1
% lo_trs = 1e-4;
% hi_trs = 4e-3;
% nm_lo_trs = 1e-16;
% super_hi_trs = 1e16;

%SF3 (normalized SF1) for adapt3 
% lo_trs = 5e-3;
% hi_trs = 1.63e-2;
% nm_lo_trs = 1e-16;
% super_hi_trs = 1e16;

%RW2 for adapt 2
% lo_trs = 0.095;
% hi_trs = 0.128;
% nm_lo_trs = 1e-16;
% super_hi_trs = 1e16;

%structure function anisotropy
% nm_lo_trs = 1e-16;
% lo_trs = 6e-4;
% hi_trs = 5e-2;
% super_hi_trs = 1e10;

%stuttgart relative weight
% nm_lo_trs = 1e-10;
% lo_trs = 1.7e-3;
% hi_trs = 2.5e-3;
% super_hi_trs = 1e10;

%model energy
%lo_trs = 0.025;
%hi_trs = 0.045;

%SF1 with same values as the cylinder
lo_trs = 5e-4;
hi_trs = 1e-2;
nm_lo_trs = 1e-16;
super_hi_trs = 1e16;

up = find(indicator>hi_trs & indicator<super_hi_trs);
lo = find(indicator<lo_trs & indicator> nm_lo_trs);
me = find(indicator<hi_trs & indicator > lo_trs);
nm = find(indicator<nm_lo_trs);
su_up = find(indicator>super_hi_trs);

figure
plot3(position(1,lo),position(2,lo),position(3,lo),'^b')
hold on
plot3(position(1,me),position(2,me),position(3,me),'^g')
plot3(position(1,up),position(2,up),position(3,up),'^r')
plot3(position(1,nm),position(2,nm),position(3,nm),'^k')
plot3(position(1,su_up),position(2,su_up),position(3,su_up),'^m')

%% set the degree
deg_lim = 3 * ones(grid.ne,1);
use_model = 1 * ones(grid.ne,1);

deg_lim (indicator<lo_trs) = 2;
deg_lim (indicator>hi_trs) = 4;
deg_lim (indicator>super_hi_trs) = k_base+2;
use_model (indicator<nm_lo_trs) = 0;

%% set the degree and number of dofs statistics
dof_deg = [4 10 20 35 56 84 120];
full_dofs = dof_deg*double(grid.ne);

for i = 1:7
elems_degs(i) = sum(deg_lim == i);
end
dofs_adapt = sum(elems_degs.*dof_deg);


%% Print out


%TODO: fix the fact that if the file exists it goes mad
file_append = '_adaptdata4_sf1.h5';
h5create([filename_xyz,file_append],'/edeg',length(deg_lim),....
                                                        'Datatype','int32')
h5write([filename_xyz,file_append],'/edeg',deg_lim)
h5create([filename_xyz,file_append],'/use_model',length(use_model),....
                                                        'Datatype','int32')
h5write([filename_xyz,file_append],'/use_model',use_model)
h5create([filename_xyz,file_append],'/indicator',length(indicator),....
                                                       'Datatype','double')
h5write([filename_xyz,file_append],'/indicator',indicator)



