% a crude script to calculate the average number of degrees of freedom in a
% dynamically adaptive simulation

folder = '/home/matteo/DG/SIMULATIONS/VORTBOX/configA/adapt3/Output/';
filename = 'adapt3_1-dofs_count.h5';

dofs = h5read([folder,filename],'/tot_dofs');

ave_dofs = mean(dofs(10:end))