function  prepare_uniform_deg_lim( ncells, deg, filename )
%build a file with a uniform degree limitation

deg_lim = deg*ones(1,ncells);
h5create([filename,'.h5'],'/deg_lim',length(deg_lim),....
                                                        'Datatype','int32')
h5write([filename,'.h5'],'/deg_lim',deg_lim)


end

