%attempt to process and compare the different vortex configurations
clear
close all

path='/home/matteo/remote_galileo/VORTEX/longer_series_1/';

tests(1).file_F = 'Dc_Iw_P1_Fmax_long/Output/Dc_Iw_P1_Fmax_1-forces-cylinder_sides.h5';
tests(1).leg = 'Dc Iw P1 Fmax';
tests(1).offset = 3.5; %L max
tests(2).file_F = 'Dcc_Iw_P1_Fmax_long/Output/Dcc_Iw_P1_Fmax_1-forces-cylinder_sides.h5';
tests(2).leg = 'Dcc Iw P1 Fmax';
tests(2).offset = 3.5; %L max
tests(3).file_F = 'Dc_Iw_P2_Fmax_long/Output/Dc_Iw_P2_Fmax_1-forces-cylinder_sides.h5';
tests(3).leg = 'Dc Iw P2 Fmax';
tests(3).offset = 3.5; %L max
tests(4).file_F = 'Dc_Iw_P1_Fmax_long/Output/Dc_Iw_P1_Fmax_2-forces-cylinder_sides.h5';
tests(4).leg = 'Dc Iw P0 Fmax longer';
tests(4).offset = 3.5; %L max


tests(5).file_F = 'Dcc_Iw_P1_Fmin_long/Output/Dcc_Iw_P1_Fmin_1-forces-cylinder_sides.h5';
tests(5).leg = 'Dcc Iw P1 Fmin';
tests(5).offset = 7.0; %L min

tests(6).file_F = 'Dcc_Iw_P1_F0_long/Output/Dcc_Iw_P1_F0_1-forces-cylinder_sides.h5';
tests(6).leg = 'Dcc Iw P1 F0';
tests(6).offset = 5.5; %L0


ref.file_F = '../series1/ref_adapt/Output/ref_adapt1_1-forces-cylinder_sides.h5';
ref.leg = 'ref';
%SAVEFIG = true;
SAVEFIG = false;
ntests = length(tests);
%% Load
legendtext{1} = 'ref';
%ref
ref.t = h5read([path,ref.file_F],'/time');
ref.forces_coeff = h5read([path,ref.file_F],'/forces_coeff');

for i=1:ntests
   tests(i).t =  h5read([path,tests(i).file_F],'/time');
   tests(i).tstart = tests(i).t(1); 
   tests(i).tend = tests(i).t(end);
   %tests(i).rstart = find(ref.t == tests(i).tstart+tests(i).offset);
   %tests(i).rend = find(ref.t == tests(i).tend+tests(i).offset);
   [dum,tests(i).rstart] = min(abs(ref.t - (tests(i).tstart+tests(i).offset)));
   [dum,tests(i).rend] = min(abs(ref.t - (tests(i).tend+tests(i).offset)));
   tests(i).forces_coeff =  h5read([path,tests(i).file_F],'/forces_coeff');
   legendtext{i+1} = tests(i).leg;
end



%resize different data length (for the bloody moment assuming all equal)
%len_t = length(tests(1).t);
%ref.t = ref.t(1:len_t);
%ref.forces_coeff = ref.forces_coeff(:,1:len_t);

%% Slice the data

for i=1:ntests
   tests(i).cl_press  =  squeeze(tests(i).forces_coeff(1,:));
   tests(i).cd_press  =  squeeze(tests(i).forces_coeff(2,:));
   tests(i).cl_visc   =  squeeze(tests(i).forces_coeff(10,:));
   tests(i).cd_visc   =  squeeze(tests(i).forces_coeff(11,:));
   tests(i).cl = tests(i).cl_press + tests(i).cl_visc;
   tests(i).cd = tests(i).cd_press + tests(i).cd_visc;
   tests(i).cd = smooth(tests(i).cd,10)';
end

   ref.cl_press  =  squeeze(ref.forces_coeff(1,:));
   ref.cd_press  =  squeeze(ref.forces_coeff(2,:));
   ref.cl_visc   =  squeeze(ref.forces_coeff(10,:));
   ref.cd_visc   =  squeeze(ref.forces_coeff(11,:));
   ref.cl = ref.cl_press + ref.cl_visc;
   ref.cd = ref.cd_press + ref.cd_visc;
   ref.cd = smooth(ref.cd,10)';

   
%% Process the data

for i=1:ntests
  tests(i).f = sqrt(tests(i).cl.^2+tests(i).cd.^2);
  tests(i).phi = rad2deg(atan2(tests(i).cl,tests(i).cd));
  tests(i).intf = trapz(tests(i).t,tests(i).f);
  tests(i).fmean = tests(i).intf/(tests(i).t(end)-tests(i).t(1));
  tests(i).fmin = min(tests(i).f);
  tests(i).fmax = max(tests(i).f);
  tests(i).intcd = trapz(tests(i).t,tests(i).cd);
  tests(i).cdmean = tests(i).intcd/(tests(i).t(end)-tests(i).t(1));
  tests(i).cdmin = min(tests(i).cd);
  tests(i).cdmax = max(tests(i).cd);
  tests(i).intcl = trapz(tests(i).t,tests(i).cl);
  tests(i).clmean = tests(i).intcl/(tests(i).t(end)-tests(i).t(1));
  tests(i).clmin = min(tests(i).cl);
  tests(i).clmax = max(tests(i).cl);
  %tests(i).cl_noref = tests(i).cl-ref.cl(tests(i).rstart:tests(i).rend);
  %tests(i).cd_noref = tests(i).cd-ref.cd(tests(i).rstart:tests(i).rend);
  %tests(i).f_noref = sqrt(tests(i).cl_noref.^2+tests(i).cd_noref.^2);
  %tests(i).intf_noref = trapz(tests(i).t,tests(i).f_noref);
end
ref.f = sqrt(ref.cl.^2+ref.cd.^2);
ref.phi = rad2deg(atan2(ref.cl,ref.cd));
ref.intf = trapz(ref.t,ref.f);
ref.fmean = ref.intf/(ref.t(end)-ref.t(1));
ref.fmin = min(ref.f);
ref.fmax = max(ref.f);
ref.intcd = trapz(ref.t,ref.cd);
ref.cdmean = ref.intcd/(ref.t(end)-ref.t(1));
ref.cdmin = min(ref.cd);
ref.cdmax = max(ref.cd);
ref.intcl = trapz(ref.t,ref.cl);
ref.clmean = ref.intcl/(ref.t(end)-ref.t(1));
ref.clmin = min(ref.cl);
ref.clmax = max(ref.cl);




%% Plots
figroot='/home/matteo/Dropbox/Poli/Phd/MainProject/Datas/Vortex/series1_fig/';


col = hsv(3);

% phase max L
figure('name','Cl - L max')
plot(ref.t(tests(1).rstart:tests(1).rend)-tests(1).offset, ref.cl(tests(1).rstart:tests(1).rend),'k'), hold on
for i=1:3
    plot(tests(i).t, tests(i).cl,'color',col(i,:))
end
legend(legendtext([1,2:4]))
if (SAVEFIG); saveas(gcf,[figroot,'Lmax_cl.pdf']); end;

figure('name','Cd - L max')
plot(ref.t(tests(1).rstart:tests(1).rend)-tests(1).offset, ref.cd(tests(1).rstart:tests(1).rend),'k'), hold on
for i=1:3
    plot(tests(i).t, tests(i).cd,'color',col(i,:))
end
legend(legendtext([1,2:4]))
if (SAVEFIG); saveas(gcf,[figroot,'Lmax_cd.pdf']); end;

figure('name','Fmod - L max')
plot(ref.t(tests(1).rstart:tests(1).rend)-tests(1).offset, ref.f(tests(1).rstart:tests(1).rend),'k'), hold on
for i=1:3
    plot(tests(i).t, tests(i).f,'color',col(i,:))
end
legend(legendtext([1,2:4]))
if (SAVEFIG); saveas(gcf,[figroot,'Lmax_f.pdf']); end;

figure('name','Angle - L max')
plot(ref.t(tests(1).rstart:tests(1).rend)-tests(1).offset, ref.phi(tests(1).rstart:tests(1).rend),'k'), hold on
for i=1:3
    plot(tests(i).t, tests(i).phi,'color',col(i,:))
end
legend(legendtext([1,2:4]))
if (SAVEFIG); saveas(gcf,[figroot,'Lmax_alpha.pdf']); end;




%phase min L
figure('name','Cl - L min')
plot(ref.t(tests(5).rstart:tests(5).rend)-tests(5).offset, ref.cl(tests(5).rstart:tests(5).rend),'k'), hold on
for i=5:5
    plot(tests(i).t, tests(i).cl,'color',col(i-4,:))
end
legend(legendtext([1,6:6]))
if (SAVEFIG); saveas(gcf,[figroot,'Lmin_cl.pdf']); end;

figure('name','Cd - L min')
plot(ref.t(tests(5).rstart:tests(5).rend)-tests(5).offset, ref.cd(tests(5).rstart:tests(5).rend),'k'), hold on
for i=5:5
    plot(tests(i).t, tests(i).cd,'color',col(i-4,:))
end
legend(legendtext([1,6:6]))
if (SAVEFIG); saveas(gcf,[figroot,'Lmin_cd.pdf']); end;

figure('name','Fmod - L min')
plot(ref.t(tests(5).rstart:tests(5).rend)-tests(5).offset, ref.f(tests(5).rstart:tests(5).rend),'k'), hold on
for i=5:5
    plot(tests(i).t, tests(i).f,'color',col(i-4,:))
end
legend(legendtext([1,6:6]))
if (SAVEFIG); saveas(gcf,[figroot,'Lmin_f.pdf']); end;

figure('name','Angle - L min')
plot(ref.t(tests(5).rstart:tests(5).rend)-tests(5).offset, ref.phi(tests(5).rstart:tests(5).rend),'k'), hold on
for i=5:5
    plot(tests(i).t, tests(i).phi,'color',col(i-4,:))
end
legend(legendtext([1,6:6]))
if (SAVEFIG); saveas(gcf,[figroot,'Lmin_alpha.pdf']); end;

%phase L 0
figure('name','Cl - L 0')
plot(ref.t(tests(6).rstart:tests(6).rend)-tests(6).offset, ref.cl(tests(6).rstart:tests(6).rend),'k'), hold on
for i=6:6
    plot(tests(i).t, tests(i).cl,'color',col(i-5,:))
end
legend(legendtext([1,7:7]))
if (SAVEFIG); saveas(gcf,[figroot,'L0_cl.pdf']); end;

figure('name','Cd - L 0')
plot(ref.t(tests(6).rstart:tests(6).rend)-tests(6).offset, ref.cd(tests(6).rstart:tests(6).rend),'k'), hold on
for i=6:6
    plot(tests(i).t, tests(i).cd,'color',col(i-5,:))
end
legend(legendtext([1,7:7]))
if (SAVEFIG); saveas(gcf,[figroot,'L0_cd.pdf']); end;

figure('name','Fmod - L 0')
plot(ref.t(tests(6).rstart:tests(6).rend)-tests(6).offset, ref.f(tests(6).rstart:tests(6).rend),'k'), hold on
for i=6:6
    plot(tests(i).t, tests(i).f,'color',col(i-5,:))
end
legend(legendtext([1,7:7]))
if (SAVEFIG); saveas(gcf,[figroot,'L0_f.pdf']); end;

figure('name','Angle - L 0')
plot(ref.t(tests(6).rstart:tests(6).rend)-tests(6).offset, ref.phi(tests(6).rstart:tests(6).rend),'k'), hold on
for i=6:6
    plot(tests(i).t, tests(i).phi,'color',col(i-5,:))
end
legend(legendtext([1,7:7]))
if (SAVEFIG); saveas(gcf,[figroot,'L0_alpha.pdf']); end;




