function ind = ind_structfun_anisotropy1(DD,rr,ne)
ind = zeros(1,ne);
for ie =1:ne
   Q1 = struct_fun_anisotropy(DD(:,:,1,ie),rr(:,1,ie));
   Q2 = struct_fun_anisotropy(DD(:,:,2,ie),rr(:,2,ie));
   Q3 = struct_fun_anisotropy(DD(:,:,3,ie),rr(:,3,ie));
   Q4 = struct_fun_anisotropy(DD(:,:,4,ie),rr(:,4,ie));
   Q5 = struct_fun_anisotropy(DD(:,:,5,ie),rr(:,5,ie));
   Q6 = struct_fun_anisotropy(DD(:,:,6,ie),rr(:,6,ie));
   ind(ie) = (Q1+Q2+Q3+Q4+Q5+Q6)/6;
end
end