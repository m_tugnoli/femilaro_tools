%Another version of the processing of the vortex series, but to plot 
%more details of fewer results
clear
close all

path='/home/matteo/remote_galileo/VORTEX/series1/';

tests(1).file_F = 'Dc_Iw_P0_Fmax/Output/Dc_Iw_P0_Fmax_2-forces-cylinder_sides.h5';
tests(1).leg = 'Dc Iw P0 Fmax';
tests(1).offset = 3.5; %L max
tests(2).file_F = 'Dcc_Iw_P0_Fmax/Output/Dcc_Iw_P0_Fmax_1-forces-cylinder_sides.h5';
tests(2).leg = 'Dcc Iw P0 Fmax';
tests(2).offset = 3.5; %L max
tests(3).file_F = 'Dc_Iw_P1_Fmax/Output/Dc_Iw_P1_Fmax_1-forces-cylinder_sides.h5';
tests(3).leg = 'Dc Iw P1 Fmax';
tests(3).offset = 3.5; %L max
tests(4).file_F = 'Dcc_Iw_P1_Fmax/Output/Dcc_Iw_P1_Fmax_1-forces-cylinder_sides.h5';
tests(4).leg = 'Dcc Iw P1 Fmax';
tests(4).offset = 3.5; %L max
tests(5).file_F = 'Dc_Iw_P2_Fmax/Output/Dc_Iw_P2_Fmax_1-forces-cylinder_sides.h5';
tests(5).leg = 'Dc Iw P2 Fmax';
tests(5).offset = 3.5; %L max
tests(6).file_F = 'Dcc_Iw_P2_Fmax/Output/Dcc_Iw_P2_Fmax_1-forces-cylinder_sides.h5';
tests(6).leg = 'Dcc Iw P2 Fmax';
tests(6).offset = 3.5; %L max

tests(7).file_F = 'Dc_Iw_P0_Fmin/Output/Dc_Iw_P0_Fmin_1-forces-cylinder_sides.h5';
tests(7).leg = 'Dc Iw P0 Fmin';
tests(7).offset = 7.0; %L min
tests(8).file_F = 'Dcc_Iw_P0_Fmin/Output/Dcc_Iw_P0_Fmin_1-forces-cylinder_sides.h5';
tests(8).leg = 'Dcc Iw P0 Fmin';
tests(8).offset = 7.0; %L min
tests(9).file_F = 'Dc_Iw_P1_Fmin/Output/Dc_Iw_P1_Fmin_1-forces-cylinder_sides.h5';
tests(9).leg = 'Dc Iw P1 Fmin';
tests(9).offset = 7.0; %L min
tests(10).file_F = 'Dcc_Iw_P1_Fmin/Output/Dcc_Iw_P1_Fmin_1-forces-cylinder_sides.h5';
tests(10).leg = 'Dcc Iw P1 Fmin';
tests(10).offset = 7.0; %L min
tests(11).file_F = 'Dc_Iw_P2_Fmin/Output/Dc_Iw_P2_Fmin_1-forces-cylinder_sides.h5';
tests(11).leg = 'Dc Iw P2 Fmin';
tests(11).offset = 7.0; %L min
tests(12).file_F = 'Dcc_Iw_P2_Fmin/Output/Dcc_Iw_P2_Fmin_1-forces-cylinder_sides.h5';
tests(12).leg = 'Dcc Iw P2 Fmin';
tests(12).offset = 7.0; %L min


tests(13).file_F = 'Dc_Iw_P0_F0/Output/Dc_Iw_P0_F0_1-forces-cylinder_sides.h5';
tests(13).leg = 'Dc Iw P0 F0';
tests(13).offset = 5.5; %L0
tests(14).file_F = 'Dcc_Iw_P0_F0/Output/Dcc_Iw_P0_F0_merged-forces-cylinder_sides.h5';
tests(14).leg = 'Dcc Iw P0 F0';
tests(14).offset = 5.5; %L0
tests(15).file_F = 'Dc_Iw_P1_F0/Output/Dc_Iw_P1_F0_1-forces-cylinder_sides.h5';
tests(15).leg = 'Dc Iw P1 F0';
tests(15).offset = 5.5; %L0
tests(16).file_F = 'Dcc_Iw_P1_F0/Output/Dcc_Iw_P1_F0_1-forces-cylinder_sides.h5';
tests(16).leg = 'Dcc Iw P1 F0';
tests(16).offset = 5.5; %L0
tests(17).file_F = 'Dc_Iw_P2_F0/Output/Dc_Iw_P2_F0_1-forces-cylinder_sides.h5';
tests(17).leg = 'Dc Iw P2 F0';
tests(17).offset = 5.5; %L0
tests(18).file_F = 'Dcc_Iw_P2_F0/Output/Dcc_Iw_P2_F0_merged-forces-cylinder_sides.h5';
tests(18).leg = 'Dcc Iw P2 F0';
tests(18).offset = 5.5; %L0

ref.file_F = 'ref_adapt/Output/ref_adapt1_1-forces-cylinder_sides.h5';
ref.leg = 'ref';
%SAVEFIG = true;
SAVEFIG = false;
ntests = length(tests);
%% Load
legendtext{1} = 'ref';
%ref
ref.t = h5read([path,ref.file_F],'/time');
ref.forces_coeff = h5read([path,ref.file_F],'/forces_coeff');

for i=1:ntests
   tests(i).t =  h5read([path,tests(i).file_F],'/time');
   tests(i).tstart = tests(i).t(1); 
   tests(i).tend = tests(i).t(end);
   %tests(i).rstart = find(ref.t == tests(i).tstart+tests(i).offset);
   %tests(i).rend = find(ref.t == tests(i).tend+tests(i).offset);
   [dum,tests(i).rstart] = min(abs(ref.t - (tests(i).tstart+tests(i).offset)));
   [dum,tests(i).rend] = min(abs(ref.t - (tests(i).tend+tests(i).offset)));
   tests(i).forces_coeff =  h5read([path,tests(i).file_F],'/forces_coeff');
   legendtext{i+1} = tests(i).leg;
end



%resize different data length (for the bloody moment assuming all equal)
%len_t = length(tests(1).t);
%ref.t = ref.t(1:len_t);
%ref.forces_coeff = ref.forces_coeff(:,1:len_t);

%% Slice the data

for i=1:ntests
   tests(i).cl_press  =  squeeze(tests(i).forces_coeff(1,:));
   tests(i).cd_press  =  squeeze(tests(i).forces_coeff(2,:));
   tests(i).cl_visc   =  squeeze(tests(i).forces_coeff(10,:));
   tests(i).cd_visc   =  squeeze(tests(i).forces_coeff(11,:));
   tests(i).cl = tests(i).cl_press + tests(i).cl_visc;
   tests(i).cd = tests(i).cd_press + tests(i).cd_visc;
   tests(i).cd = smooth(tests(i).cd,10)';
   tests(i).cd_press = smooth(tests(i).cd_press,10)';
   tests(i).cd_visc = smooth(tests(i).cd_visc,10)';
end

   ref.cl_press  =  squeeze(ref.forces_coeff(1,:));
   ref.cd_press  =  squeeze(ref.forces_coeff(2,:));
   ref.cl_visc   =  squeeze(ref.forces_coeff(10,:));
   ref.cd_visc   =  squeeze(ref.forces_coeff(11,:));
   ref.cl = ref.cl_press + ref.cl_visc;
   ref.cd = ref.cd_press + ref.cd_visc;
   ref.cd = smooth(ref.cd,10)';
   ref.cd_press = smooth(ref.cd_press,10)';
   ref.cd_visc = smooth(ref.cd_visc,10)';

   
%% Process the data

for i=1:ntests
  tests(i).f = sqrt(tests(i).cl.^2+tests(i).cd.^2);
  tests(i).phi = rad2deg(atan2(tests(i).cl,tests(i).cd));
  tests(i).intf = trapz(tests(i).t,tests(i).f);
  tests(i).fmean = tests(i).intf/(tests(i).t(end)-tests(i).t(1));
  tests(i).fmin = min(tests(i).f);
  tests(i).fmax = max(tests(i).f);
  tests(i).intcd = trapz(tests(i).t,tests(i).cd);
  tests(i).cdmean = tests(i).intcd/(tests(i).t(end)-tests(i).t(1));
  tests(i).cdmin = min(tests(i).cd);
  tests(i).cdmax = max(tests(i).cd);
  tests(i).intcl = trapz(tests(i).t,tests(i).cl);
  tests(i).clmean = tests(i).intcl/(tests(i).t(end)-tests(i).t(1));
  tests(i).clmin = min(tests(i).cl);
  tests(i).clmax = max(tests(i).cl);
  %tests(i).cl_noref = tests(i).cl-ref.cl(tests(i).rstart:tests(i).rend);
  %tests(i).cd_noref = tests(i).cd-ref.cd(tests(i).rstart:tests(i).rend);
  %tests(i).f_noref = sqrt(tests(i).cl_noref.^2+tests(i).cd_noref.^2);
  %tests(i).intf_noref = trapz(tests(i).t,tests(i).f_noref);
end
ref.f = sqrt(ref.cl.^2+ref.cd.^2);
ref.phi = rad2deg(atan2(ref.cl,ref.cd));
ref.intf = trapz(ref.t,ref.f);
ref.fmean = ref.intf/(ref.t(end)-ref.t(1));
ref.fmin = min(ref.f);
ref.fmax = max(ref.f);
ref.intcd = trapz(ref.t,ref.cd);
ref.cdmean = ref.intcd/(ref.t(end)-ref.t(1));
ref.cdmin = min(ref.cd);
ref.cdmax = max(ref.cd);
ref.intcl = trapz(ref.t,ref.cl);
ref.clmean = ref.intcl/(ref.t(end)-ref.t(1));
ref.clmin = min(ref.cl);
ref.clmax = max(ref.cl);




%% Plots
figroot='/home/matteo/Dropbox/Poli/Phd/MainProject/Datas/Vortex/series1_fig/';



col = hsv(6);

% phase max L
% figure('name','Cl - L max')
% plot(ref.t(tests(1).rstart:tests(1).rend)-tests(1).offset, ref.cl(tests(1).rstart:tests(1).rend),'k'), hold on
% for i=1:6
%     plot(tests(i).t, tests(i).cl,'color',col(i,:))
% end
% legend(legendtext([1,2:7]))
% if (SAVEFIG); saveas(gcf,[figroot,'Lmax_cl.pdf']); end;

figure('name','Cd press - L max')
plot(ref.t(tests(1).rstart:tests(1).rend)-tests(1).offset, ref.cd_press(tests(1).rstart:tests(1).rend),'k'), hold on
for i=1:6
    plot(tests(i).t, tests(i).cd_press,'color',col(i,:))
end
legend(legendtext([1,2:7]))
if (SAVEFIG); saveas(gcf,[figroot,'Lmax_cd_press.pdf']); end;

figure('name','Cd visc - L max')
plot(ref.t(tests(1).rstart:tests(1).rend)-tests(1).offset, ref.cd_visc(tests(1).rstart:tests(1).rend),'k'), hold on
for i=1:6
    plot(tests(i).t, tests(i).cd_visc,'color',col(i,:))
end
legend(legendtext([1,2:7]))
if (SAVEFIG); saveas(gcf,[figroot,'Lmax_cd_visc.pdf']); end;

figure('name','Cl press - L max')
plot(ref.t(tests(1).rstart:tests(1).rend)-tests(1).offset, ref.cl_press(tests(1).rstart:tests(1).rend),'k'), hold on
for i=1:6
    plot(tests(i).t, tests(i).cl_press,'color',col(i,:))
end
legend(legendtext([1,2:7]))
if (SAVEFIG); saveas(gcf,[figroot,'Lmax_cl_press.pdf']); end;

figure('name','Cl visc - L max')
plot(ref.t(tests(1).rstart:tests(1).rend)-tests(1).offset, ref.cl_visc(tests(1).rstart:tests(1).rend),'k'), hold on
for i=1:6
    plot(tests(i).t, tests(i).cl_visc,'color',col(i,:))
end
legend(legendtext([1,2:7]))
if (SAVEFIG); saveas(gcf,[figroot,'Lmax_cl_visc.pdf']); end;

% figure('name','Fmod - L max')
% plot(ref.t(tests(1).rstart:tests(1).rend)-tests(1).offset, ref.f(tests(1).rstart:tests(1).rend),'k'), hold on
% for i=1:6
%     plot(tests(i).t, tests(i).f,'color',col(i,:))
% end
% legend(legendtext([1,2:7]))
% if (SAVEFIG); saveas(gcf,[figroot,'Lmax_f.pdf']); end;
% 
% figure('name','Angle - L max')
% plot(ref.t(tests(1).rstart:tests(1).rend)-tests(1).offset, ref.phi(tests(1).rstart:tests(1).rend),'k'), hold on
% for i=1:6
%     plot(tests(i).t, tests(i).phi,'color',col(i,:))
% end
% legend(legendtext([1,2:7]))
% if (SAVEFIG); saveas(gcf,[figroot,'Lmax_alpha.pdf']); end;


% phase min L
% figure('name','Cl - L min')
% plot(ref.t(tests(7).rstart:tests(7).rend)-tests(7).offset, ref.cl(tests(7).rstart:tests(7).rend),'k'), hold on
% for i=7:12
%     plot(tests(i).t, tests(i).cl,'color',col(i-6,:))
% end
% legend(legendtext([1,8:13]))
% if (SAVEFIG); saveas(gcf,[figroot,'Lmin_cl.pdf']); end;

figure('name','Cd press - L min')
plot(ref.t(tests(7).rstart:tests(7).rend)-tests(7).offset, ref.cd_press(tests(7).rstart:tests(7).rend),'k'), hold on
for i=7:12
    plot(tests(i).t, tests(i).cd_press,'color',col(i-6,:))
end
legend(legendtext([1,8:13]))
if (SAVEFIG); saveas(gcf,[figroot,'Lmin_cd_press.pdf']); end;

figure('name','Cd visc - L min')
plot(ref.t(tests(7).rstart:tests(7).rend)-tests(7).offset, ref.cd_visc(tests(7).rstart:tests(7).rend),'k'), hold on
for i=7:12
    plot(tests(i).t, tests(i).cd_visc,'color',col(i-6,:))
end
legend(legendtext([1,8:13]))
if (SAVEFIG); saveas(gcf,[figroot,'Lmin_cd_visc.pdf']); end;

% figure('name','Fmod - L min')
% plot(ref.t(tests(7).rstart:tests(7).rend)-tests(7).offset, ref.f(tests(7).rstart:tests(7).rend),'k'), hold on
% for i=7:12
%     plot(tests(i).t, tests(i).f,'color',col(i-6,:))
% end
% legend(legendtext([1,8:13]))
% if (SAVEFIG); saveas(gcf,[figroot,'Lmin_f.pdf']); end;
% 
% figure('name','Angle - L min')
% plot(ref.t(tests(7).rstart:tests(7).rend)-tests(7).offset, ref.phi(tests(7).rstart:tests(7).rend),'k'), hold on
% for i=7:12
%     plot(tests(i).t, tests(i).phi,'color',col(i-6,:))
% end
% legend(legendtext([1,8:13]))
% if (SAVEFIG); saveas(gcf,[figroot,'Lmin_alpha.pdf']); end;

% % phase L 0
% figure('name','Cl - L 0')
% plot(ref.t(tests(13).rstart:tests(13).rend)-tests(13).offset, ref.cl(tests(13).rstart:tests(13).rend),'k'), hold on
% for i=13:18
%     plot(tests(i).t, tests(i).cl,'color',col(i-12,:))
% end
% legend(legendtext([1,14:19]))
% if (SAVEFIG); saveas(gcf,[figroot,'L0_cl.pdf']); end;
% 
% figure('name','Cd - L 0')
% plot(ref.t(tests(13).rstart:tests(13).rend)-tests(13).offset, ref.cd(tests(13).rstart:tests(13).rend),'k'), hold on
% for i=13:18
%     plot(tests(i).t, tests(i).cd,'color',col(i-12,:))
% end
% legend(legendtext([1,14:19]))
% if (SAVEFIG); saveas(gcf,[figroot,'L0_cd.pdf']); end;
% 
% figure('name','Fmod - L 0')
% plot(ref.t(tests(13).rstart:tests(13).rend)-tests(13).offset, ref.f(tests(13).rstart:tests(13).rend),'k'), hold on
% for i=13:18
%     plot(tests(i).t, tests(i).f,'color',col(i-12,:))
% end
% legend(legendtext([1,14:19]))
% if (SAVEFIG); saveas(gcf,[figroot,'L0_f.pdf']); end;
% 
% figure('name','Angle - L 0')
% plot(ref.t(tests(13).rstart:tests(13).rend)-tests(13).offset, ref.phi(tests(13).rstart:tests(13).rend),'k'), hold on
% for i=13:18
%     plot(tests(i).t, tests(i).phi,'color',col(i-12,:))
% end
% legend(legendtext([1,14:19]))
% if (SAVEFIG); saveas(gcf,[figroot,'L0_alpha.pdf']); end;