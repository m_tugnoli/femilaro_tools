function [ Q ] = isoform_norm( D, r)
%STRUCT_FUN_ANISOTROPY quantify how much the structure function is not an
%isotropic function
%  Takes as input a structure function tensor D and the vector used to make
%  the correlation r and returns the quadratic error Q. r should be a
%  column vector

normR2 = norm(r)^2;
RiRj = (r *  r')/normR2;
trRiRj = 1;
RiRj2 = sum(sum(RiRj.^2));
trD = trace(D);

Dnn = (sum(sum(D.*RiRj))*trRiRj-trD*RiRj2)/(trRiRj-3*RiRj2);
Dll = Dnn*(trRiRj-3)/(trRiRj)+trD/trRiRj;

Q = sum(sum((Dnn*eye(3)+(Dll-Dnn)*RiRj).^2));


end

