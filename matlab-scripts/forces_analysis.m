function []=forces_analysis(varargin)

%% Load data

%meshtest2_11
forces_name = '/forces_coeff';
time_name = '/time';
f_data = []; t = []; tend=0;
for i_f = 1:nargin
  f_data_t = h5read(varargin{i_f}, forces_name);
  t_t = h5read(varargin{i_f}, time_name);
  f_data = [f_data, f_data_t];
  t = [t, tend+t_t];
  tend = t(end);
end

dt = t(2) - t(1);
c_l = (f_data(1,:)+f_data(10,:));
c_d = (f_data(2,:)+f_data(11,:));
c_l_mean = f_data(4,:)+f_data(13,:);
c_d_mean = f_data(5,:)+f_data(14,:);



%% Data analysis
%Cl cross correlation
[corr, lag] = xcorr(c_l-mean(c_l),'coeff');
corr = corr(length(c_l)-1:end);
lag = lag(length(c_l)-1:end)*dt;

%peak finding
[peaks, ploc] = findpeaks(corr);
T = lag(ploc(2))-lag(ploc(1));
St = 1/T;

%means and rms
m_c_l = mean(c_l);
m_c_d = mean(c_d);
rms_c_l = rms(c_l - m_c_l);
rms_c_d = rms(c_d - m_c_d);
%% print results
fprintf([varargin{1},' results:\n'])
fprintf('Shedding period: %e, Strouhal: %e\n',T, St)
fprintf('Mean Cl: %e, rms Cl: %e\n',m_c_l, rms_c_l)
fprintf('Mean Cd: %e, rms Cd: %e\n',m_c_d, rms_c_d)


%% Plot results
figure
plot(t,c_l,'b')


figure
plot( lag, corr,'b')
hold on 
plot(lag(ploc), peaks,'^b')
