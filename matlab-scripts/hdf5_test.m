%simple script to test the loading of hdf5 files written by matlab into
%dg-comp

close all; clc
ncells = 18432;
filename = '/home/matteo/DG/SIMULATIONS/CHANNEL/chan180_m07_mpo_wronggrad_adapt1/Input/';

real_3d = rand(3,4,ncells);
h5create([filename,'real_3d.h5'],'/real_3d',size(real_3d),'Datatype','double')
h5write([filename,'real_3d.h5'],'/real_3d',real_3d)

int_1d = 1:1:ncells;
h5create([filename,'int_1d.h5'],'/int_1d',length(int_1d),'Datatype','int32')
h5write([filename,'int_1d.h5'],'/int_1d',int_1d)