function [ Q ] = struct_fun_anisotropy3( D, r)
%STRUCT_FUN_ANISOTROPY quantify how much the structure function is not an
%isotropic function
%  Takes as input a structure function tensor D and the vector used to make
%  the correlation r and returns the quadratic error Q. r should be a
%  column vector

%EXPERIMENTAL version with removal of norms before subtraction

normR2 = norm(r)^2;
RiRj = (r *  r')/normR2;
trRiRj = 1;
RiRj2 = sum(sum(RiRj.^2));
trD = trace(D);

Dnn = (sum(sum(D.*RiRj))*trRiRj-trD*RiRj2)/(trRiRj-3*RiRj2);
Dll = Dnn*(trRiRj-3)/(trRiRj)+trD/trRiRj;

isoD = Dnn*eye(3)+(Dll-Dnn)*RiRj;

%Q = sum(sum((Dnn*eye(3)+(Dll-Dnn)*RiRj-D).^2));
Q = sum(sum((isoD/norm(isoD,'fro')-D/norm(D,'fro')).^2));


end

