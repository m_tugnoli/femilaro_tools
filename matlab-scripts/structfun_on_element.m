function [ D, r ] = structfun_on_element( U_g, xig, B, combination, edeg )
%CORRELATE_ON_ELEMENT function to calculate the structure function (not the
%expected value, only the correlation)
%   This function executes the correlation for the calculation of the
%   structure function. Inputs are the value of velocity field at the 
%   gauss nodes U_g, the position in the reference element of the gauss
%   nodes xig and the trasformation matrix x = B*xi. The last argument isto
%   choose which combination of vertex to use to make the correlation,
%   since the gauss nodes nearer to the four vertexes will be used. The
%   output is the structure function tensor D (not yet meaned) and the
%   vector used for the correlation (in physical coordinates)
comb_table(1,:,:) = [1 1 1 1 1 1
                     1 1 1 1 1 1];
comb_table(2,:,:) = [1 1 1 2 2 3
                     2 3 4 3 4 4];
comb_table(3,:,:) = [1 1  1 2  2  3
                     2 3 11 3 11 11];
comb_table(4,:,:) = 4+[1 1 1 2 2 3
                       2 3 4 3 4 4];
comb_table(5,:,:) = 9+[1 1 1 2 2 3
                       2 3 4 3 4 4];
corr_nodes = xig(:,comb_table(edeg+1,:,combination));
corr_U = U_g(:,comb_table(edeg+1,:,combination));

Udiff = corr_U(:,2)-corr_U(:,1);

D = Udiff * Udiff';

r = B*(corr_nodes(:,2)-corr_nodes(:,1));
end

