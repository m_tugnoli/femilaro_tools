close all; clear; clc
%Small program to test how the energy is distributed among the different
%modes, in different zones of the domain, at different polynomial degrees

filename_2 = '/home/matteo/DG/SIMULATIONS/CHANNEL_2/chan_deg2_aniso_ref1/Output/chan_deg2_aniso_ref1_1';
filename_3 = '/home/matteo/DG/SIMULATIONS/CHANNEL_2/chan_deg3_aniso_ref1/Output/chan_deg3_aniso_ref1_1';
filename_4 = '/home/matteo/DG/SIMULATIONS/CHANNEL_2/chan_deg4_aniso_ref1/Output/chan_deg4_aniso_ref1_1';
filename_3wg = '/home/matteo/DG/SIMULATIONS/CHANNEL/chan180_m07_mpo_wronggrad_temp/Output/chan180_m07_mpo_wronggrad_temp';
res1 = 3;
res2 = 34;

%% Degree 2
% Loading
load ([filename_2,'-base.mat'],'base');
base_2 = base; clear base;
load ([filename_2,'-grid.mat'],'grid');
grid_2 = grid; clear grid;
position_2 = [grid_2.e.xb];
maxdeg = 2;
energy_modes = zeros(maxdeg+1,grid_2.ne);
format = '%04d';



% Indicator calculation
for it = res1:res2
  uuu = h5read([filename_2,'-res-',num2str(it,format),'.h5'], '/uuu');
   
  u_m = uuu(3:2+grid_2.d,:,:);
  nbas_pol = [0 1 4 10 20 35];
  for ie = 1:grid_2.ne
    for ib = 1:maxdeg+1
      ex=sum(u_m(1,nbas_pol(ib)+1:nbas_pol(ib+1),ie).^2);
      ey=sum(u_m(2,nbas_pol(ib)+1:nbas_pol(ib+1),ie).^2);
      ez=sum(u_m(3,nbas_pol(ib)+1:nbas_pol(ib+1),ie).^2);
      energy_modes(ib, ie) = energy_modes(ib, ie) + 0.5*(ex+ey+ez);
    end
  end
   
end
energy_modes_2 = energy_modes/(res2-res1);

%% Processing

%--- 2 ---
[y_uniq_2, ia, ic] = unique(position_2(2,:));
set_mean_2 = zeros(maxdeg+1, length(y_uniq_2));
for iu=1:length(y_uniq_2)
   subset = energy_modes_2(:,ic==iu);
   set_mean_2(:,iu) = mean(subset,2);
end


%% Degree 3
% Loading
load ([filename_3,'-base.mat'],'base');
base_3 = base; clear base;
load ([filename_3,'-grid.mat'],'grid');
grid_3 = grid; clear grid;
position_3 = [grid_3.e.xb];
maxdeg = 3;
energy_modes = zeros(maxdeg+1,grid_3.ne);
format = '%04d';



% Indicator calculation
for it = res1:res2
  uuu = h5read([filename_3,'-res-',num2str(it,format),'.h5'], '/uuu');
   
  u_m = uuu(3:2+grid_3.d,:,:);
  nbas_pol = [0 1 4 10 20 35];
  for ie = 1:grid_3.ne
    for ib = 1:maxdeg+1
      ex=sum(u_m(1,nbas_pol(ib)+1:nbas_pol(ib+1),ie).^2);
      ey=sum(u_m(2,nbas_pol(ib)+1:nbas_pol(ib+1),ie).^2);
      ez=sum(u_m(3,nbas_pol(ib)+1:nbas_pol(ib+1),ie).^2);
      energy_modes(ib, ie) = energy_modes(ib, ie) + 0.5*(ex+ey+ez);
    end
  end
   
end
energy_modes_3 = energy_modes/(res2-res1);

%% Processing

%--- 2 ---
[y_uniq_3, ia, ic] = unique(position_3(2,:));
set_mean_3 = zeros(maxdeg+1, length(y_uniq_3));
for iu=1:length(y_uniq_3)
   subset = energy_modes_3(:,ic==iu);
   set_mean_3(:,iu) = mean(subset,2);
end



%% Degree 4
% Loading
load ([filename_4,'-base.mat'],'base');
base_4 = base; clear base;
load ([filename_4,'-grid.mat'],'grid');
grid_4 = grid; clear grid;
position_4 = [grid_4.e.xb];
maxdeg = 4;
energy_modes = zeros(maxdeg+1,grid_4.ne);
format = '%04d';



% Indicator calculation
for it = res1:res2
  uuu = h5read([filename_4,'-res-',num2str(it,format),'.h5'], '/uuu');
   
  u_m = uuu(3:2+grid_4.d,:,:);
  nbas_pol = [0 1 4 10 20 35];
  for ie = 1:grid_4.ne
    for ib = 1:maxdeg+1
      ex=sum(u_m(1,nbas_pol(ib)+1:nbas_pol(ib+1),ie).^2);
      ey=sum(u_m(2,nbas_pol(ib)+1:nbas_pol(ib+1),ie).^2);
      ez=sum(u_m(3,nbas_pol(ib)+1:nbas_pol(ib+1),ie).^2);
      energy_modes(ib, ie) = energy_modes(ib, ie) + 0.5*(ex+ey+ez);
    end
  end
   
end
energy_modes_4 = energy_modes/(res2-res1);

%% Processing

%--- 2 ---
[y_uniq_4, ia, ic] = unique(position_4(2,:));
set_mean_4 = zeros(maxdeg+1, length(y_uniq_4));
for iu=1:length(y_uniq_4)
   subset = energy_modes_4(:,ic==iu);
   set_mean_4(:,iu) = mean(subset,2);
end

%% Pretty Pictures

figure

for i = 1:length(y_uniq_4)

plot(set_mean_4(2:end,i),'r-*')
hold on
axis([1,4,0,1e-3])
plot(set_mean_3(2:end,i),'g-^')
plot(set_mean_2(2:end,i),'b-+')
hold off
pause(0.5)
end
%% Pretty Pictures

figure

for i = 1:length(y_uniq_4)

loglog(set_mean_4(:,i),'r-*')
hold on
axis([1,5,1e-6,1e-1])
loglog(set_mean_3(:,i),'g-^')
loglog(set_mean_2(:,i),'b-+')
hold off
pause(0.5)
end