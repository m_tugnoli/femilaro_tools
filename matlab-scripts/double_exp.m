function e = double_exp (a)

binexp = bitget(a,52:62);
e = bi2de(binexp);