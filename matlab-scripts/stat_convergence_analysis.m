%series of test to asssess the convergence of statistics...

close all

file_root = '/home/matteo/DG/SIMULATIONS/SQCYL/adapt20/Output/';
file_names = {'adapt20_1-forces-cylinder_sides.h5', 'adapt20_2-forces-cylinder_sides.h5'};

%file_root = '/home/matteo/DG/SIMULATIONS/SQCYL/adapt3/Output/';
%file_names = {'adapt3_1-forces-cylinder_sides.h5', 'adapt3_2-forces-cylinder_sides.h5', 'adapt3_3-forces-cylinder_sides.h5'};


forces_name = '/forces_coeff';
time_name = '/time';
f_data = []; t = []; tend=0;
for i_f = 1:length(file_names)
  f_data_t = h5read([file_root,file_names{i_f}], forces_name);
  t_t = h5read([file_root,file_names{i_f}], time_name);
  f_data = [f_data, f_data_t];
  t = [t, tend+t_t];
  tend = t(end);
end

dt = t(2) - t(1);
c_l = (f_data(1,:)+f_data(10,:));
c_d = (f_data(2,:)+f_data(11,:));
rms_cl = std(c_l);
rms_cd = std(c_d);
cl_me = mean(c_l);
cd_me = mean(c_d);


%build an artificial c_l
omega =0.13;
cl_synth = 2*sin(2*pi*omega*t);
figure
plot(t,c_l)
hold on
plot(t,cl_synth)

%c_l = cl_synth;

cl_mean = zeros(size(c_l));
cl_mean(1) = c_l(1);
for i = 2:length(cl_mean)
    cl_mean(i) = cl_mean(i-1)*((i-1)/i) + c_l(i)*(1/i);
end
figure
plot(t,cl_mean)
func = @(t)  1./(pi*omega*t)*2;
hold on
%plot(t(300:end),func(t(300:end)),'r')
%plot(t(300:end),-func(t(300:end)),'r')

est1 = @(t) 1./(pi*omega*t)*sqrt(2)*rms_cl;
est2 = @(t) 1./(2*omega*t)*rms_cl;
plot(t(300:end),est1(t(300:end))+cl_me,'g')
plot(t(300:end),est2(t(300:end))+cl_me,'k')
plot(t(300:end),-est1(t(300:end))+cl_me,'g')
plot(t(300:end),-est2(t(300:end))+cl_me,'k')


cd_mean = zeros(size(c_d));
cd_mean(1) = c_d(1);
for i = 2:length(cd_mean)
    cd_mean(i) = cd_mean(i-1)*((i-1)/i) + c_d(i)*(1/i);
end

figure
plot(t,cd_mean)
func = @(t)  1./(pi*omega*t)*2;
hold on
%plot(t(300:end),func(t(300:end)),'r')
%plot(t(300:end),-func(t(300:end)),'r')

est1 = @(t) 1./(pi*omega*t)*sqrt(2)*rms_cd;
est2 = @(t) 1./(2*omega*t)*rms_cd;
plot(t(300:end),est1(t(300:end))+cd_me,'g')
plot(t(300:end),est2(t(300:end))+cd_me,'k')
plot(t(300:end),-est1(t(300:end))+cd_me,'g')
plot(t(300:end),-est2(t(300:end))+cd_me,'k')



rms_cl_mean = rms_cl/sqrt(length(c_l));