function [gamma] = find_gamma(y1,Ny)
% function [gamma] = find_gamma(y1,Ny)
%
% Given y1(distance of the first point from the wall)
% and Ny (number of subdivisions in the y-direction),
% this function evaluates gamma (stretching parameter).

 gamma_i = 1;
 y1 = 1-abs(y1); % absolute coordinate of the first point
 gamma = fsolve(@(x) y1-abs(tanh(x*(1-2/Ny))/tanh(x)),gamma_i);

 % points ditribution
 j = 0:Ny;
 y = -tanh(gamma*(1-2*j/Ny))/tanh(gamma);
%%{
% figure, hold on
% plot(gamma,y,'bo')
% xlabel('\gamma'), ylabel('y')
% title('Points distribution')
%%}

return
