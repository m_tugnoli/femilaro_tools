%function get_indicator_test1(filename,res)
close all; clc, clear
%filename_xyz = '/home/matteo/DG/SIMULATIONS/CHANNEL/chan180_m07_mpo_wronggrad_temp/Output/chan180_m07_mpo_wronggrad_temp';
%filename_xyz = '/home/matteo/DG/SIMULATIONS/SQCYL/meshtest2/Output/meshtest2_12';
%filename_xyz = '/home/matteo/DG/SIMULATIONS/SQCYL/ref2/Output/ref2_1';
filename_xyz = '/home/matteo/DG/SIMULATIONS/SQCYL/ref3/Output/ref3_1';
%res1 = 5;
%res2 = 34;
% res1 = 1;
% res2 = 63;
res1 = 3;
res2 = 123;
k_base = 4;
% this is mainly to output more data to evaluate the normalization of 
% anisotropy of the structure function

%% Loading
%think about converting the mat files here
load ([filename_xyz,'-grid.mat'])
load ([filename_xyz,'-base.mat'])

position = [grid.e.xb];
indicator = zeros(1,grid.ne);
format = '%04d';

DDmean = zeros(3,3,6,grid.ne);
load intmat.mat
intmat_sagaut = load ('intmat_sagaut.mat');

%% Indicator calculation
diss_mean = zeros(grid.ne,1);
umean2 = zeros(grid.ne,1);
for it = res1:res2
   uuu = h5read([filename_xyz,'-res-',num2str(it,format),'.h5'], '/uuu');
   td = h5read([filename_xyz,'-res-',num2str(it,format),'.h5'], '/td_diags');
   diss  = td(2,:,:);
   diss_mean = diss_mean + squeeze(diss(1,1,:))*base.p(1,1);
   umean2 = umean2 + ( (squeeze(uuu(3,1,:))*base.p(1,1)).^2 + ...
                       (squeeze(uuu(4,1,:))*base.p(1,1)).^2 + ...
                       (squeeze(uuu(5,1,:))*base.p(1,1)).^2) ./...
                       (squeeze(uuu(1,1,:))*base.p(1,1)).^2;
   
   %==== relative weight indicator  ====
%    indicator = indicator + ind_relative_weight_1(uuu,grid.ne);
%   indicator = indicator + ind_relative_weight_3(uuu,grid,base);
    indicator = indicator + ind_vel_mag(uuu,grid,base);
   
   %==== structure function anisotropy ====
%    [DD,rr] = ind_struct_fun_experimental(uuu,grid,intmat(k_base+1).m,base);
%    DDmean = DDmean + DD;
   
   %==== stuttgart relative weight ====
%    indicator = indicator + ind_stutt_1(uuu, grid, base);
   
   %==== modelled energy ====
   %tau = h5read([filename,'-res-',num2str(it,format),'.h5'], '/td_diags');
   %tau = tau(3:11,:,:);
   %indicator = indicator + ind_model_energy_1( uuu, tau, grid);
   
   %==== Burbeau & Sagaut ===
%    indicator = indicator + ind_sagaut_1(uuu,grid,intmat_sagaut.intmat(k_base+1).m);
end
%  DDmean = DDmean/(res2-res1+1);
%  diss_mean = diss_mean/(res2-res1+1);
%  anisotropy = ind_structfun_anisotropy3(DDmean,diss_mean,umean2,rr,grid.ne);
%  strfun_norm = ind_structfun_norm1(DDmean,grid.ne);
%  isoform_norm = ind_isoform_norm1(DDmean,rr,grid.ne);

indicator = indicator/(res2-res1+1);


%% Print out


%TODO: fix the fact that if the file exists it goes mad
file_output = '/home/matteo/DG/SIMULATIONS/SQCYL/ref3/indicator_tests/norm_test_8.h5';

h5create(file_output,'/indicator',length(indicator),....
                                                       'Datatype','double')
h5write(file_output,'/indicator',indicator)

% h5create(file_output,'/anisotropy',length(anisotropy),....
%                                                        'Datatype','double')
% h5write(file_output,'/anisotropy',anisotropy)
% 
% h5create(file_output,'/strfun_norm',length(strfun_norm),....
%                                                        'Datatype','double')
% h5write(file_output,'/strfun_norm',strfun_norm)
% 
% h5create(file_output,'/isoform_norm',length(isoform_norm),....
%                                                        'Datatype','double')
% h5write(file_output,'/isoform_norm',isoform_norm)



