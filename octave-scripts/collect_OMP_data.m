function DATA = collect_OMP_data(varargin)
% DATA = collect_OMP_data(file1,file2,...)
%
% Collect OpenMP diagnostics from a series of text files.

 CMAP = jet;
 for j=1:nargin
   fid = fopen(varargin{j},'r');
   data = struct();
 
   line = 1;
   while(line~=-1)
 
     line = fgets(fid);
 
     if(length(line)>4)
       if(strcmp(line(1:4),'$OMP')==1)
         % parse the string
         [omp,key,time] = sscanf(line,'%s %s %f',"C");
	 key(find(key==":")) = "_";
         if(isfield(data,key))
           data = setfield(data,key,[getfield(data,key),time]);
         else
           data = setfield(data,key,time);
         end
       end
     end
   end
 
   fclose(fid);
   DATA{j} = data;
   icolor = floor(size(CMAP,1)/2 + (j-1)/nargin*size(CMAP,1));
   icolor = mod(icolor-1,size(CMAP,1))+1;
   color(j,:) = CMAP(icolor,:);
 end

 keys = fieldnames(DATA{1});
 for i=1:length(keys)
   h(i) = figure;
 end
 for i=1:length(keys)
   tmin = 1e100;
   tmax = 0;
   for j=1:nargin
     tmin = min([tmin,getfield(DATA{j},keys{i})]);
     tmax = max([tmax,getfield(DATA{j},keys{i})]);
   end
   Deltat = tmax-tmin;

   figure(h(i));
   for j=1:nargin
     times = getfield(DATA{j},keys{i});
     [nn,tt] = hist(times,50);
     nn(find(nn==1)) = 1.5;
     nn(find(nn==0)) = 1;
     hh = bar(tt,log10(nn),0.5*Deltat/(max(times)-min(times)));
     hold on
     set(hh,"facecolor",color(j,:));
     set(hh,"edgecolor",[0,0,0]);
     legendtx{j} = ["Series ",num2str(j),", mean: ",num2str(mean(times))];
   end
   titlestr = keys{i};
   titlestr(find(titlestr=="_")) = " ";
   title(titlestr);
   legend(legendtx);
   xlabel("time");
   ylabel("log__{10}(N)");
 end

return

