function saveGraph(xdata, ydata, xlab, ylab, filename)

if (length(xdata) ~= length(ydata))
    error("Input vectors for the graph of different length")
end

fid = fopen(filename, "w");
fprintf(fid, "#   %s    %s \n", xlab, ylab)

for i=1:length(xdata)
    fprintf(fid, '%g %g\n', xdata(i), ydata(i))
end

fclose(fid)
