function PV_dgcomp_serial(fname,itime,k,l,outfile,outdir,varargin)
% PV_dgcomp_series(fname,itime,N,k,l,outfile)
% PV_dgcomp_series(fname,itime,N,k,l,outfile,select)
%
% Call PV_dgcomp for a series of files and prepare a .pvd file to
% collect the generated vtk files.
%
% fname must contain a '%' to indicate the subdomain number and a '$'
% to indicate the suffix 'grid', 'base' or 'res-XXXX'.
% EXAMPLE: 'ch_icT-P%-$.octxt'
%
% itime: label(s) of the selected output times
% EXAMPLE: for 'ch_icT-P%-res-0003.octxt', itime=3
% 
% N: number of partitions minus 1 (the counter starts from 0)
% k: FE order
% l: order of interpolation (ca be taken less or equal to k) ??
% outfile: name of the file without extension (string)
% outdir: folder in which to store the results
%
% The optional argument 'select' can be used as follows:
% 0 : no postprocessing
% 1 : total values, i.e. deviations + IO reference state
% 2 : total values; include also all the additional diagnostics
% 3 : primitive variables deviations from IO reference

warning('off','Octave:load-file-in-path');

 % input arguments
 total = 0;
 alldiags = 0;
 primdevs = 0;
 if(nargin>6)
   switch varargin{1}
    case 1
     total = 1;
    case 2
     total = 1;
     alldiags = 1;
    case 3
     primdevs = 1;
    otherwise
     error("Invalid value for 'select'");
   endswitch
 end

 % Summary file
 fnameNX = [outdir,'/',outfile,'.pvd'];
 fid = fopen(fnameNX,'w');
 
 line = '<?xml version="1.0"?>';
 fprintf(fid,'%s\n',line);
 line = '<VTKFile type="Collection" version="0.1" byte_order="BigEndian">';
 fprintf(fid,'%s\n',line);
 line = ' <Collection>';
 fprintf(fid,'%s\n',line);

 % Part loop
 Npos = find(fname=='%');
 for j=1:length(itime)
     fnameN = fname;
     Xpos = find(fnameN=='$');
     fnameNX = [fnameN(1:Xpos-1),'grid',fnameN(Xpos+1:end)];
     grid = load(fnameNX);
     fnameNX = [fnameN(1:Xpos-1),'base',fnameN(Xpos+1:end)];
     base = load(fnameNX);
     fnameNX = [fnameN(1:Xpos-1),'res-',num2str(itime(j),'%.4i'), ...
                fnameN(Xpos+1:end)];
     res = load(fnameNX);

     if(total==1)
       fnameNX = [fnameN(1:Xpos-1),'init',fnameN(Xpos+1:end)];
       ref = load(fnameNX);
       uuu = res.uuu + ref.uuu_ioref;
       uuu(1,:,:) = uuu(1,:,:) + permute(ref.atm_ref.rho,[3 1 2]);
       uuu(2,:,:) = uuu(2,:,:) + permute(ref.atm_ref.e,[3 1 2]);
     elseif(primdevs==1)
       fnameNX = [fnameN(1:Xpos-1),'init',fnameN(Xpos+1:end)];
       ref = load(fnameNX);
       [tmp,uuu,tmp,pitheta] = dgcomp_postp(res.uuu , base.base , ...
                              ref.uuu_ioref , ref.atm_ref , ref.phc);
       % This is a trick to compute pi and theta for the reference
       % state, so that we can plot the deviations.
       [tmp,tmp,tmp,io_pith] = dgcomp_postp(0*res.uuu , base.base , ...
                              ref.uuu_ioref , ref.atm_ref , ref.phc);
     else
       uuu = [res.uuu;res.td_diags(1:2,:,:)];
     end

     fnameNX = [outfile,'-',num2str(itime(j)),'.vtu'];
     fnameNXpath = [outdir,'/',outfile,'-',num2str(itime(j)),'.vtu'];
     if(alldiags)
       PV_dgcomp(grid.grid,base.base,uuu,k,l,fnameNXpath,res.test_name,res.add_diags);
     elseif(primdevs)
       PV_dgcomp(grid.grid,base.base,uuu,k,l,fnameNXpath,res.test_name,pitheta-io_pith);
     else
       PV_dgcomp(grid.grid,base.base,uuu,k,l,fnameNXpath,res.test_name);
     end
     line = ['  <DataSet timestep="', num2str(res.time), ...
             '" file="',fnameNX,'"/>'];
     fprintf(fid,'%s\n',line);
 end

 line = ' </Collection>';
 fprintf(fid,'%s\n',line);
 line = '</VTKFile>';
 fprintf(fid,'%s\n',line);

 fclose(fid);

return

