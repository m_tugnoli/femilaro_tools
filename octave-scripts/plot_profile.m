function plot_profile(section_file,fname,step,np,sgs)
% section_file must be a string, containing section_file's full name
% fname must be a string (e.g. 'out' for file out-P0000-res-0100.octxt)
% step is number associated to the res (100 in the previous example)
% np is the numper of partition minus 1
% plot is a boolean variable
% This function uses extract_data.m

 [data]=extract_statistics(section_file,fname,step,np);
 
 for i = 1:length(data)
   y(i) = data(i).y;
   rho_m(i)  = data(i).values(1);
   rho_f(i)  = data(i).values(2);   
   u_m(i)    = data(i).values(3);  
   v_m(i)    = data(i).values(4);      
   w_m(i)    = data(i).values(5);      
   tau_uu(i) =  data(i).values(6); 
   tau_uv(i) =  data(i).values(7);
   tau_uw(i) =  data(i).values(8);      
   tau_vu(i) =  data(i).values(9);   
   tau_vv(i) =  data(i).values(10);   
   tau_vw(i) =  data(i).values(11);   
   tau_wu(i) =  data(i).values(12);   
   tau_wv(i) =  data(i).values(13);   
   tau_ww(i) =  data(i).values(14);   
   t_m(i)    = data(i).values(15);
   t_f(i)    = data(i).values(16);   
   tau_mod_uu(i) =  data(i).values(17); 
   tau_mod_uv(i) =  data(i).values(18);
   tau_mod_uw(i) =  data(i).values(19);      
   tau_mod_vu(i) =  data(i).values(20);   
   tau_mod_vv(i) =  data(i).values(21);   
   tau_mod_vw(i) =  data(i).values(22);   
   tau_mod_wu(i) =  data(i).values(23);   
   tau_mod_wv(i) =  data(i).values(24);   
   tau_mod_ww(i) =  data(i).values(25);   
 end
  
