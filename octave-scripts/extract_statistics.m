function [data]=extract_statistics(section_file,fname,step,np,varargin)
% section_file must be a string, containing section_file's full name
% fname must be a string (e.g. 'out' for file out-P0000-res-0100.octxt)
% step is number associated to the res (100 in the previous example)
% np is the numper of partition minus 1
%
% added an additional argument to mark the use of hdf5 output files, it's 
% not used, just add 'hdf5' for clarity

 load(section_file)
 n_section=size(SC,2);
 grid_stat=cell2struct(SC,'sections',n_section);
 
 % Collect side_sections position
 y_pos = [];
 for i=1:n_section
   if(strcmp(grid_stat(i).sections.section_type,'point_section'))
     y_sec= grid_stat(i).sections.section_name;
     y_sections(i).y_sec=y_sec;
     y_sections(i).y_pos=grid_stat(i).sections.x(2); %extract the y coordinate
                                                     %which is the same on all
                                                     %section points
%     y_pos = [y_pos , grid_stat(i).sections.x(2)];
   end
 end
 
 if (nargin > 4)
    % Load results from the hdf5 res file
    file_res=[fname,'-res-',num2str(step,'%.4i'),'.h5'];
    results = load(file_res);
    for j=1:length(y_sections)
        for k=1:length(results.sections.point_sections.nregs) %they should be ordered so 
                                                    %a single cycle should be
                                                    %enough, but who knows...
            if(strcmp(strtrim(results.sections.point_sections.section_name(k,:)),...
                       y_sections(j).y_sec))
            data(j).values = results.sections.point_sections.regs(:,:,k) ;% saving data
            y_sections(j).y_sec='empty' ;%In order to avoid repetitions
            data(j).y= y_sections(j).y_pos;
            end                                        
        end
    end
 
 else
 
 % Load results from file res
 for i=1:np
   file_res=[fname,'-P',num2str(i-1,'%.4i'),'-res-',num2str(step,'%.4i'),'.octxt'];
   results(i)=load(file_res);
 end
 
 % Save side_section data in struct data.y
 value=[];
 for i=1:np
   for j=1:length(y_sections) 
     for k=1:size(results(i).sections.sections,2)
       
       if(strcmp(results(i).sections.sections(k).section_name,y_sections(j).y_sec))
         data(j).values = results(i).sections.sections(k).regs ;% saving data
         y_sections(j).y_sec='empty' ;%In order to avoid repetitions
         data(j).y= y_sections(j).y_pos;
       end
     
     end
   end
 end
 end
   
%sort the datas in ascending y order
[dum, ind] = sort([data.y]);
data = data(ind);

