% Logarythmic velocity profile

clear all, close all, clc

y = -1:0.05:1;

Retau = 186; Reb = 2800;
k = 0.41; a = 5.2;


y_p = (y - sign(y)*1)*Retau;

for i=1:length(y)
 if abs(y_p(i))>5
  % logarythmic profile
  u_p(i) = 1/k*log(abs(y_p(i))) + a;
 elseif abs(y_p(i))<=5
  % linear profile
  u_p(i) = y_p(i);
 end
end

% non-dimensional velocity with respect to u bulk
u = u_p.*(Retau/Reb);

% temperature profile
gamma = 1.4; 
Ma = 0.7;
e = (gamma-1) + 1/2*(gamma*Ma^2)*(u.^2);
Ti = 1/(gamma-1)*(e - 1/2*(gamma*Ma^2)*(u.^2));

figure
hold on, grid on
plot(y,u,'b')
xlabel('y'), ylabel('u')
figure
hold on, grid on
plot(y,Ti,'k')
xlabel('y'), ylabel('T')
