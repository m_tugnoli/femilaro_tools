function dgcomp_grid2grid_full(fname,np1,np2)
% function dgcomp_grid2grid_full(fname,np1,np2)
%
% This function has the objective to prepare the ingredients
% for the function dgcomp_grid2grid.
% fname: spec-name of the testcase in exam
% np1: number of partitions of the starting grid
% np2: number of partitions of the new grid
% 
% The new solution is saved in files with spec-name fname_2.

 % Build arrays for the original grid
 for i=1:np1

   fname_grid = [fname,'-P',num2str(i-1,'%.3i'),'-grid.octxt'];
   load(fname_grid);
   grid1{i} = grid;
   ddc_grid1{i} = ddc_grid;
   clear grid ddc_grid

   fname_res = [fname,'-P',num2str(i-1,'%.3i'),'-restart.octxt'];
   res = load(fname_res);
   uuu1{i} = res.uuu;
   clear res

 end
 fname_base = [fname,'-P',num2str(0,'%.3i'),'-base.octxt'];
 load(fname_base);
 base1 = base;
 clear base

 % loop over the partitions of new grid
 for i=1:np2
 
   fname_grid = [fname,'_2-P',num2str(i-1,'%.3i'),'-grid.octxt'];
   load(fname_grid);
   grid2 = grid;
   clear grid ddc_grid

   fname_base = [fname,'_2-P',num2str(i-1,'%.3i'),'-base.octxt'];
   load(fname_base);
   base2 = base;
   clear base

   uuu2 = dgcomp_grid2grid( grid1,ddc_grid1,base1,uuu1, ...
                              grid2,base2 );

   fname_res = [fname,'_2-P',num2str(i-1,'%.3i'),'-restart.octxt'];
   uuu = uuu2;
   save( fname_res, 'uuu' )

 end

return
