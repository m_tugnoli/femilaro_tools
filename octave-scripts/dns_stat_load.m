function [dns] = dns_stat_load(ref_path, reTauDns)

%mean profile
dat = importdata([ref_path, 'Umean.dat'],' ',2);
dns.Umean.y = (dat.data(:,1)+1)*reTauDns;
dns.Umean.val = dat.data(:,2);

%Reynolds stresses
dat = importdata([ref_path, 'tauuu.dat'],' ',2);
dns.tauuu.y = (dat.data(:,1)+1)*reTauDns;
dns.tauuu.val = dat.data(:,2);

dat = importdata([ref_path, 'tauuv.dat'],' ',2);
dns.tauuv.y = (dat.data(:,1)+1)*reTauDns;
dns.tauuv.val = dat.data(:,2);

dat = importdata([ref_path, 'tauvv.dat'],' ',2);
dns.tauvv.y = (dat.data(:,1)+1)*reTauDns;
dns.tauvv.val = dat.data(:,2);

dat = importdata([ref_path, 'tauww.dat'],' ',2);
dns.tauww.y = (dat.data(:,1)+1)*reTauDns;
dns.tauww.val = dat.data(:,2);

%Rms
dat = importdata([ref_path, 'rmsu.dat'],' ',2);
dns.rmsu.y = (dat.data(:,1)+1)*reTauDns;
dns.rmsu.val = dat.data(:,2);

dat = importdata([ref_path, 'rmsv.dat'],' ',2);
dns.rmsv.y = (dat.data(:,1)+1)*reTauDns;
dns.rmsv.val = dat.data(:,2);

dat = importdata([ref_path, 'rmsw.dat'],' ',2);
dns.rmsw.y = (dat.data(:,1)+1)*reTauDns;
dns.rmsw.val = dat.data(:,2);


%tke
dat = importdata([ref_path, 'turbKinEn.dat'],' ',2);
dns.tke.y = (dat.data(:,1)+1)*reTauDns;
dns.tke.val = dat.data(:,2);
