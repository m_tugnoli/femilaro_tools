function [flux] = ev_flux(fname,np,itime)
% function [flux] = ev_flux(fname,np,itime)
%
% This function evaluates the total fluxes through
% a plane coincident with cells' subdivision in 
% y direction.
%
% This is the vector of planes coincident with cells' subdivision
%y = [-1.00000  -0.97500  -0.93361  -0.86661  -0.76193  -0.60722  -0.39630  -0.13834   0.13834   0.39630   0.60722   0.76193   0.86661   0.93361   0.97500   1.00000];

 y = 0.86661;
 toll = 10^(-3);
 gs_idx = [];
 nface = 0;

  % PARTITION LOOP ---
  for pp = 1:np
   % identification of files
   grid_file = strcat(fname,'-P',num2str(pp-1,'%.3i'),'-grid.octxt');
   base_file = strcat(fname,'-P',num2str(pp-1,'%.3i'),'-base.octxt');
   res_file = strcat(fname,'-P',num2str(pp-1,'%.3i'),'-res-',num2str(itime,'%.4i'),'.octxt');
   load(grid_file)
   load(base_file)
   load(res_file)   

   for is=1:grid.ns
     % check if the point belongs to the considered plane
     if ( abs(grid.s(is).xb(2) - y) < toll )
       pos = find( gs_idx == ddc_grid.gs(1,is) );
       
       if ( isempty(pos) ) 
         gs_idx = [gs_idx, ddc_grid.gs(1,is) ];
         nface = nface + 1;
         pos = nface;

         ie_s = grid.s(is).ie(1);
         is_loc = grid.s(is).isl(1);
         i_perm = grid.e(ie_s).pi(is_loc);
         pb = base.pb(:,base.stab(i_perm,:),is_loc);

         wgsa(:,pos) = (grid.s(is).a/base.me.voldm1) * base.wgs;

         face_data(1,pos) = struct(...
           'uuu', uuu(:,:,ie_s) * pb, ...
           'n', grid.e(ie_s).n(:,is_loc) );

         ie_s = grid.s(is).ie(2);
         % ie_s<0 for a domain decomposition (ddc) side
         if ( ie_s > 0 )
           is_loc = grid.s(is).isl(2);
           i_perm = grid.e(ie_s).pi(is_loc);
           pb = base.pb(:,base.stab(i_perm,:),is_loc);
           face_data(2,pos) = struct(...
             'uuu', uuu(:,:,ie_s) * pb, ...
             'n', grid.e(ie_s).n(:,is_loc) );
         end

       else

         ie_s = grid.s(is).ie(1);
         is_loc = grid.s(is).isl(1);
         i_perm = grid.e(ie_s).pi(is_loc);
         pb = base.pb(:,base.stab(i_perm,:),is_loc);
         face_data(2,pos) = struct(...
           'uuu', uuu(:,:,ie_s) * pb, ...
           'n', grid.e(ie_s).n(:,is_loc) );
         
       end
     end
   end
   
  end %---

  % Evaluate the total flux across the considered faces
  flux = 0; 
  for i=1:nface
    % 1st face
    rho(1,:) = face_data(1,i).uuu(1,:);
    qdm = face_data(1,i).uuu(3:5,:);
    p(1,:) = 1/2.5*( face_data(1,i).uuu(2,:) - 0.5*sum(qdm.^2,1)./rho(1,:) );
    % 2nd face
    rho(2,:) = face_data(2,i).uuu(1,:);
    qdm = face_data(2,i).uuu(3:5,:);
    p(2,:) = 1/2.5*( face_data(2,i).uuu(2,:) - 0.5*sum(qdm.^2,1)./rho(2,:) );
    % lambda
    lambda1 = sqrt(1.4*p(1,:)/rho(1,:)) + abs( (face_data(1,i).n' *qdm)./rho(1,:) );
    lambda2 = sqrt(1.4*p(2,:)/rho(2,:)) + abs( (face_data(2,i).n' *qdm)./rho(2,:) );
    lambda = max( lambda1 , lambda2 );
    % flux accumulation
    flux = flux ...
     + ( 0.5*(face_data(1,i).uuu(4,:)+face_data(2,i).uuu(4,:)) ...
     + (lambda/2).*(face_data(1,i).uuu(1,:)*face_data(1,i).n(2) + ...
                    face_data(2,i).uuu(1,:)*face_data(2,i).n(2) ) ) ...
     * wgsa(:,i);
   end

return
