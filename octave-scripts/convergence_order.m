function [rL2_h, rA_h, rL2m_h, rAm_h, e_L2, e_A] = convergence_order(h_vect,p_vect,k_vect,plots)
% function [rL2_h, rA_h, rL2m_h, rAm_h, e_L2, e_A] = convergence_order(h_vect,p_vect,k_vect,plots)
%
% Calculation of the order of convergence in the L2-norm and A-seminorm
% for a scalar variable (i.e. tracer).
%
% h_vect: column vector of grid resolutions
% p_vect: column vector of number of partitions used for each grid
% k_vect: vector of FE orders used for each grid
% plots: if true, plots of the errors are displayed

% Initialization of the output variables
n_grids = length(h_vect); n_k = length(k_vect); 
n_el = zeros(n_grids,1); n_dof_el = zeros(n_k,1);
r_L2 = zeros(n_grids-1,n_k); r_A = zeros(n_grids-1,n_k);

% Initialization of the error variables
e_L2 = zeros(n_grids,n_k); e_A = zeros(n_grids,n_k);

% FE ORDERS LOOP ---
d = 3;
for kk = 1:n_k

 % FE ELEMENTAL DOF
 n_dof_el(kk) = nll_sympoly(d,k_vect(kk));

 % GRID LOOP ---
 for hh = 1:n_grids

  L2 = 0; A = 0; H1 = 0;
  % PARTITION LOOP ---
  for pp = 1:p_vect(hh)

   % identification of files
   grid_file = strcat('ch_icT_h',num2str(hh),'k',num2str(k_vect(kk)),'-P',num2str(pp-1,'%.3i'),'-grid.octxt');
   base_file = strcat('ch_icT_h',num2str(hh),'k',num2str(k_vect(kk)),'-P',num2str(pp-1,'%.3i'),'-base.octxt');
   output_file = strcat('ch_icT_h',num2str(hh),'k',num2str(k_vect(kk)),'-P',num2str(pp-1,'%.3i'),'-res-0100.octxt');

   % number of elements of the grid (once for all FE orders)
   if kk == 1
    grid = load(grid_file);
    n_el(hh,1) = n_el(hh,1) + grid.grid.ne;
    clear grid
   end

   % error^2 computation
   [ee_L2, ee_A, ee_H1] = errors(grid_file,base_file,output_file);
   L2 = L2 + ee_L2^2; A = A + ee_A^2; H1 = H1 + ee_H1^2;

  end %---

  % error computation (square root)
  e_L2(hh,kk) = sqrt(L2); e_A(hh,kk) = sqrt(A); e_H1(hh,kk) = sqrt(H1);
  % degrees of freedom of the mesh
  n_dof(hh,kk) = n_el(hh,1)*n_dof_el(kk);
 
 end %---

 % NUMERICAL ORDER OF CONVERGENCE with respect to h
 rL2_h(1:n_grids-1,kk) = log10(e_L2(2:n_grids,kk)./e_L2(1:n_grids-1,kk))./log10(h_vect(2:n_grids)./h_vect(1:n_grids-1));
 rA_h(1:n_grids-1,kk) = log10(e_A(2:n_grids,kk)./e_A(1:n_grids-1,kk))./log10(h_vect(2:n_grids)./h_vect(1:n_grids-1));
 rH1_h(1:n_grids-1,kk) = log10(e_H1(2:n_grids,kk)./e_H1(1:n_grids-1,kk))./log10(h_vect(2:n_grids)./h_vect(1:n_grids-1));

 % MEAN NUMERICAL ORDER OF CONVERGENCE (CONSIDER THE FIRST AND THE LAST GRID)
 rL2m_h(1,kk) = log10(e_L2(n_grids,kk)./e_L2(1,kk))./log10(h_vect(n_grids)./h_vect(1));
 rAm_h(1,kk) = log10(e_A(n_grids,kk)./e_A(1,kk))./log10(h_vect(n_grids)./h_vect(1));
 rH1m_h(1,kk) = log10(e_H1(n_grids,kk)./e_H1(1,kk))./log10(h_vect(n_grids)./h_vect(1));

 % NUMERICAL ORDER OF CONVERGENCE with respect to NDOF
 rL2_N(1:n_grids-1,kk) = log10(e_L2(2:n_grids,kk)./e_L2(1:n_grids-1,kk))./log10(n_dof(1:n_grids-1,kk)./n_dof(2:n_grids,kk));
 rH1_N(1:n_grids-1,kk) = log10(e_H1(2:n_grids,kk)./e_H1(1:n_grids-1,kk))./log10(n_dof(1:n_grids-1,kk)./n_dof(2:n_grids,kk));
 rA_N(1:n_grids-1,kk) = log10(e_A(2:n_grids,kk)./e_A(1:n_grids-1,kk))./log10(n_dof(1:n_grids-1,kk)./n_dof(2:n_grids,kk));

end %---

% PLOTS ---
if plots == 1
 color = 'kbr'; marker = 'os^';
 % L2 norm vs h
 figure(1), hold on
 for kk = 1:n_k
  plot(log10(1./h_vect),log10(e_L2(:,kk)),...
   ['-',color(kk),marker(kk)],'MarkerFaceColor',color(kk))
 end
 title('error L2-norm'), ylabel('log_{10}||e||_{L2}'), xlabel('log_{10} h')
 legend('k=1','k=2','k=3')

 % A seminorm vs h
 figure(2), hold on
 for kk = 1:n_k
  plot(log10(1./h_vect),log10(e_A(:,kk)),...
   ['-',color(kk),marker(kk)],'MarkerFaceColor',color(kk))
 end
 title('error A-seminorm'), ylabel('log_{10}|e|_A'), xlabel('log_{10} h')
 legend('k=1','k=2','k=3')
end 
% ---

return
