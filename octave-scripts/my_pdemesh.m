function my_pdemesh(h,grid,color)
% my_pdemesh(h,grid,color)

 figure(h);

 switch(grid.d)

  case 1 % plot the elements
   X = zeros(grid.ne,2);
   for ie=1:grid.ne
     X(ie,1) = grid.v(grid.e(ie).iv(1)).x;
     X(ie,2) = grid.v(grid.e(ie).iv(2)).x;
   end
   plot(X',0*X',[color "o-"]);

  case 2 
   P = zeros(2,grid.nv);
   E = zeros(5,grid.nb);
   T = zeros(4,grid.ne);
   for iv=1:grid.nv
     P(:,iv) = grid.v(iv).x;
   end
   for ie=1:grid.ne
     T(1:3,ie) = grid.e(ie).iv;
     T(4  ,ie) = 1;
   end
   hhh = pdemesh(P,E,T);
   view(2);
   %set(hhh,'linecolor',color);

   %X = zeros(grid.ns,2,2); % plot the sides
   %for is=1:grid.ns
   %  for i=1:2
   %    X(is,1,i) = grid.v(grid.s(is).iv(1)).x(i);
   %    X(is,2,i) = grid.v(grid.s(is).iv(2)).x(i);
   %  end
   %end
   %plot(X(:,:,1)',X(:,:,2)',[color ";;"]);
    
  case 3 % plot the edges of the sides (each edge is plotted twice)
   X = zeros(grid.ns,4,3);
   for is=1:grid.ns
     for i=1:3
       X(is,1,i) = grid.v(grid.s(is).iv(1)).x(i);
       X(is,2,i) = grid.v(grid.s(is).iv(2)).x(i);
       X(is,3,i) = grid.v(grid.s(is).iv(3)).x(i);
       X(is,4,i) = grid.v(grid.s(is).iv(1)).x(i);
     end
   end
   plot3(X(:,:,1)',X(:,:,2)',X(:,:,3)',[color ";;"]);
   
  otherwise
   error("It's not possible to print grid with more than 3 dimensions");
 end

 axis equal
   
return

