function [xxx,ui] = DG_plot(fname,np1,itime)
% function [xxx,ui] = DG_plot(fname,np1,itime)
%
% This function plot the instantaneous profile
% for selected quantities in y-direction

 set_plot_defaults

 % Build arrays for the original grid
 for i=1:np1
   fname_grid = [fname,'-P',num2str(i-1,'%.3i'),'-grid.octxt'];
   load(fname_grid);
   grid1{i} = grid;
   ddc_grid1{i} = ddc_grid;
   clear grid ddc_grid
 end
 fname_base = [fname,'-P',num2str(0,'%.3i'),'-base.octxt'];
 load(fname_base);
 base1 = base;
 clear base

 % Define the interpolating line
 np = 300; % number of points on each line
 xxx = zeros(3,np);
 xxx(1,:) = 0;
 xxx(2,:) = linspace(-1,1,np);
 xxx(3,:) = 0;

 ui_xz = zeros(5,np+1);
 % recover the res field file
   for j=1:np1
     fname_res = [fname,'-P',num2str(j-1,'%.3i'),'-res-',num2str(itime,'%.4i'),'.octxt'];
     res = load(fname_res);
     uuu1{j} = res.uuu;
     clear res
   end
   ui = dgcomp_interpolation( grid1,ddc_grid1,base1,uuu1,xxx );

 % Figures of selected quantities
 % quantità di moto
 figure, hold on
 plot(xxx(2,:),ui(3,:),'-k')
 xlabel('y')
 ylabel('\rho u')
 ylim([0 1.4])
 print -depsc2 inst_u
  % energia
 figure, hold on
 plot(xxx(2,:),ui(2,:),'-k')
 xlabel('y')
 ylabel('\rho e')
 print -depsc2 inst_e

return
