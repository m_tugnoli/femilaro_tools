function [p_new,e_new,t_new] = reflect_grid(p,e,t,refl,marker,remark)
% [p_new,e_new,t_new] = reflect_grid(p,e,t,refl,marker,remark)
%
% Creates a new grid applying a "reflection" refl to a given grid,
% fixing the side identified by marker.
%
% In fact, the transformation can be arbitrary and not necessarily a
% reflection, and must be prescribed as
%
%    x_new = refl(x)
%
% where x and x_new are d-dimensional coordinates. All the vertexes
% belonging to the side with index marker are not duplicated, while
% all the other vertexes as well as all the elements are duplicated.
%
% Concerning the sides, those belonging to the selected marker are
% eliminated, while all the other ones are duplicated and receive new
% markers using the function remark.
%
% Consider that, for instance, in 3D a reflection along the plane x=x0
% is represented as
%
%   refl = @(x)( [ -(x(1)-x0)+x0 ; x(2) ; x(3) ] )
%
% The function remark can be, for instance:
%
%   remark = @(m)( max_old_marker + m )

 d  = size(p,1);
 nv = size(p,2);

 % Vertexes on the selected side
 isid = find( e(d+3,:)==marker );
 ifix = unique( e(1:d,isid)(:) );
 inew = setdiff( [1:nv] , ifix );

 % Create the new vertexes

 p_new = zeros(d,2*nv-length(ifix));
 p_new(:,1:nv) = p;

 new_v = zeros(1,nv);
 for iiv=1:length(ifix)
   % these vertexes are not duplicated
   iv = ifix(iiv);
   new_v(iv) = iv; 
 end

 nv_new = nv;
 for iiv=1:length(inew)
   nv_new = nv_new + 1; % create a new node
   iv = inew(iiv);
   new_v(iv) = nv_new; 
   p_new(:,nv_new) = refl(p(:,iv));
 end

 % Create the new elements
 ne = size(t,2);
 t_new = zeros(size(t,1),2*ne);
 t_new(:,1:ne) = t;
 for ie=1:ne
   t_new(:,ne+ie) = [new_v( t(1:d+1,ie) )' ; t(d+2:end,ie)];
 end

 % Create the new sides (and delete the fixed one)
 e(:,isid) = [];
 e_new = [ e , e ]; % the second half will now be changed
 for is=size(e,2)+1:2*size(e,2)
   e_new( 1:d , is ) = new_v( e_new( 1:d , is ));
   e_new( d+3 , is ) = remark(e_new( d+3 , is ));
 end

return

