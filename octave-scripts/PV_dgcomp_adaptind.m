function data = PV_dgcomp_adaptind(gridname, basename, adaptname, outname, k,l)
% Very crude and simple implementation for hdf5 adaptive results
%
%Just pass the name of the output grid, the name of the base, the 
%name of the adaptive polynomial distribution, the name of the output
% and then left k and l equal 1
% 

 part_format = '%.4i'

 grid = load(gridname);
 fprintf('...loaded grid\n');
 fflush(stdout);
 bases = load(basename);
 base = bases.base(k+1);
 fprintf('...loaded base\n');
 fflush(stdout);
 adaptdata = load(adaptname);
 if (~any(strcmp(fieldnames(adaptdata),'deg_lim')))                                                                                                                                                                               
   adaptdata.deg_lim = adaptdata.edeg;                                                       
  end 
 if (~any(strcmp(fieldnames(adaptdata),'indicator')))                                                                                                                                                                               
   adaptdata.indicator = adaptdata.adaptind_val;                                                       
  end 
 fprintf('...loaded adapt data\n');
 fflush(stdout);
 
 uuu = zeros(2,4,grid.grid.ne);
 uuu(1,1,:) = adaptdata.deg_lim;
 uuu(2,1,:) = adaptdata.indicator;
 uuu = 1/base.p(1,1)*uuu;
 varnames = {'degree','indicator'};
 outname = ['./Postprocessing/',outname,'.vtu'];
 
 diags = zeros(0,size(uuu,2),size(uuu,3)); % change this!!!

 % Add the same vars as diagnostics
 %diags(end+1,1,:) = 1/base.p(1,1)*permute(adaptdata.deg_lim,[3 1 2]);
 %diags(end+1,1,:) = 1/base.p(1,1)*permute(adaptdata.indicator,[3 1 2]);
 %varnames{end+1} = 'Degree, diag';
 %varnames{end+1} = 'indicator, diag';

 PV_dgcomp_nscalars(grid.grid,base,uuu,k,l,outname,'pippo', ...
               diags,varnames);


return

