function avg_time_turb_kin_en(fstatname,gstatname,t0,tf,step,outfile)
% avg_time_turb_kin_en.m(fstatname,gstatname,itime,outfile)
%
% Read the statistics files and compute the volume average of 
% turbulent kinetic energy in time
%
% fstatname 
% EXAMPLE: Ma=1.5_anis-P000-stat-0017.octxt -> 'Ma=1.5_anis'
%
% gstatname file containing the grid for statistics
%
% EXAMPLE: for 'Ma=1.5_anis-P000-stat-0003.octxt', itime=3
%

 load([gstatname,'.octxt']);
 
 tt(1)=0;
 jj=1;
 avg_en_turb=0;
 avg_en_turb_old=0;
 for j=t0:step:tf
   fstatnameN = [fstatname,'-P000-stat-',num2str(j,'%.4i'),'.octxt'];
   load(fstatnameN);
   en_kin=stat.td.kin(:,stat.td.turb_res);
   en_kin_int=trapz(yp,en_kin);
   avg_en_turb=en_kin_int*(step/j) - avg_en_turb_old*(1-step/j)
   e_time(jj,:)=[time avg_en_turb];
   avg_en_turb_old=avg_en_turb;
   jj=jj+1;
 end
 plot(e_time(:,1),e_time(:,2));
 save(outfile,'e_time');

return
 
