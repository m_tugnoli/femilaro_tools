function am_I_efficient(casename, instant, processors)

n_procs = processors
format = '%04d';
limit_tot = [];
for ipart = 1:n_procs
basename = ['./Output/',casename,'-P',num2str(ipart-1,format),'-res-',num2str(instant,format),'.octxt'];
load(basename)
limit_tot = [limit_tot, deg_lim];
end

dofs = [3 10 20 35];
max_dofs = length(limit_tot)*dofs(end);
min_dofs = length(limit_tot)*dofs(2);
act_dofs = 0;
for i=1:length(limit_tot)
   act_dofs = act_dofs+dofs(limit_tot(i));
end
fprintf('Max dofs: %d, Min dofs(deg 2): %d, Act dofs: %d \n',max_dofs, min_dofs, act_dofs)
fprintf('Reduction from max: %e \n', (act_dofs-max_dofs)/max_dofs*100)
