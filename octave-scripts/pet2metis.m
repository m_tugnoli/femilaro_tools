function pet2metis (p, e, t, d, filename)
  
  fid = fopen (filename, "w");
  if (fid >= 0)
    fprintf (fid, "%d\n", columns(t));
    if (d == 3)
      fprintf (fid, "%d %d %d %d\n", t(1:4, :));
    elseif (d == 2)
      fprintf (fid, "%d %d %d\n", t(1:3, :));
    else
      error (["pet2metis: can only be used for 2d or 3d "...
	      "meshes of simplices"])
    endif
  else
    error (["pet2metis: could not"...
	    " open file %s for writing"], 
	   filename)
  endif
  fclose (fid);
  
endfunction