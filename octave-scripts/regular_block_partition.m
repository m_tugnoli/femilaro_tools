function [idx] = regular_block_partition(B,d,p,t)
% function [idx] = regular_block_partition(B,d,p,t)
%
% Build the index for partitioning a 3D structured grid 
% using B(1) blocks in x, B(2) blocks in y and 
% B(3) blocks in z.
% 
% d: spatial dimension
% p,t : points and elements of the grid
% idx: indexes of the subdomain

block_number = B(1)*B(2)*B(3);
block_index = 0:block_number-1;

% domain limits and dimensions
min_limits = [min(p(1,:)); min(p(2,:)); min(p(3,:))];
max_limits = [max(p(1,:)); max(p(2,:)); max(p(3,:))];
L = abs(max_limits - min_limits);

% block dimensions
dB = L./B'; 

% loop over elements
ne = size(t,2);
for i=1:ne

  % barycenter
  xb = sum(p(:,t(1:4,i)),2)/4; 
  % reference system with origin in (0,0,0)
  xb = xb - min_limits;
  % identifying the position of the element
  iB = floor(xb./dB);
  % calculating the block position
  block_position = iB(1)*B(2)*B(3) + iB(2)*B(3) + iB(3);
  % index of the block
  idx(i) = block_position;

end

return
