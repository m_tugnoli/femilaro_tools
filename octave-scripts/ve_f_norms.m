function [fn1,fn2,fninf] = ve_f_norms(grid,xb,vb,fff)
% Compute various norms of the distribution function for the Vlasov
% equation; this can be used to compute error norms.

 fn1 = 0;
 fn2 = 0;
 fninf = 0;
 for ie=1:grid.ne

   wgx = grid.gx.e(grid.e(ie).e1(1)).vol * xb.wgld;
   wgv = grid.gv.e(grid.e(ie).e1(2)).vol * vb.wgld;

   n1e = wgx * abs(fff(:,:,ie)) * wgv';
   n2e = wgx * (fff(:,:,ie).^2) * wgv';
   nie = max(max(abs(fff(:,:,ie))));

   fn1 = fn1 + n1e;
   fn2 = fn2 + n2e;
   fninf = max(fninf,nie);

 end

 fn2 = sqrt(fn2);

return

