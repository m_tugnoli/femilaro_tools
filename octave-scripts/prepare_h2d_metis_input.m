function prepare_h2d_metis_input(t,mfile)
% prepare_h2d_metis_input(t,mfile)
%
% Prepare a file mfile which can be used to partition the grid
% described by the connectivity t, which has has the same structure
% returned by grid_h2d_gmsh2oct.
%
% A weight is assigned to the elements, given by the number of nodes.
% This is specified according to section 4.1.2 in the metis manual,
% namely one integer weight is added before listing the element
% vertices.
%
% Remark: in t, the element ordering is defined by t{:}.ie, which
% could be in any arbitrary order. metis, on the other hand, deduces
% the element number from the position at which is appears in the
% file. This means that we need to write the elements in the order
% specified by t{:}.ie.


 % Count the total number of elements
 ne = 0;
 mpol = 0; % maximum number of sides
 for i=1:length(t)
   ne = ne + length(t{i}.ie);
   mpol = max(mpol,size(t{i}.it,1));
 end

 % Pack the elements in the right order
 tm = int64(zeros(ne,mpol+1));
 ie = 0;
 for i=1:length(t)
   poly = size(t{i}.it,1);
   for iie=1:size(t{i}.it,2)
     ie = t{i}.ie(iie);
     tm(ie,1) = poly;
     tm(ie,2:1+poly) = t{i}.it(:,iie)';
   end
 end

 fid = fopen(mfile,'w');

 % First line: total number of elements and 1 weight
 fprintf(fid,'%d %d\n',[ne 1]);

 % now the elements
 for ie=1:size(tm,1)
   poly = tm(ie,1);
   fmt = '%d'; % weight
   for j=1:poly
     fmt = [ fmt , ' %d' ];
   end
   fmt = [ fmt , '\n' ];

   fprintf(fid,fmt,tm(ie,1:1+poly));
 end

 fclose(fid);

return

