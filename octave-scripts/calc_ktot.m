function [ktot] = calc_ktot(y, k);

%integrazione con trapezi
sum_basis = k(2:end) + k(1:end-1);
height = y(2:end) -y(1:end-1);

ktot = sum(0.5*sum_basis.*height);
