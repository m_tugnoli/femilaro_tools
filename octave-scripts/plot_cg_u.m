function h = plot_cg_u(grid,base,dofs,U)
% h = plot_cg_u(grid,base,dofs,U)
%
% Use this function for continuous polinomial spaces.

 h = plot_u(grid,base,U(dofs(:)));
 
return

