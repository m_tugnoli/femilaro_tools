function confronto(np,res,file,grid_path,varargin)
% np:  number of partitions minus 1 (the counter starts from 0)
% res: time selected for statistics
% file: if equal to 1 print a file containing statistics
% grid_path: path to stats grid file

clc
close all

set (findobj (gca, "-property", "keylabel"), "interpreter",   
"none") 

%Path to octave-scripts
%addpath /home/michele/FEMILARO/Simulazioni/octave-scripts

%DNS

 % reystress
load('/home/michele/DNS/Kim, Moi, Moser/chan180/profiles/chan180.reystress')
 y_DNS=chan180(:,2);
 tau_uu_DNS=chan180(:,3);
 tau_vv_DNS=chan180(:,4);
 tau_ww_DNS=chan180(:,5);
 tau_uv_DNS=chan180(:,6);
 tke_DNS=tau_uu_DNS+tau_vv_DNS+tau_ww_DNS;
 % viscs
load('/home/michele/DNS/Kim, Moi, Moser/chan180/profiles/chan180.means')
 u_media_DNS=chan180(:,3);
 w_media_DNS=chan180(:,5);
 fm_DNS=trapz(y_DNS,u_media_DNS);

k_u= fm_DNS/178.77;
k_tau=k_u^2;

for j=1:nargin -4
%First

  cd(varargin{j})

  [data]=extract_statistics(grid_path,['M_0.2_',varargin{j}],res,np);
 
  % Preliminary cycle to determine y and interpolation factor
  for i = 1:length(data)/2+1
    y(i) = (1+data(i).y)*178.77;
  end

  for i=1:length(data)
    u_media(i).case(j) =  data(i).values(3)*k_u;
    v_media(i).case(j) =  data(i).values(4)*k_u;
    w_media(i).case(j) =  data(i).values(5)*k_u;
    tau_uu(i).case(j) =  data(i).values(6)*k_tau; 
    tau_uv(i).case(j) =  data(i).values(7)*k_tau;
    tau_vv(i).case(j) =  data(i).values(10)*k_tau;   
    tau_ww(i).case(j) =  data(i).values(14)*k_tau;   
    tau_mod_uu(i).case(j) =  data(i).values(17)*k_tau; 
    tau_mod_uv(i).case(j) =  data(i).values(18)*k_tau;
    tau_mod_vv(i).case(j) =  data(i).values(21)*k_tau;   
    tau_mod_ww(i).case(j) =  data(i).values(25)*k_tau;
    tke(i).case(j) = tau_uu(i).case(j)+tau_vv(i).case(j)+ tau_ww(i).case(j)+tau_mod_uu(i).case(j)+tau_mod_vv(i).case(j)+tau_mod_ww  (i).case(j);
  end


  cd ../

end 

for j=1:nargin -4
  for i=1:length(data)/2+1
    tau_uu_mean(i).case(j)=(tau_uu(i).case(j)+tau_uu(end +1-i).case(j))/2;
    tau_vv_mean(i).case(j)=(tau_vv(i).case(j)+tau_vv(end +1-i).case(j))/2;
    tau_ww_mean(i).case(j)=(tau_ww(i).case(j)+tau_ww(end +1-i).case(j))/2;
    tau_uv_mean(i).case(j)=(tau_uv(i).case(j)-tau_uv(end +1-i).case(j))/2; 
    tau_mod_uu_mean(i).case(j)=(tau_mod_uu(i).case(j)+tau_mod_uu(end +1-i).case(j))/2;
    tau_mod_vv_mean(i).case(j)=(tau_mod_vv(i).case(j)+tau_mod_vv(end +1-i).case(j))/2;
    tau_mod_ww_mean(i).case(j)=(tau_mod_ww(i).case(j)+tau_mod_ww(end +1-i).case(j))/2;
    tau_mod_uv_mean(i).case(j)=(tau_mod_uv(i).case(j)-tau_mod_uv(end +1-i).case(j))/2; 
    u_media_mean(i).case(j)=(u_media(i).case(j)+u_media(end +1-i).case(j))/2;
    w_media_mean(i).case(j)=(w_media(i).case(j)+w_media(end +1-i).case(j))/2;
    tke_mean(i).case(j)=(tke(i).case(j)+tke(end+1-i).case(j))/2;
  end
end

if(file==1)
  for j=1:nargin -4
    fid=fopen(['dati_M_0.2_',varargin{j}],'w');
    fprintf(fid,'%% y, tau_uu, tau_uu_mod, tau_ww, tau_ww_mod,tau_ww, tau_ww_mod, tau_uv, tau_uv_mod, tke, u_mean, v_mean, w_mean, rho_mean,rho_rms, t_mean, t_rms \n');

    for i=1:length(data)/2+1
      fprintf(fid,'%f   ',y(i));
      fprintf(fid,'%f   ',tau_uu_mean(i).case(j)+tau_mod_uu_mean(i).case(j));
      fprintf(fid,'%f   ',tau_vv_mean(i).case(j)+tau_mod_vv_mean(i).case(j));
      fprintf(fid,'%f   ',tau_ww_mean(i).case(j)+tau_mod_ww_mean(i).case(j));
      fprintf(fid,'%f   ',tau_uv_mean(i).case(j)+tau_mod_uv_mean(i).case(j));
      fprintf(fid,'%f   ',tke_mean(i).case(j));
      fprintf(fid,'%f   ',u_media_mean(i).case(j));
      fprintf(fid,'%f \n',w_media_mean(i).case(j));
    end

    fclose(fid)
  end
end
%cd visc/

%[data_visc]=extract_statistics('../grid_8x16x12_192p-stats.octxt','M_0.2_visc',res,191);
 
% Preliminary cycle to determine y and interpolation factor
 
% for i=1:length(data_visc)
%   u_media_visc(i) =  data_visc(i).values(3)*k_u;
%   v_media_visc(i) =  data_visc(i).values(4)*k_u;
%   w_media_visc(i) =  data_visc(i).values(5)*k_u;
%   tau_uu_visc(i) =  data_visc(i).values(6)*k_tau; 
%   tau_uv_visc(i) =  data_visc(i).values(7)*k_tau;
%   tau_vv_visc(i) =  data_visc(i).values(10)*k_tau;   
%   tau_ww_visc(i) =  data_visc(i).values(14)*k_tau;   
%   tau_mod_uu_visc(i) = 0; 
%   tau_mod_uv_visc(i) =  0;
%   tau_mod_vv_visc(i) =  0;   
%   tau_mod_ww_visc(i) =   0;
%   tke_visc(i) = tau_uu_visc(i)+tau_vv_visc(i)+ tau_ww_visc(i);%+tau_mod_uu_visc(i)+tau_mod_vv_visc(i)+tau_mod_ww_visc(i);
% end
%for i=1:length(data_visc)/2+1
%  tau_uu_visc_mean(i)=(tau_uu_visc(i)+tau_uu_visc(end +1-i))/2;
%  tau_vv_visc_mean(i)=(tau_vv_visc(i)+tau_vv_visc(end +1-i))/2;
%  tau_ww_visc_mean(i)=(tau_ww_visc(i)+tau_ww_visc(end +1-i))/2;
%  tau_uv_visc_mean(i)=(tau_uv_visc(i)-tau_uv_visc(end +1-i))/2;
%  u_media_visc_mean(i)=(u_media_visc(i)+u_media_visc(end +1-i))/2;
%  w_media_visc_mean(i)=(w_media_visc(i)+w_media_visc(end +1-i))/2;
%  tke_visc_mean(i)=(tke_visc(i)+tke_visc(end+1-i))/2;
%end

%%% Plot %%%
col  = { '-ro', '-go', '-bo','-mo','-co', '-yo'};

% tau_uu

hold on
for j=1:nargin-4
  % Need this in order to fix an octave bug (cs list cannot be further indexed)
  for i=1:length(tau_uu_mean)
      var(i)=tau_uu_mean(i).case(j) + tau_mod_uu_mean(i).case(j);
  end      
 plot(y,var,'LineWidth',2,col{j})
end

plot(y_DNS,tau_uu_DNS,'--k')
legend(varargin{:},'DNS')
 xlim([0 180])
%tau_vv

figure()
hold on
for j=1:nargin-4
  % Need this in order to fix an octave bug (cs list cannot be further indexed)
  for i=1:length(tau_vv_mean)
      var(i)=tau_vv_mean(i).case(j) + tau_mod_vv_mean(i).case(j);
  end      
 plot(y,var,'LineWidth',2,col{j})
end
plot(y_DNS,tau_vv_DNS,'--k')
 legend(varargin{:},'DNS')
 xlim([0 180])

% tau_ww

figure()
hold on
for j=1:nargin-4
  % Need this in order to fix an octave bug (cs list cannot be further indexed)
  for i=1:length(tau_ww_mean)
      var(i)=tau_ww_mean(i).case(j) + tau_mod_ww_mean(i).case(j);
  end      
 plot(y,var,'LineWidth',2,col{j})
end
plot(y_DNS,tau_ww_DNS,'--k')
legend(varargin{:},'DNS')
 xlim([0 180])

%tau_uv

figure()
hold on
for j=1:nargin-4
  % Need this in order to fix an octave bug (cs list cannot be further indexed)
  for i=1:length(tau_uv_mean)
      var(i)= -tau_uv_mean(i).case(j) - tau_mod_uv_mean(i).case(j);
  end      
hl= plot(y,var,'LineWidth',2,col{j})
end
plot(y_DNS,-tau_uv_DNS,'--k')
legend(varargin{:},'DNS')
 xlim([0 180])

%tke 

figure()
hold on
for j=1:nargin-4
  % Need this in order to fix an octave bug (cs list cannot be further indexed)
  for i=1:length(tke_mean)
      var(i)= tke_mean(i).case(j);
  end      
 plot(y,var,'LineWidth',2,col{j})
end
plot(y_DNS,tke_DNS,'--k')
legend(varargin{:},'DNS')
 xlim([0 180])

%u_media

figure()
hold on
for j=1:nargin-4
  % Need this in order to fix an octave bug (cs list cannot be further indexed)
  for i=1:length(u_media_mean)
      var(i)= u_media_mean(i).case(j);
  end      
 semilogx(y,var,'LineWidth',2,col{j})
end
semilogx(y_DNS,u_media_DNS,'--k')
legend(varargin{:},'DNS')
 %xlim([0 180])

%w_media

figure()
hold on
for j=1:nargin-4
  % Need this in order to fix an octave bug (cs list cannot be further indexed)
  for i=1:length(w_media_mean)
      var(i)= w_media_mean(i).case(j);
  end      
 plot(y,var,'LineWidth',2,col{j})
end
plot(y_DNS,w_media_DNS,'--k')
legend(varargin{:},'DNS')
 xlim([0 180])

