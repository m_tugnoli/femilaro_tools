function dst = distance_from_line(xp,xl)
% dst = distance_from_line(xp,xl)
%
% Compute the distance of a set of points from a line, provided as a
% collection of segments.
% xp: points for which the distance is desired (one column per point)
% xl: points defining the line segments (one column per point)
%
% Example: compute the distance of x1=[1;1;1] and x2=[2;3;4] from the
% line defined by the tree segments p2-p1, p3-p2, p4-p3:
%
%   dst = distance_from_line( [x1,x2] , [p1,p2,p3,p4] )
%  
%
% The algorithm is very simple:
% a) compute the distance of each point from each segment
% b) for each point, take the minimum over all the segments
%
% Some geometric considerations:
%
% *) given a segment B-A and a point P, the plane passing through P
%  and orthogonal to the segment has equation
%   (x-P) . (B-A) = 0
%
% *) the intersection of this plane with the (prolongation of the)
%  segment is obtained setting  x = A + (B-A)t  so that
%   |B-A|^2 t = (P-A) . (B-A)
%
% *) if t belongs to [0,1], then the distance is |P-(A+(B-A)t)|
%  otherwise the distance is the distance from either A or B
% 

 A   = xl(:,1:end-1);
 B   = xl(:,2:end  );
 BA  = B - A;           % B-A
 BA2 = sum( BA.^2 , 1); % |B-A|^2

 nl = size(A,2);  % number of segments
 np = size(xp,2); % number of points

 dst_i = zeros(nl,1);

 for i=1:np % loop over the points

   t = sum( (xp(:,i) - A) .* BA , 1 ) ./ BA2; % using broadcast

   % determine the weights for the intersection point
   tB = max( min( t , 1 ) , 0 );
   tA = 1 - tB;

   % compute the distance
   dst(i) = min( sum( ( xp(:,i) - (tA.*A + tB.*B) ).^2 , 1) );

 end

 dst = sqrt(dst);

return
