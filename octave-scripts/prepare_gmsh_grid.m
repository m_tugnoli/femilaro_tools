function prepare_gmsh_grid(caso,N)


mpmetis = '/usr/local/bin/mpmetis'; 


d=3;
%[p,e,t]=grid_gmsh2oct(3,[caso,'.msh'],[0 0 0 0 0 2 4 0 0 0 0 0 6 1 0 0 0 0 5 0 3]);
[p,e,t]=grid_gmsh2oct(3,[caso,'.msh'],[0 2 3 4 5 6 1 ]);
save([caso,'.octxt'],'d','p','e','t');

%save(['chan','.octxt'],'d','p','e','t');

% 5) partition the grid
% to avoid self-intersection partitions (due to periodicity), the domain
% is cut into four blocks, each of which is then partitioned by METIS
idx0 = zeros(1,size(t,2));
clear idxB
for i=1:4
  idxB{i} = [];
end
for ie=1:size(t,2)
  xb = sum(p(:,t(1:4,ie)),2)/4;
  if(xb(1)<=max(p(1,:))/2)
    if(xb(3)<=max(p(3,:))/2)
      idx0(ie) = 0;
      idxB{1} = [idxB{1},ie];
    else
      idx0(ie) = 1;
      idxB{2} = [idxB{2},ie];
    end
  else
    if(xb(3)<=max(p(3,:))/2)
      idx0(ie) = 2;
      idxB{3} = [idxB{3},ie];
    else
      idx0(ie) = 3;
      idxB{4} = [idxB{4},ie];
    end
  end
end

clear idx
for i=1:length(idxB)
  tt = t(1:4,idxB{i});
  fid = fopen('tmp-file','w');
  fprintf(fid,'%d\n',size(tt,2));
  fprintf(fid,'%d %d %d %d\n',tt); % column major
  fprintf(fid,'\n\n');
  fclose(fid);
  system([mpmetis,' -gtype=dual tmp-file ',num2str(N)]);
  idxi = load(['tmp-file.epart.',num2str(N)]);
  idx(idxB{i}) = idxi + (i-1)*N;
end


pf = [
2              1  
4              6 
-max(p(1,:))   0
0              0 
0       max(p(3,:))                 
];

grids = grid_partition_old(p,e,t,idx,pf);

% 6) write the grid
d = 3;
for i=1:size(grids,2)
  p=grids(i).p; t=grids(i).t; e=grids(i).e; ivl2ivn=grids(i).ivl2ivn;
  save([caso,'.',num2str(i-1,'%.4d')],'d','p','e','t','ivl2ivn');
end

end 
