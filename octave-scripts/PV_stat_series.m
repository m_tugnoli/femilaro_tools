function PV_stat_series(fname,itime,outfile)
% function PV_stat_series(fname,itime,outfile)
% 
% Temporal collection of statistics file
% fname = 'ch_lam' (example)
% itime: vector of temporal step to be considered
% outfile: string with name of the output file,
%          without extension (example: 'chL-stat')

 % Name of grid file
 fname_grid = [fname,'-P000-s_grid.octxt'];
 load(fname_grid);

 %---------------------------------
 % Summary file
 fnameNX = [outfile,'.pvd'];
 fid = fopen(fnameNX,'w');
 
 line = '<?xml version="1.0"?>';
 fprintf(fid,'%s\n',line);
 line = '<VTKFile type="Collection" version="0.1" byte_order="BigEndian">';
 fprintf(fid,'%s\n',line);
 line = ' <Collection>';
 fprintf(fid,'%s\n',line);

 %---------------------------------
 % Temporal loop
 for j=1:length(itime)

   % Stat file name
   fname_stat = [fname,'-P000-stat-',num2str(itime(j),'%.4i'),'.octxt'];
   load(fname_stat);

   % Generate a .vtr file
   fnameNX = [outfile,'-',num2str(j),'.vtr'];

%   PV_stat(s_grid,stat,fnameNX); % LAMINAR
%   PV_stat_new(s_grid,stat,fnameNX);
   PV_stat_last(s_grid,stat,fnameNX); % VISCOUS
%   PV_stat_essential(s_grid,stat,fnameNX);

   % Insert the file in the temporal collection
   line = ['  <DataSet timestep="', num2str(time), ...
           '" group="" part="" file="',fnameNX,'"/>'];
   fprintf(fid,'%s\n',line);

 end

 %---------------------------------
 % Close the file
 line = ' </Collection>';
 fprintf(fid,'%s\n',line);
 line = '</VTKFile>';
 fprintf(fid,'%s\n',line);

 fclose(fid);
return
