function profile_confrontation_teo_mk2(casefolders)
close all
% MACHINE DEPENDENT, CHANNEL FLOW ONLY, DEPRECATED
%casefolder is a structure of casefolders strings: {'case1','case2', 'case3'}
%only not modelled profiles to avoid troubles with nomods


ref_path = '/home/matteo/Dropbox/Poli/Phd/MainProject/Datas/DNS/M0.7/';
reTauDns = 178.77;
ReBulkSim = 2800;
[dns] = dns_stat_load(ref_path, reTauDns);

nsim = length(casefolders);

simdata  = sim_stat_load([casefolders{1},'/'], ReBulkSim, reTauDns);

for i=2:nsim
    simtmp = sim_stat_load([casefolders{i},'/'], ReBulkSim, reTauDns);
    simdata = [simdata, simtmp];
end

  fprintf('the ReTau values are:\n')
for j=1:nsim
  fprintf('%s: %f\n', casefolders{j}, simdata(j).ReTau)  
end

col  = { '-ro', '-go', '-bo','-mo','-co', '-yo'};

%% --- tau_uu
figure()
hold on
for j=1:nsim      
 plot(simdata(j).y,simdata(j).tau_uu_mean,'LineWidth',2,col{j})
end
plot(dns.tauuu.y,dns.tauuu.val,'LineWidth',2,'--k')
legend(casefolders{:},'DNS')
xlim([0 180])
xlabel('y^+')
ylabel('\tau_{xx}')

%% --- tau_vv
figure()
hold on
for j=1:nsim      
 plot(simdata(j).y,simdata(j).tau_vv_mean,'LineWidth',2,col{j})
end
plot(dns.tauvv.y,dns.tauvv.val,'LineWidth',2,'--k')
legend(casefolders{:},'DNS')
xlim([0 180])
xlabel('y^+')
ylabel('\tau_{yy}')

%% --- tau_ww
figure()
hold on
for j=1:nsim      
 plot(simdata(j).y,simdata(j).tau_ww_mean,'LineWidth',2,col{j})
end
plot(dns.tauww.y,dns.tauww.val,'LineWidth',2,'--k')
legend(casefolders{:},'DNS')
xlim([0 180])
xlabel('y^+')
ylabel('\tau_{zz}')

%% --- tau_uv
figure()
hold on
for j=1:nsim      
 plot(simdata(j).y,-simdata(j).tau_uv_mean*simdata(j).u2_outer_2_inner,'LineWidth',2,col{j})
end
plot(dns.tauuv.y,dns.tauuv.val,'LineWidth',2,'--k')
legend(casefolders{:},'DNS')
xlim([0 180])
xlabel('y^+')
ylabel('\tau_{zz}')


%% --- U mean
figure()
hold on

for j=1:nsim      
 semilogx(simdata(j).y,simdata(j).u_media_mean,'LineWidth',2,col{j})
end
semilogx(dns.Umean.y,dns.Umean.val,'LineWidth',2,'--k')
legend(casefolders{:},'DNS')
xlim([1 180])
xlabel('y^+')
ylabel('<U>')

figure()
hold on
for j=1:nsim      
 plot(simdata(j).yfull,simdata(j).u_media,'LineWidth',2,col{j})
end
plot(dns.Umean.y,dns.Umean.val,'LineWidth',2,'--k')
legend(casefolders{:},'DNS')
%xlim([0 180])
xlabel('y^+')
ylabel('<U>')
