function [f,M_mean] = energy_spectra(fname,np1,itime,dir,y)
% function [f,M_mean] = energy_spectra(fname,np1,itime,dir,y)
%
% This function evaluates 1D energy spectra over x or z directions,
% once a vector y of plane is chosen. Spectra are calculated on
% different sampling lines and at different sample times.
% A final average of the spectra is then performed.

 % building xxx
 nl = 20; % considered point-lines on each plane
 npp = length(y);
 switch dir
  case 'x'
    np = 300; % number of points on each line
    for iy=1:npp
     for i=1:nl
      xxx{i,iy} = zeros(3,np);
      xxx{i,iy}(1,:) = linspace(0,2*pi,np);
      xxx{i,iy}(2,:) = y(iy);
      xxx{i,iy}(3,:) = -2/3*pi + (4/3*pi)/(nl+1)*i;
     end
    end
    xfft = linspace(0,2*pi,np);
  case 'z'
    np = 200; % number of points on each line
    for iy=1:npp
     for i=1:nl
      xxx{i,iy} = zeros(3,np);
      xxx{i,iy}(1,:) = (2*pi)/(nl+1)*i;
      xxx{i,iy}(2,:) = y(iy);
      xxx{i,iy}(3,:) = linspace(-2/3*pi,2/3*pi,np);
     end
    end
    xfft = linspace(-2/3*pi,2/3*pi,np);
 end
 
 % Build arrays for the original grid
 for i=1:np1
   fname_grid = [fname,'-P',num2str(i-1,'%.3i'),'-grid.octxt'];
   load(fname_grid);
   grid1{i} = grid;
   ddc_grid1{i} = ddc_grid;
   clear grid ddc_grid
 end
 fname_base = [fname,'-P',num2str(0,'%.3i'),'-base.octxt'];
 load(fname_base);
 base1 = base;
 clear base

 % time units considered
 M_mean = zeros(3,np,npp);
 ntimes = length(itime);
 for n=1:ntimes
   % recover the res field file
   for j=1:np1
     fname_res = [fname,'-P',num2str(j-1,'%.3i'),'-res-',num2str(itime(n),'%.4i'),'.octxt'];
     res = load(fname_res);
     uuu1{j} = res.uuu;
     clear res
   end
   % interpolate & fft for spectra
   for iy=1:npp
    for i=1:nl
      ui = dgcomp_interpolation( grid1,ddc_grid1,base1,uuu1,xxx{i,iy} );
      for j=1:3
        uui = ui(2+j,:)./ui(1,:);
        uui = uui - mean(uui);
        [f,M] = time_spectra(xfft,uui.^2);
        M_mean(j,:,iy) = M_mean(j,:,iy) + M;
      end
    end
   end
 end
 % spatial and temporal mean of the correlation
 M_mean = M_mean/(nl*ntimes);

return
