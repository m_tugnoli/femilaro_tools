function [tcor_mean] = time_correlation(idata)
% function [tcor_mean] = time_correlation(idata)
%
% Time correlation provided the temporal hystory
% of some selected points over a fixed plane.

%{
 % sample points
 xP007 = [2,-0.2,0]';
 xP012 = [4,-0.2,-1.5]';
 xP013 = [4,-0.2,0]';
 xP014 = [4,-0.2,1.5]';
 xP019 = [6,-0.2,0]';
 xP010 = [2,0.97,0]';
 xP015 = [4,0.97,-1.5]';
 xP016 = [4,0.97,0]';
 xP017 = [4,0.97,1.5]';
 xP022 = [6,0.97,0]';

 % Interpolation of the solution at sample points
 % plane y=-0.2 --------------
 % P007 
 load visc-P007-grid.octxt
 load visc-P007-base.octxt
 load dataP007.txt;
 idata{1,1} = interp_fast_output(grid,base,dataP007,xP007);
 clear grid base dataP007
 % P012 
 load visc-P012-grid.octxt
 load visc-P012-base.octxt
 load dataP012.txt;
 idata{1,2} = interp_fast_output(grid,base,dataP012,xP012);
 clear grid base dataP012
 % P013 
 load visc-P013-grid.octxt
 load visc-P013-base.octxt
 load dataP013.txt;
 idata{1,3} = interp_fast_output(grid,base,dataP013,xP013);
 clear grid base dataP013
 % P014 
 load visc-P014-grid.octxt
 load visc-P014-base.octxt
 load dataP014.txt;
 idata{1,4} = interp_fast_output(grid,base,dataP014,xP014);
 clear grid base dataP014
 % P019 
 load visc-P019-grid.octxt
 load visc-P019-base.octxt
 load dataP019.txt;
 idata{1,5} = interp_fast_output(grid,base,dataP019,xP019);
 clear grid base dataP019

 % plane y=0.97 --------------
 % P010 
 load visc-P010-grid.octxt
 load visc-P010-base.octxt
 load dataP010.txt;
 idata{2,1} = interp_fast_output(grid,base,dataP010,xP010);
 clear grid base dataP010
 % P015 
 load visc-P015-grid.octxt
 load visc-P015-base.octxt
 load dataP015.txt;
 idata{2,2} = interp_fast_output(grid,base,dataP015,xP015);
 clear grid base dataP015
 % P016 
 load visc-P016-grid.octxt
 load visc-P016-base.octxt
 load dataP016.txt;
 idata{2,3} = interp_fast_output(grid,base,dataP016,xP016);
 clear grid base dataP016
 % P017 
 load visc-P017-grid.octxt
 load visc-P017-base.octxt
 load dataP017.txt;
 idata{2,4} = interp_fast_output(grid,base,dataP017,xP017);
 clear grid base dataP017
 % P022 
 load visc-P022-grid.octxt
 load visc-P022-base.octxt
 load dataP022.txt;
 idata{2,5} = interp_fast_output(grid,base,dataP022,xP022);
 clear grid base dataP022
%}
 nsample = size(idata{1,1},2);
 nvar = size(idata{1,1},1);
 ntau = ceil(nsample/2);
 tcor = zeros(nvar,ntau);

 % loop over planes and points
 for p=1:size(idata,1)
   tcor_mean{p} = zeros(nvar,ntau);
   for pp=1:size(idata,2)
     % compute fluctuations
     for i=1:nvar
       data_f = idata{p,pp}(i,:) - mean(idata{p,pp}(i,:));
     end
     % compute the correlation
     for tau=0:ntau-1
       tcor(:,tau+1) = sum( data_f(:,1:ntau) .* data_f(:,1+tau:ntau+tau) ,2) / ...
                       ntau;
     end
     tcor_mean{p} = tcor_mean{p} + tcor;
   end
   tcor_mean{p} = tcor_mean{p}./size(idata,2);  
 end

return
