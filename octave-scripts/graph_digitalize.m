function [datmat,newmat]=graph_digitalize
% function [datmat,newmat]=my_digital 
%
% Uses the Matlab built-in function "ginput" to extract data from a
% generally scaled graph image and save them with correct scale, so that
% they can be used for further calculations.
% Suggestion: very useful to compare data obtained experimentally, for
% which a graph is provided, with those calculated by numerical means.
%
% Supported image formats:
% .bmp .jpeg .png
%
% outputs two matrices -
% 1 datmat - matrix of pixel values of the image
% 2 newmat - matrix of data values calculated from axes scale
%
%
% P.M.C @ Polimi 2010/11/12


% Fileopen window
[imgfile, imgpath] = uigetfile({'*.bmp;*.jpeg;*.png'}, 'Now choose the image!');
if isequal(imgfile,0) || isequal(imgpath,0)
    disp('You didn''t choose any image :-(')
else
    disp(['You chose:', fullfile(imgpath, imgfile)])
end

[X,map]=imread([imgpath,imgfile]);
image(X);
axis image;
colormap(map);
hold on;


% Get coordinates of upper left/lower right point of axes for scaling
h1=msgbox('Click at the upper left, then the bottom right corner of the curve');
uiwait(h1)
[x1,y1]=ginput(1);
[x2,y2]=ginput(1);


% Enter max/min x and y values for scaling
prompt={'Min Xval','Max Xval','Bottom Yval','Top Yval'};
title='Axes';
answer=inputdlg(prompt,title);
minx=str2double(char(answer(1,:)));
maxx=str2double(char(answer(2,:)));
boty=str2double(char(answer(3,:)));
topy=str2double(char(answer(4,:)));

% Get points!
h2=msgbox('Ready to read curve: left-click to read, enter/return to stop');
uiwait(h2)

% Initialise
hold on
[xi,yi] = ginput;
% Add values to data matrix
datmat=[xi yi];
% Mark the place where points are picked.
plot(xi,yi,'b+')

% datmat=sortrows(datmat,1); %Sort X for interpolation, etc...
% datmat=sortrows(datmat,2); % For stratigraphic data

%calculate scale factor for x and y axes
scalex=(maxx-minx)/(x2-x1);
scaley=abs((topy-boty)/(y2-y1));

% calculate the length of bmp y-axis to reverse scales (stratigraphic data)
lengthy=abs(y2-y1);

% make newmat - matrix with rescaled values 
if topy>boty % y-axis run normally (i.e. min value at base etc)
 newmat=[(((datmat(:,1)-x1).*scalex)+minx),(((lengthy-(datmat(:,2)-y1)).*scaley)+boty)];
else % if y-axis are reversed (e.g. stratigraphic data)
 newmat=[(((datmat(:,1)-x1).*scalex)+minx),(((datmat(:,2)-y1).*scaley)+topy)];
end
newmat
R=input('Do you want me to plot your new data? 1=yes / 2=no \n \n');
if(R==1)
    figure(3)
    plot(newmat(:,1),newmat(:,2))
    hold on; grid on;
end


end
