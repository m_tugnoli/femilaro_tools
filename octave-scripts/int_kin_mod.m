function [kin,rho,m]=int_kin_mod(fname,t0,tf,step,N, k,l)

% Return the turbulent kinetic energy (TKE) for resolved scale, using stat files.
% fname must be a string (e.g. 'out' for file out-P000-stat-0100.octxt)
% t0 is the first stat file
% tf is the last stat file
% step is distance between each stat files

kin=[];

for i=t0:step:tf
 for j=0:N
   
   % Load grid, base and res structures

   load([fname,'-P', num2str(j,'%.3i'),'-grid.octxt']);
   load([fname,'-P', num2str(j,'%.3i'),'-base.octxt']);
   res=load([fname,'-P', num2str(j,'%.3i'),'-res-',num2str(i,'%.4i'),'.octxt']);

   uuu = res.uuu;

   % Interpolate to degree k

   initialize_Uk = 1;
   for h=1:size(uuu,1)
     [Xi,Ti,Ui,cell_type] = ...
     el_resample(grid,base,permute(uuu(h,:,:),[2 3 1]),k,l);
     if(initialize_Uk)
       Uk = zeros(size(uuu,1),size(Ui,1),size(Ui,2));
       initialize_Uk = 0;
     end
     Uk(h,:,:) = permute(Ui,[3 1 2]);
   end

   % Write the grid -coordinate point

   npoints = size(Xi,2)*size(Xi,3); % total number of points   
   n_v4e   = size(Xi,2);    % number of vertexes per element
   n_v4c   = size(Ti,1);    % number of vertexes per cell
   n_c4e   = size(Ti,2);    % number of cells per element
   n_cells = n_c4e*grid.ne; % total number of cells

   if(grid.d==2) % add z to the data
     Xi(3,:,:) = 0;
   endif


   % Density
   
   for j_d=1:size(Xi,3)
     for i_d=1:size(Xi,2)
     rho=Uk(1,i_d,j_d);
     end
   end
   
   % Momentum
   
   if(grid.d==2) % add Uz to the data
     for i_z=size(Uk,1):-1:5 % move tracer data
       Uk(i_z+1,:,:) = Uk(i_z,:,:);
     end
     Uk(5,:,:) = 0;
   endif

   for j_m=1:size(Xi,3)
     for i_m=1:size(Xi,2)
       m=Uk(3:5,i_m,j_m);
     end
   end
    
   % Velocity
   
   end
end

kin=1;
