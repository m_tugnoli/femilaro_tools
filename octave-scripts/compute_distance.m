function [u_ord,u,I_u,dist_ord,dist,I_d]=compute_distance(fname,np,res)

u=[];
dist=[];

for i=1:np

  file_res = [fname,'-P',num2str(i-1,'%.4i'),'-res-',num2str(res,'%.4i'),'.octxt'];
  file_base = [fname,'-P',num2str(i-1,'%.4i'),'-base.octxt'];
  file_grid = [fname,'-P',num2str(i-1,'%.4i'),'-grid.octxt'];

  load(file_res);
  load(file_base);
  load(file_grid);
   
  wg0=base.wg./grid.me.vol;
  
  for j=1:size(uuu,3)

    u_g = uuu(3,:,j)*base.p;
    u_ie(j)= sum(u_g.*wg0);
    d_g = td_diags(12,:,j)*base.p;
    d_ie(j)= sum(d_g.*wg0);
  end
  
  u=[u,u_ie];
  dist=[dist,d_ie];
  
  u_ie=[];
  d_ie=[];
  u_g=[];
  d_g=[];

end


[u_ord,I_u]=sort(u);
[dist_ord,I_d]=sort(dist);


  
  

