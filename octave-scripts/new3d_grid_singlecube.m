function [t,ex,ey,ez] = new3d_grid_singlecube(iverts,orientation)
% This function should be only called from new3d_grid. It computes the
% six tetrahedra used to represent a cube. The input parameters are
% the vertices of the cube, and a string specifying the desired
% orientation, which must be one of the following: up_north, up_south,
% down_north, down_south.

 ex = zeros(6,2,2); ey = ex; ez = ey;

 switch orientation
  case 'up_north'
   tt = [ 1 1 1 1 1 1;
          8 6 8 8 8 4;
          5 5 2 7 3 2;
          7 8 6 3 4 8 ];
   ex(1:3,:,1) = iverts([1,3,4;1,4,2]'); ex(6,:,1) = 2;
   ex(1:3,:,2) = iverts([5,7,8;5,8,6]'); ex(6,:,2) = 4;
   ey(1:3,:,1) = iverts([1,5,6;1,6,2]'); ey(6,:,1) = 3;
   ey(1:3,:,2) = iverts([3,7,8;3,8,4]'); ey(6,:,2) = 5;
   ez(1:3,:,1) = iverts([1,5,7;1,7,3]'); ez(6,:,1) = 6;
   ez(1:3,:,2) = iverts([2,6,8;2,8,4]'); ez(6,:,2) = 1;
  case 'up_south'
   tt = [ 6 8 6 6 6 2;
          3 3 3 3 3 3;
          7 7 4 5 1 4;
          5 6 8 1 2 6 ];
   ex(1:3,:,1) = iverts([1,3,2;2,3,4]'); ex(6,:,1) = 2;
   ex(1:3,:,2) = iverts([5,7,6;7,8,6]'); ex(6,:,2) = 4;
   ey(1:3,:,1) = iverts([1,5,6;1,6,2]'); ey(6,:,1) = 3;
   ey(1:3,:,2) = iverts([3,7,8;3,8,4]'); ey(6,:,2) = 5;
   ez(1:3,:,1) = iverts([1,5,3;5,7,3]'); ez(6,:,1) = 6;
   ez(1:3,:,2) = iverts([2,6,4;6,8,4]'); ez(6,:,2) = 1;
  case 'down_north'
   tt = [ 7 5 7 7 7 3;
          2 2 2 2 2 2;
          6 6 1 8 4 1;
          8 7 5 4 3 7 ];
   ex(1:3,:,1) = iverts([1,3,2;2,3,4]'); ex(6,:,1) = 2;
   ex(1:3,:,2) = iverts([5,7,6;6,7,8]'); ex(6,:,2) = 4;
   ey(1:3,:,1) = iverts([1,5,2;5,6,2]'); ey(6,:,1) = 3;
   ey(1:3,:,2) = iverts([3,7,4;7,8,4]'); ey(6,:,2) = 5;
   ez(1:3,:,1) = iverts([1,5,7;1,7,3]'); ez(6,:,1) = 6;
   ez(1:3,:,2) = iverts([2,6,8;2,8,4]'); ez(6,:,2) = 1;
  case 'down_south'
   tt = [ 4 4 4 4 4 4;
          5 7 5 5 5 1;
          8 8 3 6 2 3;
          6 5 7 2 1 5 ];
   ex(1:3,:,1) = iverts([1,3,4;1,4,2]'); ex(6,:,1) = 2;
   ex(1:3,:,2) = iverts([5,7,8;5,8,6]'); ex(6,:,2) = 4;
   ey(1:3,:,1) = iverts([1,5,2;2,5,6]'); ey(6,:,1) = 3;
   ey(1:3,:,2) = iverts([3,7,4;7,8,4]'); ey(6,:,2) = 5;
   ez(1:3,:,1) = iverts([1,5,3;5,7,3]'); ez(6,:,1) = 6;
   ez(1:3,:,2) = iverts([2,6,4;6,8,4]'); ez(6,:,2) = 1;
  otherwise
   error(['Wrong orientation "',orientation,'"']);
 endswitch

 t = iverts(tt);

return

