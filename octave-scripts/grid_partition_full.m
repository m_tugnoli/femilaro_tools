function grid_partition_full(grid_file,grid_name,varargin)
% function grid_partition_full(grid_file,grid_name,varargin)
%
% Procedure for the generation of a partitioned grid starting from the full one.
%
% grid_file: the computational grid file in Octave format (.octxt)
% grid_name: the computational grid name (string)
% varargin{1}: periodic_faces (see grid_partition.m)
% varargin{2}: node_data_names (see grid_partition.m)
% varargin{3}: node_data (see grid_partition.m)

% 1. Load the full grid in Octave format
load(grid_file);

% 2. Selection of partition technique
disp('1 -> Metis')
disp('2 -> Regular Block')
tech_part = input('Partition technique: ');

if tech_part==1
 % transpose the vertices and save the input file for METIS
 tt = t(1:4,:)';
 save metis_input tt;
 % modify the input file, launch METIS, come back here
 disp('Delete the header of the METIS input file, except for the number of elements (rows) which must be left only as a number without any text');
 disp('Launch METIS specifying the number of partitions');
 disp('Press ENTER when done');
 pause
 nb = input('Number of partitions chosen: ');
 % load the .epart METIS file and store the indexes in idx
 idx = load(strcat('metis_input.epart.',num2str(nb)));

elseif tech_part==2
 B = input('Number of blocks in x,y,z direction [Bx,By,Bz]: ');
 idx = regular_block_partition(B,d,p,t);
 nb = max(idx)+1;

else 
 error('Partition technique not available');
end

% 3. Use the function grid_partition.m 
 % periodicity & nodal data
 if((nargin>2)&&(~isempty(varargin{1})))
   periodic_faces = varargin{1};
   grids = grid_partition(p,e,t,idx,periodic_faces);
 elseif(nargin>3)
   node_data_names = varargin{2};
   node_data       = varargin{3};
   grids = grid_partition(p,e,t,idx,periodic_faces,node_data_names,node_data);
 else
   grids = grid_partition(p,e,t,idx);
 end

% 4. Save the grid files in .octxt format
fname = strcat(grid_name,'-p',num2str(nb),'.octxt');
for i = 1:size(grids,2)
 p=grids(i).p; t=grids(i).t; e=grids(i).e; ivl2ivn=grids(i).ivl2ivn;
 save([fname,'.',num2str(i-1,'%.3d')],'d','p','e','t','ivl2ivn');
end

return
