function S = coeff_ms(fname,np,ivar,itime)
% function S = coeff_ms(fname,np,ivar,itime)
% 
% This function evaluates the energy distribution among the
% different modes of the solution for the selected variable
% ivar, performing a mean over time.

 k = 4;

 for ik=1:k+1
  if (ik==1)
    js(ik) = 1;
  else
    js(ik) = nll_sympoly(3,ik-2)+1;
  end
  je(ik) = nll_sympoly(3,ik-1);
 end

 y1 = [-1.00000  -0.97500  -0.93361  -0.86661  -0.76193  -0.60722  -0.39630  -0.13834   0.13834   0.39630 0.60722   0.76193   0.86661   0.93361   0.97500];
 y2 = [-0.97500  -0.93361  -0.86661  -0.76193  -0.60722  -0.39630  -0.13834   0.13834   0.39630   0.60722   0.76193   0.86661   0.93361   0.97500   1.00000];

 Nl = size(y1,2);
 S = zeros(k+1,Nl);
 ne = zeros(Nl,1);
 ntime = length(itime);

 for ii=1:ntime

  for i=1:np

   fname_grid = [fname,'-P',num2str(i-1,'%.3i'),'-grid.octxt'];
   load(fname_grid);
   fname_res = [fname,'-P',num2str(i-1,'%.3i'),'-res-',num2str(itime(ii),'%.4i'),'.octxt'];
   load(fname_res);

   for ie=1:grid.ne
     yb = grid.e(ie).xb(2);
     for ll=1:Nl
       if(and( yb>y1(ll) , yb<y2(ll) ))
         for ik=1:k+1
           S(ik,ll) = S(ik,ll) + sum(uuu(ivar, js(ik):je(ik) ,ie).^2);
         end
         ne(ll) = ne(ll)+1;
       end
     end
   end

  end

 end

 for ll=1:Nl
   S(:,ll) = S(:,ll)/ne(ll)/ntime;
 end

return
