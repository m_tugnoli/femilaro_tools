function []=mesh_dims(fname)
% Small utility to check the mesh bounding box dimensions
load(fname);
xmin = min(p(1,:));
xmax = max(p(1,:));
ymin = min(p(2,:));
ymax = max(p(2,:));
zmin = min(p(3,:));
zmax = max(p(3,:));

fprintf('Mesh bounding box:\n');
fprintf('xmin: %g xmax %g\n',xmin,xmax);
fprintf('ymin: %g ymax %g\n',ymin,ymax);
fprintf('zmin: %g zmax %g\n',zmin,zmax);
