function [trim_name] = trim_machine_name(name)
% Takes in input a case folder name xxyyzz_machinename and gives back
% the case basename xxyyzz
len = length(name);

found = 0;
i = len;
while (not(found) && i > 1)
	if (name(i) == '_')
		found = 1;
    end
	i = i-1;
end

trim_name = name(1:i);
