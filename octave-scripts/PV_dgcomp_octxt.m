function PV_dgcomp_octxt(grid,base,U,k,l,outfile,test_name)

% Write the DG solution in octxt file.
% This function is based on PV_dgcomp and writes only coordinate points, 
% density, energy and momentum.

 % Interpolate to degree k

 initialize_Uk = 1;
 for i=1:size(U,1)
   [Xi,Ti,Ui,cell_type] = ...
     el_resample(grid,base,permute(U(i,:,:),[2 3 1]),k,l);
   if(initialize_Uk)
     Uk = zeros(size(U,1),size(Ui,1),size(Ui,2));
     initialize_Uk = 0;
   end
   Uk(i,:,:) = permute(Ui,[3 1 2]);
  end

 % Write the output file

 fid = fopen(outfile,'w');
 line = ['# name: ','data_struct'];
 fprintf(fid,'%s\n',line);
 line = ['# type: ', 'struct'];
 fprintf(fid,'%s\n',line);
 line = ['# length: ', num2str(4,'%1d')];
 fprintf(fid,'%s\n',line);

 % Write the grid -coordinate point
 npoints = size(Xi,2)*size(Xi,3); % total number of points
 n_v4e   = size(Xi,2);    % number of vertexes per element
 n_v4c   = size(Ti,1);    % number of vertexes per cell
 n_c4e   = size(Ti,2);    % number of cells per element
 n_cells = n_c4e*grid.ne; % total number of cells
 line = ['# name: ' , 'points'];
 fprintf(fid,'%s\n',line);
 line = ['# type: ', 'cell' ];
 fprintf(fid,'%s\n',line);
 line = ['# rows: ', num2str(1,'%1d') ];
 fprintf(fid,'%s\n',line);
 line = ['# columns: ', num2str(1,'%1d') ];
 fprintf(fid,'%s\n',line);
 line = ['# name: ', '<cell-element>' ];
 fprintf(fid,'%s\n',line);
 line = ['# type: ', 'matrix'];
 fprintf(fid,'%s\n',line);
 line = ['# rows: ',num2str(npoints,'%1d')];
 fprintf(fid,'%s\n',line);
 line = ['# columns :',num2str(3,'%1d')];
 fprintf(fid,'%s\n',line);
 if(grid.d==2) % add z to the data
   Xi(3,:,:) = 0;
 endif
 for j=1:size(Xi,3)
   for i=1:size(Xi,2)
     fprintf(fid,'%e ',Xi(:,i,j));
     fprintf(fid,'\n');
   end
 end

% Write density
 line = ['# name: ','density'];
 fprintf(fid,'%s\n',line);
 line = ['# type: ', 'cell' ];
 fprintf(fid,'%s\n',line);
 line = ['# rows: ', num2str(1,'%1d') ];
 fprintf(fid,'%s\n',line);
 line = ['# columns: ', num2str(1,'%1d') ];
 fprintf(fid,'%s\n',line);
 line = ['# name: ', '<cell-element>' ];
 fprintf(fid,'%s\n',line);
 line = ['# type: ','matrix'];
 fprintf(fid,'%s\n',line);
 line = ['# rows: ',num2str(npoints,'%1d')];
 fprintf(fid,'%s\n',line);
 line = ['# columns :',num2str(1,'%1d')];
 fprintf(fid,'%s\n',line);
 for j=1:size(Xi,3)
   for i=1:size(Xi,2)
     fprintf(fid,'%e\n',Uk(1,i,j));
   end
 end

% Write energy
 line = ['# name: ','energy'];
 fprintf(fid,'%s\n',line);
 line = ['# type: ', 'cell' ];
 fprintf(fid,'%s\n',line);
 line = ['# rows: ', num2str(1,'%1d') ];
 fprintf(fid,'%s\n',line);
 line = ['# columns: ', num2str(1,'%1d') ];
 fprintf(fid,'%s\n',line);
 line = ['# name: ', '<cell-element>' ];
 fprintf(fid,'%s\n',line);
 line = ['# type: ','matrix'];
 fprintf(fid,'%s\n',line);
 line = ['# rows: ',num2str(npoints,'%1d')];
 fprintf(fid,'%s\n',line);
 line = ['# columns: ',num2str(1,'%1d')];
 fprintf(fid,'%s\n',line);
 for j=1:size(Xi,3)
   for i=1:size(Xi,2)
     fprintf(fid,'%e\n',Uk(2,i,j));
   end
 end

% Write momentum

 line = ['# name: ','momentum'];
 fprintf(fid,'%s\n',line);
 line = ['# type: ', 'cell' ];
 fprintf(fid,'%s\n',line);
 line = ['# rows: ', num2str(1,'%1d') ];
 fprintf(fid,'%s\n',line);
 line = ['# columns: ', num2str(1,'%1d')];
 fprintf(fid,'%s\n',line);
 line = ['# name: ', '<cell-element>'];
 fprintf(fid,'%s\n',line);
 line = ['# type: ', 'matrix'];
 fprintf(fid,'%s\n',line);
 line = ['# rows: ',num2str(npoints,'%1d')];
 fprintf(fid,'%s\n',line);
 line = ['# columns: ',num2str(3,'%1d')];
 fprintf(fid,'%s\n',line);
 if(grid.d==2) % add Uz to the data
   for i=size(Uk,1):-1:5 % move tracer data
     Uk(i+1,:,:) = Uk(i,:,:);
   end
   Uk(5,:,:) = 0;
 endif
 for j=1:size(Xi,3)
   for i=1:size(Xi,2)
     fprintf(fid,'%e ',Uk(3:5,i,j));
     fprintf(fid,'\n');
   end
 end


% Close

 fclose(fid);

return
