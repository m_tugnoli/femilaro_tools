function plot_h2d_nodalfield(grid,base,f)
% plot_h2d_nodalfield(grid,base,f,key)
%
% Plot a scalar field f, known at the grid nodes. Quadrilaterals are
% split into two triangles, so that one can use trisurf.

 % Split quadrilaterals into two triangles

 ntri = sum([grid.e.poly]==3) + 2*sum([grid.e.poly]==4);
 tri = zeros(ntri,3);
 
 it = 0;
 for ie=1:grid.ne
   
   switch grid.e(ie).poly
    case 3
     it = it+1;
     tri(it,:) = grid.e(ie).iv;
    case 4
     it = it+1;
     tri(it,:) = grid.e(ie).iv([1,2,3]);
     it = it+1;
     tri(it,:) = grid.e(ie).iv([1,3,4]);
    otherwise
     error("Unsupported shape.")
   endswitch
   
 end

 % Make the plot
 xy = [grid.v(:).x];
 trisurf(tri,xy(1,:),xy(2,:),f)

return

