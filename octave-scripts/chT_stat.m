function [mean_g,mean_v,t] = chT_stat(fname,itime)
% function [mean_g,rms_g,t] = chT_stat(fname,itime)
%
% Function for a preliminary check of the calculated output.
% fname is the name of the statistics file.
% itime can be a vector of different times.
% Output variables are the indexes of the figures.

 close all
 n = length(itime);

 % Stat grid
 fname_sgrid = [fname,'-s_grid.octxt'];
 sgrid = load(fname_sgrid);
 y = sgrid.s_grid.yp;
 clear sgrid

 % Global values over time
 t = []; mean_g = []; rms_g = []; mean_v = [];
 for j=1:n
   fname_stat = [fname,'-stat-',num2str(itime(j),'%.4i'),'.octxt'];
   stat = load(fname_stat);
   t = [t,stat.time];
   mean_g = [mean_g, stat.stat.uuu_g(1,:)'];
   rms_g = [rms_g, stat.stat.uuu_g(2,:)'];
   mean_v = [mean_v, stat.stat.uuu_v(1,:)'];

   if (j==1)
     ui = stat.stat.stat3d(1,:,stat.stat.uu(1));
     vi = stat.stat.stat3d(1,:,stat.stat.uu(2));
     wi = stat.stat.stat3d(1,:,stat.stat.uu(3));
     Ti = stat.stat.stat3d(1,:,stat.stat.T);
   end
   if (j==n)
     un = stat.stat.stat3d(1,:,stat.stat.uu(1));
     vn = stat.stat.stat3d(1,:,stat.stat.uu(2));
     wn = stat.stat.stat3d(1,:,stat.stat.uu(3));
     un_rms = stat.stat.stat3d(2,:,stat.stat.uu(1));
     Tn = stat.stat.stat3d(1,:,stat.stat.T);
     Tn_rms = stat.stat.stat3d(2,:,stat.stat.T);
   end

   clear stat
 end
%{
 % global mean values
 rho_mg = mean_g(1,:);
 e_mg = mean_g(2,:);
 m_mg = sqrt( sum(mean_g(3:5,:).^2) ); % momentum
 mg = figure; hold on; grid on
 plot(t,rho_mg,'b',t,e_mg,'r',t,m_mg,'k')
 legend('\rho','e','m')
 xlabel('time'), ylabel('Global mean values')
 title('Global MEAN values')
%}
 % spatial mean values
 rho_v = mean_v(1,:);
 e_v = mean_v(2,:);
 m_v = sqrt( sum(mean_v(3:5,:).^2) ); % momentum
 figure; hold on; grid on
 plot(t,rho_v,'b',t,e_v,'r',t,m_v,'k')
 legend('\rho','e','m')
 xlabel('time'), ylabel('Spatial mean values')
 title('Spatial MEAN values')

 % spatial mean energy
 figure; hold on; grid on
 plot(t,e_v,'-r')
 legend('e')
 xlabel('time'), ylabel('Spatial mean energy')
 title('Spatial MEAN energy')
 % spatial mean momentum
 figure; hold on; grid on
 plot(t,m_v,'k')
% plot(t,0.5*m_v.^2./rho_v,'k')
 legend('m')
 xlabel('time'), ylabel('Spatial mean momentum')
 title('Spatial MEAN momentum')

 % global rms values
 rho_rmsg = rms_g(1,:);
 e_rmsg = rms_g(2,:);
 m_rmsg = sqrt( sum(rms_g(3:5,:).^2) ); % momentum
 rmsg = figure; hold on; grid on
 plot(t,rho_rmsg,'b',t,e_rmsg,'r',t,m_rmsg,'k')
 legend('\rho','e','m')
 xlabel('time'), ylabel('Global RMS values')
 title('Global RMS values')

%{
 % Velocity (streamwise component)
 u = figure; hold on; grid on
 plot(y,ui,'b',y,un,'k')
 legend('initial','last')
 xlabel('y'), ylabel('u')
 title('Streamwise velocity')
 % Temperature
 T = figure; hold on; grid on
 plot(y,Ti,'b',y,Tn,'k')
 legend('initial','last')
 xlabel('y'), ylabel('T')
 title('Temperature')
 % RMS
 figure, hold on, grid on
 plot(y,un_rms,'b',y,Tn_rms,'k')
 legend('rmsU','rmsT')
 xlabel('y'), ylabel('rms')
%}

return
