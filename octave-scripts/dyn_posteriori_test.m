function dyn_posteriori_test(fname,itime)
% function dyn_posteriori_test(fname,itime)
%
% Function for a posteriori test when a dynamic model is used.
% It's possible to check if kinetic energy dissipation is positive
% over the different planes and if the second law of thermodynamics 
% is respected in its integral form.
%
% fname is the name of the statistics file
% itime can be a vector of different times 

 close all
 n = length(itime);

 % Stat grid
 fname_sgrid = [fname,'-s_grid.octxt'];
 sgrid = load(fname_sgrid);
 y = sgrid.s_grid.yp;
 np = length(y);
 clear sgrid

 % collect necessary outputs
 t = zeros(1,n);
% qv = zeros(3,np,n); qt = qv; qsgs = qv;
% dissv = zeros(np,n); disst = dissv; disssgs = dissv;

 for j=1:n
   fname_stat = [fname,'-stat-',num2str(itime(j),'%.4i'),'.octxt'];
   stat = load(fname_stat);

   t(j) = stat.time;
   qv = stat.stat.td.q(:,:,stat.stat.td.visc);
   qt = stat.stat.td.q(:,:,stat.stat.td.turb_res);
   qsgs = stat.stat.td.q(:,:,stat.stat.td.turb_sgs);
   
   dissv = stat.stat.td.diss(:,stat.stat.td.visc);
   disst = stat.stat.td.diss(:,stat.stat.td.turb_res);
   disssgs = stat.stat.td.diss(:,stat.stat.td.turb_sgs);

   % kinetic dissipation 
   test1(:,j) = dissv + disst + disssgs;
   % second law of thermodynamics
   test2(:,j) = sum(qv + qt + qsgs,1)' + 8/3*pi^2*(dissv + disst + disssgs);

   clear stat
 end

 % Figures

 figure,hold on, grid on
 for j=1:n
   plot(t(j),test1(:,j),'b+')
 end
 title('kinetic dissipation over planes')
 xlabel('t')

 figure,hold on, grid on
 for j=1:n
   plot(t(j),test2(:,j),'r+')
 end
 title('2nd law of thermodynamics')
 xlabel('t')

return
