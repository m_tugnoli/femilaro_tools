function [ynod,T,u,u_b] = laminar_analytical_FE()
% function [ynod,T,u,u_b] = laminar_analytical_FE()
% 
% Equations solved:
% Temperature -------------
%   -T^alpha*d2T^(alpha+1)/dy2 = tauw_av^2*y^2/C, for y in (-1,1)
%   T = 1  for y = +1, y = -1
% Velocity ----------------
% linear equation once the temperature field is solved
%   T^alpha du/dy = tau(y) for y in (-1,1)
%   u = 0 for y = +1, y = -1
%
% Output:
%   - ynod: nodi di discretizzazione
%   - T,u: non-dimensional profiles of temperature and
%          velocity
%   - u_b: bulk velocity

addpath /home/alessio/FE1D
addpath /home/alessio/FE1D/LIBRERIA_UTENTE
addpath /home/alessio/FE1D/UTILITA

% Global variables can be used by all functions
global CT Tm1 alpha
global tauw_m tauw_p
alpha = 0.7; gamma = 1.4; Pr = 0.72; 
Re = 100; Ma = 0.7;
CT = 1/(Ma^2*Pr*(gamma-1)*(alpha+1));
tauw_m = 3;
tauw_p = -tauw_m;

% Number and order of elements
N = 15;
r = 3;
% nodes (equi-spaced)
hy = 2/(r*N);
ynod = [-1 : hy : 1];
% computational grid
griglia = struttura_griglia(ynod,r);
% local base
base = struttura_base(r);

% building matrices and vectors
K = termine_diffusione(griglia,base,'coeff_mu');
C = termine_trasporto(griglia,base,'coeff_a');
% oppure, volendo usare una stabilizzazione:
%C = termine_trasporto_upwind(griglia,base,'coeff_a','coeff_mu');
%C = termine_trasporto_SG(griglia,base,'coeff_a','coeff_mu');
bv = termine_noto(griglia,base,'coeff_fT');

% Dirichlet boundary condition
u_incognite = [2 : 1 : griglia.dim-1];
u_note      = [1,griglia.dim];

% global matrix and rhs for the temperature system
A = K;
b = bv;

A11 = A(u_incognite,u_incognite);
A12 = A(u_incognite,u_note     );
A21 = A(u_note     ,u_incognite);
A22 = A(u_note     ,u_note     );
T2 = [1;1];  % valori noti di T al bordo

% Solution
Tm1 = ones(griglia.dim,1);

for i=1:100

  bb = b./(Tm1.^alpha);
  b1 = bb(u_incognite);
  T1 = A11\(b1-A12*T2);
  Tn(u_incognite,1) = T1;
  Tn(u_note     ,1) = T2;

  Tn = Tn.^(1/(alpha+1));
  delta = norm(Tn-Tm1);
  if(delta<1e-10)
    break
  end

  Tm1 = Tn;

end
T = Tn;

% global matrix and rhs for velocity equation
A = C;
b = termine_noto(griglia,base,'coeff_fu');

A11 = A(u_incognite,u_incognite);
A12 = A(u_incognite,u_note     );
A21 = A(u_note     ,u_incognite);
A22 = A(u_note     ,u_note     );
u2 = [0;0];  % valori noti di u al bordo

% Solution
b = b./(T.^alpha);
b1 = b(u_incognite);
u1 = A11\(b1-A12*u2);
u(u_incognite,1) = u1;
u(u_note     ,1) = u2;
% bulk velocity
u_b = 1/2*sum(u(2:end)+u(1:end-1))/2*hy;

return
