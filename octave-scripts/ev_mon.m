function z = ev_mon(mon,x)
% This function evaluates a monomial mon at point x. It's an octave
% rewriting of ev_1d on mod_symmon.

 z = zeros(1,size(x,2));
 for i=1:size(x,2)
   z(i) = mon(1) * prod( x(1:length(mon(2:end,1)),i).^mon(2:end,1) );
 end

return

