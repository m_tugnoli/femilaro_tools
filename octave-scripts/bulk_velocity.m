function [ub] = bulk_velocity(fname,itime)
% function [ub] = bulk_velocity(fname,itime)
%
% Bulk velocity evaluation: select the file and the
% time considered.

% sgrid_name = [fname,'-s_grid.octxt'];
 sgrid_name = ['stat-grid.octxt'];
 load(sgrid_name)
 y = s_grid.yp;

% stat_name =  [fname,'-stat-',num2str(itime,'%.4i'),'.octxt'];
 stat_name =  [fname,'-stat.octxt'];
 load(stat_name)
% u = permute(stat.uu(1,1,:),[3,1,2]);
 u = stat3d2octave( stat, 1, stat.uu(1) )';

 umax = max(u)
 ub = 1/2* sum( (u(2:end)+u(1:end-1))./2 .* abs(y(2:end)-y(1:end-1)) );

 [tau_visc] = td2octave( stat.td.tau, [1,2], stat.td.visc );
 tauw = tau_visc(1)

return
