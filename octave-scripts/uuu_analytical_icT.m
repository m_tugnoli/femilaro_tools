function [uuue_icT, grad_uuue_icT] = uuu_analytical_icT(x_eval)
% function [uuue_icT, grad_uuue_icT] = uuu_analytical_icT(x_eval)
%
% Evaluation of the analytical solution and its gradient
% at x_eval points (for instance Gauss nodes).
% Periodic initial condition.
% evaluation at the final time step t_end = 1.0 s
%
% x_eval: points matrix for the evaluation, each column identifies a point
% -> x_eval(1,i) = x, x_eval(2,i) = y, x_eval(3,i) = z

% Channel dimensions
Lx = 5.0; Ly = 0.5; Lz = 1.5;
% Wavenumbers
k = 1; l = 1; m = 1;
% Viscosity
nu = 0.25;
% Characteristic time
Kx = pi*k/Lx; Ky = pi/2*l/Ly; Kz = pi/2*m/Lz;
K = Kx^2 + Ky^2 + Kz^2;
tau = 1/(nu*K);

% Defining the analytical solution
t_end = 1.0;
u_str = 'exp(-t_end/tau)*sin(Kx*x)*cos(Ky*y)*cos(Kz*z)';
u = inline(u_str,'x','y','z');
% Defining the gradient of the analytical solution
gradx_u_str = 'Kx*exp(-t_end/tau)*cos(Kx*x)*cos(Ky*y)*cos(Kz*z)';
grady_u_str = '-Ky*exp(-t_end/tau)*sin(Kx*x)*sin(Ky*y)*cos(Kz*z)';
gradz_u_str = '-Kz*exp(-t_end/tau)*sin(Kx*x)*cos(Ky*y)*sin(Kz*z)';
gradx_u = inline(gradx_u_str,'x','y','z');
grady_u = inline(grady_u_str,'x','y','z');
gradz_u = inline(gradz_u_str,'x','y','z');

% Evaluate the solution
n_points = size(x_eval,2);
for i = 1:n_points
 % row vector: each column is the value at the considered point
 uuue_icT(1,i) = u(x_eval(1,i),x_eval(2,i),x_eval(3,i));
end
% Evaluate the gradient fo the solution
for i = 1:n_points
 % each column is the gradient of the solution at the considered point
 grad_uuue_icT(:,i) = [gradx_u(x_eval(1,i),x_eval(2,i),x_eval(3,i));
                       grady_u(x_eval(1,i),x_eval(2,i),x_eval(3,i));
                       gradz_u(x_eval(1,i),x_eval(2,i),x_eval(3,i));];  
end

return
