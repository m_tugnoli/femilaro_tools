function PV_dgcomp_series_octxt(fname, itime,N,k,l,outfile,test_name) 
 
% Write a .octxt data file for each partition, using PV_dgcomp_octxt.m 
% function
 
% fname must contain a '%' to indicate the subdomain number and a '$'
% to indicate the suffix 'grid', 'base' or 'res-XXXX'.
% EXAMPLE: 'ch_icT-P%-$.octxt'
%
% itime: label(s) of the selected output times
% EXAMPLE: for 'ch_icT-P%-res-0003.octxt', itime=3
% 
% N: number of partitions minus 1 (the counter starts from 0)
% k: FE order
% l: order of interpolation (ca be taken less or equal to k) ??
% outfile: name of the file without extension (string)
%
% This function is based on PV_dg_comp_series.m 

% Load file grid, base and res

warning('off','Octave:load-file-in-path');

 Npos = find(fname=='%');
 for j=1:length(itime)
   for i=0:N
     fnameN = [fname(1:Npos-1),num2str(i,'%.3i'),fname(Npos+1:end)];
     Xpos = find(fnameN=='$');
     fnameNX = [fnameN(1:Xpos-1),'grid',fnameN(Xpos+1:end)];
     grid = load(fnameNX);
     fnameNX = [fnameN(1:Xpos-1),'base',fnameN(Xpos+1:end)];
     base = load(fnameNX);
     fnameNX = [fnameN(1:Xpos-1),'res-',num2str(itime(j),'%.4i'), ...
                fnameN(Xpos+1:end)];
     res = load(fnameNX);

     uuu = res.uuu;

% Write octxt file 

     fnameNX = [outfile,'-',num2str(itime(j)), ...
                        '-',num2str(i,'%.3i'),'.octxt'];

     PV_dgcomp_octxt(grid.grid,base.base,uuu,k,l,fnameNX,res.test_name);
   
   end
 end 



return
