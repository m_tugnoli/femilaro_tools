function [x_grid] = affmap(e,base_xig)
% function [x_grid] = affmap(e,base_xig)
%
% Transformations from the reference element to the real grid one.
%
% e: element of the computational grid
% base_xig: finite element reference nodes

 x_grid = e.b*base_xig;
 for i = 1:e.m
   x_grid(i,:) = x_grid(i,:) + e.x0(i);
 end

return
