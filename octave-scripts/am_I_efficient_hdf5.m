function am_I_efficient_hdf5(result)
% print statistics about the adaptive results
%ARG 1: either input static p-adaptivity file or result file

data = load(result);

if(any(strcmp('edeg',fieldnames(data))))
  e_deg = data.edeg;
elseif(any(strcmp('deg_lim',fieldnames(data))))
  e_deg = data.deg_lim;
else
  error('No degree specification found in input/result file')
end
ne = length(e_deg);
dofs = [3 10 20 35];
nbas = dofs(e_deg);

uniform_dofs = dofs*ne;
actual_dofs = sum(nbas);
diff_dofs = (actual_dofs-uniform_dofs)./uniform_dofs*100;
fprintf('Actual dofs: %d\n',actual_dofs)
fprintf('Uniform degree dofs [1 2 3 4]: %d %d %d %d\n',uniform_dofs)
fprintf('Percentage difference with respect to uniform degree: %d %d %d %d\n',diff_dofs)
fprintf('Degree distribution:\n')
for i = min(e_deg):max(e_deg)
  cnt = sum(e_deg == i);
  fprintf('Number of elements of degree %d: %d \n',i,cnt)
end

if(any(strcmp('indicator',fieldnames(data))))
  indval = data.indicator;
elseif(any(strcmp('adaptind_val',fieldnames(data))))
  indval = data.adaptind_val;
else
  error('No indicator found in input/result file')
end

fprintf('a posteriori threshold estimation:\n')
for i = min(e_deg):max(e_deg)
  mmax = max(indval(e_deg == i));
  mmin = min(indval(e_deg == i));
  fprintf('degree %d\n',i)
  fprintf('minimum indicator: %e, maximum indicator: %e\n', mmin, mmax)
end
