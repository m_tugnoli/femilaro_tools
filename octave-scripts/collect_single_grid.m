function uuu2 = collect_single_grid(grid2,base2,fname1,itime,N)
% uuu2 = collect_single_grid(grid2,base2,fname1,itime,N)
%
% Interpolate the solution of fname1 on grid2. Essentially, this
% functions works as an interface to dgcomp_grid2grid.

 % Part loop
 Npos = find(fname1=='%');
 for i=0:N
   fnameN = [fname1(1:Npos-1),num2str(i,'%.3i'),fname1(Npos+1:end)];
   Xpos = find(fnameN=='$');
   fnameNX = [fnameN(1:Xpos-1),'grid',fnameN(Xpos+1:end)];
   tmp = load(fnameNX);
   grid1{i+1}     = tmp.grid;
   ddc_grid1{i+1} = tmp.ddc_grid;
   if(i==0)
     fnameNX = [fnameN(1:Xpos-1),'base',fnameN(Xpos+1:end)];
     tmp = load(fnameNX);
     base1 = tmp.base;
   end
   fnameNX = [fnameN(1:Xpos-1),'res-',num2str(itime,'%.4i'), ...
              fnameN(Xpos+1:end)];
   tmp = load(fnameNX);
   uuu1{i+1} = tmp.uuu;
 end

 uuu2 = dgcomp_grid2grid( grid1,ddc_grid1,base1,uuu1, grid2,base2 );

return

