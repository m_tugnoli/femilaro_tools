function z = ev_pol(pol,x)
% This function evaluates a polynomial pol at point x. It's an octave
% rewriting of ev_1d on mod_sympoly.

 z = zeros(1,size(x,2));
 for i=1:size(pol,2)
   z = z + ev_mon(pol(:,i),x);
 end

return

