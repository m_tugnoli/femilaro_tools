function [eL2,eL1,eLi,XY] = compute_h2d_errors(grid,base,val,exact)
% [eL2,eL1,eLi] = compute_h2d_errors(grid,base,val,exact)
%
% Compute the errors for a collection of 2D, hybrid grids
%
% val: can be passed as {res.XXX} where XXX is the required field
% exact: should return a row vector with the exact solution

 eL1 = 0.0;
 eL2 = 0.0;
 eLi = 0.0;
 if(nargout>3)
   XY = [];
 endif

 % Loop over the subdomains
 for iid=1:length(grid)

   % Unpack the grid
   ne = grid(iid).ne;
    e = grid(iid).e;
    z =  val{iid};

   % Translate the poly index starting from 1
   pol1( base(iid).bounds(1):base(iid).bounds(2) ) = ...
                           [1:length(base(iid).bounds)];

   % Compute the error
   for ie=1:ne

     % Get some fields
     xig = base(iid).e{ pol1(e(ie).poly) }.xig;
     wg  = base(iid).e{ pol1(e(ie).poly) }.wg;
     p   = base(iid).e{ pol1(e(ie).poly) }.p;

     % Map the quad. nodes to physical space
     xg = e(ie).me.map( e(ie).me , xig );

     % Scale the quad. weights
     swg = e(ie).me.scale_wg( e(ie).me , xig , wg );

     % Compute the pointwise error
     eg = exact(xg) - z(e(ie).iv)' * p;

     % Accumulate the error norms
     eL1 = eL1 + sum( swg .* abs(eg));
     eL2 = eL2 + sum( swg .* eg.^2  );
     eLi = max( eLi , max( abs(eg) ));

     if(nargout>3)
       XY = [ XY ; xg' , eg' ];
     endif

   end

 end

 eL2 = sqrt(eL2);

return

