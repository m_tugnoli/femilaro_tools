function ch_icT_plot(h_vect,k_vect,P)
% function ch_icT_plot(h_vect,k_vect,P)
%
% Plotting the solution for the diffusive testcase, with:
% h_vect,k_vect: vectors for grid size and polynomial order
% P: considered partition, where the point of interest is located

 % centre channel point
 x = [2.5,0,0]';

 % Defining the analytical solution
 Lx = 5.0; Ly = 0.5; Lz = 1.5;
 k = 1; l = 1; m = 1;
 nu = 0.25;
 Kx = pi*k/Lx; Ky = pi/2*l/Ly; Kz = pi/2*m/Lz;
 K = Kx^2 + Ky^2 + Kz^2;
 tau = 1/(nu*K);

 close all
 set_plot_defaults
 figure
 hold on
 str_symbol{1} = '-k^'; 
 str_symbol{2} = '-ko';
 str_symbol{3} = '-ks';

 for i=1:length(h_vect)
  for j=1:length(k_vect)
 
   fname = ['ch_icT_h',num2str(h_vect(i)),'k',num2str(k_vect(j))];
   grid_name = [fname,'-P',num2str(P,'%.3i'),'-grid.octxt'];
   load(grid_name);
   base_name = [fname,'-P',num2str(P,'%.3i'),'-base.octxt'];
   load(base_name);
   res_name =  [fname,'-P',num2str(P,'%.3i'),'-selectedres.octxt'];
   data = collect_fast_output(res_name);
   vect_plot = [1:3:length(data.times),length(data.times)];
   idata = interp_fast_output(grid,base,data,x);
   plot(data.times(vect_plot),idata(6,vect_plot),str_symbol{j})
  
  end
 end

 t = data.times;
 c_e = exp(-t/tau)*sin(Kx*x(1))*cos(Ky*x(2))*cos(Kz*x(3));
 plot(t(vect_plot),c_e(vect_plot),'-b*')
 axis([0 1 0 1.2])
 xlabel('t')
 ylabel('c')
 legend('DG-COMP k1','DG-COMP k2','DG-COMP k3','esatta')
 %print -depsc2 fname

return
