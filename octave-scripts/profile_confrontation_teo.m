function profile_confrontation_teo(np,res,file,varargin)
% MACHINE DEPENDENT, CHANNEL FLOW ONLY, DEPRECATED
% np:  number of partitions minus 1 (the counter starts from 0) (can be an array)
% res: time selected for statistics (can be an array long as the various cases)
% file: if equal to 1 print a file containing statistics
% WARNING: to be used only with cases with Teo's FOLDER STRUCTURE AND 
% NAMES
% In varargin insert a series of strings indicating case root folder name
clc
close all

%set (findobj (gca, "-property", "keylabel"), "interpreter",   
%"none") 



%% --------- DNS DATA LOAD ------------------
ref_path = '/home/matteo/Dropbox/Poli/Phd/MainProject/Datas/DNS/M0.7/';

reTauDns = 178.77;
%mean profile
dat = importdata([ref_path, 'Umean.dat'],' ',2);
dns.Umean.y = (dat.data(:,1)+1)*reTauDns;
dns.Umean.val = dat.data(:,2);

%Reynolds stresses
dat = importdata([ref_path, 'tauuu.dat'],' ',2);
dns.tauuu.y = (dat.data(:,1)+1)*reTauDns;
dns.tauuu.val = dat.data(:,2);

dat = importdata([ref_path, 'tauuv.dat'],' ',2);
dns.tauuv.y = (dat.data(:,1)+1)*reTauDns;
dns.tauuv.val = dat.data(:,2);

dat = importdata([ref_path, 'tauvv.dat'],' ',2);
dns.tauvv.y = (dat.data(:,1)+1)*reTauDns;
dns.tauvv.val = dat.data(:,2);

dat = importdata([ref_path, 'tauww.dat'],' ',2);
dns.tauww.y = (dat.data(:,1)+1)*reTauDns;
dns.tauww.val = dat.data(:,2);

%Rms
dat = importdata([ref_path, 'rmsu.dat'],' ',2);
dns.rmsu.y = (dat.data(:,1)+1)*reTauDns;
dns.rmsu.val = dat.data(:,2);

dat = importdata([ref_path, 'rmsv.dat'],' ',2);
dns.rmsv.y = (dat.data(:,1)+1)*reTauDns;
dns.rmsv.val = dat.data(:,2);

dat = importdata([ref_path, 'rmsw.dat'],' ',2);
dns.rmsw.y = (dat.data(:,1)+1)*reTauDns;
dns.rmsw.val = dat.data(:,2);


%tke
dat = importdata([ref_path, 'turbKinEn.dat'],' ',2);
dns.tke.y = (dat.data(:,1)+1)*reTauDns;
dns.tke.val = dat.data(:,2);


%% --- Normalization Values ---
k_u = 1;
k_tau = k_u^2;
 % reystress
%load('/home/michele/DNS/Kim, Moi, Moser/chan180/profiles/chan180.reystress')
 %y_DNS=chan180(:,2);
 %tau_uu_DNS=chan180(:,3);
 %tau_vv_DNS=chan180(:,4);
 %tau_ww_DNS=chan180(:,5);
 %tau_uv_DNS=chan180(:,6);
 %tke_DNS=tau_uu_DNS+tau_vv_DNS+tau_ww_DNS;
 % viscs
%load('/home/michele/DNS/Kim, Moi, Moser/chan180/profiles/chan180.means')
 %u_media_DNS=chan180(:,3);
 %w_media_DNS=chan180(:,5);
 %fm_DNS=trapz(y_DNS,u_media_DNS);

%k_u= fm_DNS/178.77;
%k_tau=k_u^2;

%% ------- SIMULATIONS DATA LOAD ---------------
if(size(res) == 1)
	res = res*ones(1,nargin -3);
end
if(size(np) == 1)
	np = np*ones(1,nargin -3);
end
for j=1:nargin -3
%First
  case_folder_name = varargin{j};
  basename{j} = trim_machine_name(case_folder_name);
  section_path = ['./Grids/',basename{j}, '_grid-stats.octxt'];
  cd(case_folder_name)

  [data]=extract_statistics(section_path,['./Output/',basename{j}],res(j),np(j));
 
  % Preliminary cycle to determine y and interpolation factor
  for i = 1:length(data)/2+1
    y(i) = (1+data(i).y)*178.77;
  end

  for i=1:length(data)
    u_media(i).case(j) =  data(i).values(3)*k_u;
    v_media(i).case(j) =  data(i).values(4)*k_u;
    w_media(i).case(j) =  data(i).values(5)*k_u;
    tau_uu(i).case(j) =  data(i).values(6)*k_tau; 
    tau_uv(i).case(j) =  data(i).values(7)*k_tau;
    tau_vv(i).case(j) =  data(i).values(10)*k_tau;   
    tau_ww(i).case(j) =  data(i).values(14)*k_tau;
    rms_u(i).case(j) =  sqrt(data(i).values(6))*k_u; 
    rms_v(i).case(j) =  sqrt(data(i).values(10))*k_u;   
    rms_w(i).case(j) =  sqrt(data(i).values(14))*k_u;      
    tau_mod_uu(i).case(j) =  data(i).values(17)*k_tau; 
    tau_mod_uv(i).case(j) =  data(i).values(18)*k_tau;
    tau_mod_vv(i).case(j) =  data(i).values(21)*k_tau;   
    tau_mod_ww(i).case(j) =  data(i).values(25)*k_tau;
    tke(i).case(j) = tau_uu(i).case(j)+tau_vv(i).case(j)+ tau_ww(i).case(j)+tau_mod_uu(i).case(j)+tau_mod_vv(i).case(j)+tau_mod_ww  (i).case(j);
  end


  cd ../

end 

%% ----- SIMULATIONS DATA PROCESSING ---------------
for j=1:nargin -3
  for i=1:length(data)/2+1
    tau_uu_mean(i).case(j)=(tau_uu(i).case(j)+tau_uu(end +1-i).case(j))/2;
    tau_vv_mean(i).case(j)=(tau_vv(i).case(j)+tau_vv(end +1-i).case(j))/2;
    tau_ww_mean(i).case(j)=(tau_ww(i).case(j)+tau_ww(end +1-i).case(j))/2;
    tau_uv_mean(i).case(j)=(tau_uv(i).case(j)-tau_uv(end +1-i).case(j))/2; 
    tau_mod_uu_mean(i).case(j)=(tau_mod_uu(i).case(j)+tau_mod_uu(end +1-i).case(j))/2;
    tau_mod_vv_mean(i).case(j)=(tau_mod_vv(i).case(j)+tau_mod_vv(end +1-i).case(j))/2;
    tau_mod_ww_mean(i).case(j)=(tau_mod_ww(i).case(j)+tau_mod_ww(end +1-i).case(j))/2;
    tau_mod_uv_mean(i).case(j)=(tau_mod_uv(i).case(j)-tau_mod_uv(end +1-i).case(j))/2; 
    rms_u_mean(i).case(j)=(rms_u(i).case(j)+rms_u(end +1-i).case(j))/2;
    rms_v_mean(i).case(j)=(rms_v(i).case(j)+rms_v(end +1-i).case(j))/2;
    rms_w_mean(i).case(j)=(rms_w(i).case(j)+rms_w(end +1-i).case(j))/2;
    u_media_mean(i).case(j)=(u_media(i).case(j)+u_media(end +1-i).case(j))/2;
    w_media_mean(i).case(j)=(w_media(i).case(j)+w_media(end +1-i).case(j))/2;
    tke_mean(i).case(j)=(tke(i).case(j)+tke(end+1-i).case(j))/2;
  end
end

%% ----- STATISTICS PROFILES WRITING --------------------
if(file==1)
  for j=1:nargin -3
    fid=fopen(['./Postprocessing/',trim_machine_name(varargin{j}),'-profiles'],'w');
    fprintf(fid,'%% y, tau_uu, tau_uu_mod, tau_ww, tau_ww_mod,tau_ww, tau_ww_mod, tau_uv, tau_uv_mod, tke, u_mean, v_mean, w_mean, rho_mean,rho_rms, t_mean, t_rms \n');

    for i=1:length(data)/2+1
      fprintf(fid,'%f   ',y(i));
      fprintf(fid,'%f   ',tau_uu_mean(i).case(j)+tau_mod_uu_mean(i).case(j));
      fprintf(fid,'%f   ',tau_vv_mean(i).case(j)+tau_mod_vv_mean(i).case(j));
      fprintf(fid,'%f   ',tau_ww_mean(i).case(j)+tau_mod_ww_mean(i).case(j));
      fprintf(fid,'%f   ',tau_uv_mean(i).case(j)+tau_mod_uv_mean(i).case(j));
      fprintf(fid,'%f   ',tke_mean(i).case(j));
      fprintf(fid,'%f   ',u_media_mean(i).case(j));
      fprintf(fid,'%f \n',w_media_mean(i).case(j));
    end

    fclose(fid)
  end
end
%cd visc/

%[data_visc]=extract_statistics('../grid_8x16x12_192p-stats.octxt','M_0.2_visc',res,191);
 
% Preliminary cycle to determine y and interpolation factor
 
% for i=1:length(data_visc)
%   u_media_visc(i) =  data_visc(i).values(3)*k_u;
%   v_media_visc(i) =  data_visc(i).values(4)*k_u;
%   w_media_visc(i) =  data_visc(i).values(5)*k_u;
%   tau_uu_visc(i) =  data_visc(i).values(6)*k_tau; 
%   tau_uv_visc(i) =  data_visc(i).values(7)*k_tau;
%   tau_vv_visc(i) =  data_visc(i).values(10)*k_tau;   
%   tau_ww_visc(i) =  data_visc(i).values(14)*k_tau;   
%   tau_mod_uu_visc(i) = 0; 
%   tau_mod_uv_visc(i) =  0;
%   tau_mod_vv_visc(i) =  0;   
%   tau_mod_ww_visc(i) =   0;
%   tke_visc(i) = tau_uu_visc(i)+tau_vv_visc(i)+ tau_ww_visc(i);%+tau_mod_uu_visc(i)+tau_mod_vv_visc(i)+tau_mod_ww_visc(i);
% end
%for i=1:length(data_visc)/2+1
%  tau_uu_visc_mean(i)=(tau_uu_visc(i)+tau_uu_visc(end +1-i))/2;
%  tau_vv_visc_mean(i)=(tau_vv_visc(i)+tau_vv_visc(end +1-i))/2;
%  tau_ww_visc_mean(i)=(tau_ww_visc(i)+tau_ww_visc(end +1-i))/2;
%  tau_uv_visc_mean(i)=(tau_uv_visc(i)-tau_uv_visc(end +1-i))/2;
%  u_media_visc_mean(i)=(u_media_visc(i)+u_media_visc(end +1-i))/2;
%  w_media_visc_mean(i)=(w_media_visc(i)+w_media_visc(end +1-i))/2;
%  tke_visc_mean(i)=(tke_visc(i)+tke_visc(end+1-i))/2;
%end

%% ------------------------ PLOT --------------------------
col  = { '-ro', '-go', '-bo','-mo','-co', '-yo'};

%% --- tau_uu

hold on
for j=1:nargin-3
  % Need this in order to fix an octave bug (cs list cannot be further indexed)
  for i=1:length(tau_uu_mean)
      var(i)=tau_uu_mean(i).case(j) + tau_mod_uu_mean(i).case(j);
  end      
 plot(y,var,'LineWidth',2,col{j})
end

plot(dns.tauuu.y,dns.tauuu.val,'--k')
legend(basename{:},'DNS')
xlim([0 180])
xlabel('y^+')
ylabel('\tau_{xx}')


%% --- tau_vv

figure()
hold on
for j=1:nargin-3
  % Need this in order to fix an octave bug (cs list cannot be further indexed)
  for i=1:length(tau_vv_mean)
      var(i)=tau_vv_mean(i).case(j) + tau_mod_vv_mean(i).case(j);
  end      
 plot(y,var,'LineWidth',2,col{j})
end
plot(dns.tauvv.y,dns.tauvv.val,'--k')
legend(basename{:},'DNS')
xlim([0 180])
xlabel('y^+')
ylabel('\tau_{yy}')


%% --- tau_ww

figure()
hold on
for j=1:nargin-3
  % Need this in order to fix an octave bug (cs list cannot be further indexed)
  for i=1:length(tau_ww_mean)
      var(i)=tau_ww_mean(i).case(j) + tau_mod_ww_mean(i).case(j);
  end      
 plot(y,var,'LineWidth',2,col{j})
end
plot(dns.tauww.y,dns.tauww.val,'--k')
legend(basename{:},'DNS')
xlim([0 180])
xlabel('y^+')
ylabel('\tau_{zz}')


%% --- tau_uv

figure()
hold on
for j=1:nargin-3
  % Need this in order to fix an octave bug (cs list cannot be further indexed)
  for i=1:length(tau_uv_mean)
      var(i)= -tau_uv_mean(i).case(j) - tau_mod_uv_mean(i).case(j);
  end      
hl= plot(y,var,'LineWidth',2,col{j})
end
plot(dns.tauuv.y,dns.tauuv.val,'--k')
legend(basename{:},'DNS')
xlim([0 180])
xlabel('y^+')
ylabel('\tau_{xy}')


%% --- rms_u

figure()
hold on
for j=1:nargin-3
  % Need this in order to fix an octave bug (cs list cannot be further indexed)
  for i=1:length(rms_u_mean)
      var(i)=rms_u_mean(i).case(j); % + tau_mod_ww_mean(i).case(j);
  end      
 plot(y,var,'LineWidth',2,col{j})
end
plot(dns.rmsu.y,dns.rmsu.val,'--k')
legend(basename{:},'DNS')
xlim([0 180])
xlabel('y^+')
ylabel('rms(u)')

%% --- rms_v

figure()
hold on
for j=1:nargin-3
  % Need this in order to fix an octave bug (cs list cannot be further indexed)
  for i=1:length(rms_v_mean)
      var(i)=rms_v_mean(i).case(j); % + tau_mod_ww_mean(i).case(j);
  end      
 plot(y,var,'LineWidth',2,col{j})
end
plot(dns.rmsv.y,dns.rmsv.val,'--k')
legend(basename{:},'DNS')
xlim([0 180])
xlabel('y^+')
ylabel('rms(v)')

%% --- rms_u

figure()
hold on
for j=1:nargin-3
  % Need this in order to fix an octave bug (cs list cannot be further indexed)
  for i=1:length(rms_w_mean)
      var(i)=rms_w_mean(i).case(j); % + tau_mod_ww_mean(i).case(j);
  end      
 plot(y,var,'LineWidth',2,col{j})
end
plot(dns.rmsw.y,dns.rmsw.val,'--k')
legend(basename{:},'DNS')
xlim([0 180])
xlabel('y^+')
ylabel('rms(w)')




%% --- tke 

figure()
hold on
for j=1:nargin-3
  % Need this in order to fix an octave bug (cs list cannot be further indexed)
  for i=1:length(tke_mean)
      var(i)= tke_mean(i).case(j);
  end      
 plot(y,var,'LineWidth',2,col{j})
end
plot(dns.tke.y,dns.tke.val,'--k')
legend(basename{:},'DNS')
xlim([0 180])
xlabel('y^+')
ylabel('k_{turb}')

%% --- u_media

figure()
hold on
for j=1:nargin-3
  % Need this in order to fix an octave bug (cs list cannot be further indexed)
  for i=1:length(u_media_mean)
      var(i)= u_media_mean(i).case(j);
  end      
 semilogx(y,var,'LineWidth',2,col{j})
end
semilogx(dns.Umean.y,dns.Umean.val,'--k')
legend(basename{:},'DNS')
xlabel('y^+')
ylabel('<U>')
 %xlim([0 180])

%% --- w_media

%figure()
%hold on
%for j=1:nargin-4
%  % Need this in order to fix an octave bug (cs list cannot be further indexed)
%  for i=1:length(w_media_mean)
%      var(i)= w_media_mean(i).case(j);
%  end      
% plot(y,var,'LineWidth',2,col{j})
%end
%plot(y_DNS,w_media_DNS,'--k')
%legend(varargin{:},'DNS')
% xlim([0 180])

