function [p,e,t] = new1d_grid(ne,a,b)
% [p,e,t] = new1d_grid(ne,a,b)
%
% Define a 1D grid with ne elements on the interval [a,b].

 if(b<a)
   x = b;
   b = a;
   a = x;
 end

 p = linspace(a,b,ne+1);
 t(1,:) = [1:ne];
 t(2,:) = [2:ne+1];
 e(:,1) = [ne+1;0;0;1];
 e(:,2) = [   1;0;0;2];

return

