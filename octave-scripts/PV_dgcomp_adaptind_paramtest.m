function data = PV_dgcomp_adaptind_paramtest(gridname, basename, adaptname, outname, k,l)
% Very crude and simple implementation for hdf5 adaptive results
% data = PV_dgcomp_series(fname,itime,N,k,l,outfile)
% data = PV_dgcomp_series(fname,itime,N,k,l,outfile,select)
%
% Call PV_dgcomp for a series of files and prepare a .pvd file to
% collect the generated vtk files.
%
% fname nust contain a '$' to indicate the suffix 'grid.octxt', 
% 'base.octxt' or 'res-XXXX.h5'.
%
% EXAMPLE: './Output/basename-$'
%
% itime: label(s) of the selected output times
% EXAMPLE: for 'ch_icT-res-0003.h5', itime=3
% 
% k: FE order
% k_max: Max FE order (redoundant, considere removing)
% l: order of interpolation (ca be taken less or equal to k) ??
% outfile: name of the file without extension (string)
% outdir: folder in which to store the results

%The optional argument 'select' can be used as follows:
% C|P : conservative or primitive variables
% D|T : deviations or total values
%
% Moreover, the collected and/or postprocessed data are collected in
% the output argument data in a format which can be passed to
% dgcomp_interpolation to interpolate at arbitrary points.

 part_format = '%.4i'

 grid = load(gridname);
 fprintf('...loaded grid\n');
 fflush(stdout);
 bases = load(basename);
 base = bases.base(k+1);
 fprintf('...loaded base\n');
 fflush(stdout);
 adaptdata = load(adaptname);
 fprintf('...loaded adapt data\n');
 fflush(stdout);
 
 uuu = zeros(1,4,grid.grid.ne);
% uuu(1,1,:) = adaptdata.indicator;
% uuu = 1/base.p(1,1)*uuu;
% varnames = {'indicator'};
 varnames = {'dummy'};
% outname = ['./Postprocessing/',outname,'.vtu'];
 
 diags = zeros(0,size(uuu,2),size(uuu,3)); % change this!!!

 % Add the same vars as diagnostics
 for [val, key] = adaptdata 
	diags(end+1,1,:) = 1/base.p(1,1)*permute(val,[3 1 2]);
	varnames{end+1} = key;
 end

 PV_dgcomp_nscalars(grid.grid,base,uuu,k,l,outname,'pippo', ...
               diags,varnames);


return

