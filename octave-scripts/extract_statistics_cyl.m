function [data]=extract_statistics_cyl(section_file,fname)
% section_file must be a string, containing section_file's full name
% fname must be a string with the hdf5 file of results

 load(section_file)
 n_section=size(SC,2);
 grid_stat=cell2struct(SC,'sections',n_section);
 
 % Collect side_sections position
 y_pos = [];
 for i=1:n_section
   if(strcmp(grid_stat(i).sections.section_type,'point_section'))
     xy_sec= grid_stat(i).sections.section_name;
     xy_sections(i).xy_sec=xy_sec;
     xy_sections(i).x_pos=grid_stat(i).sections.x(1); %extract the x and y coordinate
     xy_sections(i).y_pos=grid_stat(i).sections.x(2); %which is the same on all
                                                     %section points
%     y_pos = [y_pos , grid_stat(i).sections.x(2)];
   end
 end
 
    % Load results from the hdf5 res file
    file_res=[fname];
    results = load(file_res);
    for j=1:length(xy_sections)
        for k=1:length(results.sections.point_sections.nregs) %they should be ordered so 
                                                    %a single cycle should be
                                                    %enough, but who knows...
            if(strcmp(strtrim(results.sections.point_sections.section_name(k,:)),...
                       xy_sections(j).xy_sec))
            data(j).values = results.sections.point_sections.regs(:,:,k) ;% saving data
            xy_sections(j).xy_sec='empty' ;%In order to avoid repetitions
            data(j).x= xy_sections(j).x_pos;
            data(j).y= xy_sections(j).y_pos;
            end                                        
        end
    end 
   

