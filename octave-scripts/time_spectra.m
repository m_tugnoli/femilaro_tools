function [f,M] = time_spectra(t,y)
% [f,M] = time_spectra(t,y)
%
% Spectral analysis of the time series (t,y). It is assumed that t is
% equally spaced, so that dt=t(2)-t(1).
%
% f: array of the discrete frequencies [1/s]
% M: amplitude of the spectrum

 dt = t(2)-t(1);
 N = length(t);
 DT = N*dt;
 % Make sure k has the same shape of t
 k = t;
 for j=1:N
   k(j) = j-1;
 end

 % Output frequencies (including the effect of fftshift)
 f = 1/DT * ( k - floor(N/2) );

 % Fourier transform of the time series
 hat_y = DT/(sqrt(2*pi)*N) * fftshift(fft(y));

 % Amplitude of the Fourier coefficients
 M = abs(hat_y);

return
