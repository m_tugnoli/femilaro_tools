function chL_stat(fname,itime)
% function chL_stat(fname,itime)
%
% Function for a preliminary check of the calculated output
% fname is the name of the statistics file
% itime can be a vector of different times 

 close all
 n = length(itime);

 % Stat grid
 fname_sgrid = [fname,'-s_grid.octxt'];
 sgrid = load(fname_sgrid);
 y = sgrid.s_grid.yp;
 clear sgrid

 % Global values over time
 t = []; mean_g = []; rms_g = [];
 for j=1:n
   fname_stat = [fname,'-stat-',num2str(itime(j),'%.4i'),'.octxt'];
   stat = load(fname_stat);
   t = [t,stat.time];
   mean_g = [mean_g, stat.stat.uuu_g(1,:)'];
   rms_g = [rms_g, stat.stat.uuu_g(2,:)'];

   if (j==n)
     un = permute(stat.stat.uu(1,1,:),[1,3,2]);
     Tn = stat.stat.T(1,:);
   end

   clear stat
 end
 % global mean values
 rho_mg = mean_g(1,:);
 e_mg = mean_g(2,:);
 m_mg = sqrt( sum(mean_g(3:5,:).^2) ); % momentum
 mg = figure; hold on; grid on
 plot(t,rho_mg,'b',t,e_mg,'r',t,m_mg,'k')
 legend('\rho','e','m')
 xlabel('time'), ylabel('Global mean values')
 title('Global MEAN values')

 set_plot_defaults
 % Density
 figure
 plot(t,rho_mg,'b')
 xlabel('t'), ylabel('\rho')
 print -depsc2 chL-rho-csv
 % Momentum
 figure
 plot(t,m_mg,'b')
 xlabel('t'), ylabel('\rho u')
 print -depsc2 chL-m-csv
 % Total energy
 figure
 plot(t,e_mg,'b')
 xlabel('t'), ylabel('\rho e')
 print -depsc2 chL-e-csv

%{
 % global rms values
 rho_rmsg = rms_g(1,:);
 e_rmsg = rms_g(2,:);
 m_rmsg = sqrt( sum(rms_g(3:5,:).^2) ); % momentum
 rmsg = figure; hold on; grid on
 plot(t,rho_rmsg,'b',t,e_rmsg,'r',t,m_rmsg,'k')
 legend('\rho','e','m')
 xlabel('time'), ylabel('Global RMS values')
 title('Global RMS values')

 % Exact solution (Finite Elements)
 [ye,Te,ue,ube] = laminar_analytical_FE();
 % Velocity (streamwise component)
 u = figure; hold on; grid on
 plot(ye,ue,'b',y,un,'k')
 axis([-1 1 0 1.6])
 legend('exact','last')
 xlabel('y'), ylabel('u')
 title('Streamwise velocity')
 % Temperature
 T = figure; hold on; grid on
 plot(ye,Te,'b',y,Tn,'k')
 axis([-1 1 1 1.1])
 legend('exact','last')
 xlabel('y'), ylabel('T')
 title('Temperature')
%}

return
