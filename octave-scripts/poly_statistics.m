function poly_statistics(filename)
%load a polynomial degree specification file and print out some
%statistics

poly_deg = load(filename);
elems = length(poly_deg.deg_lim);
for i = 1:4
elems_degs(i) = sum(poly_deg.deg_lim == i);
end
dof_deg = [4 10 20 35];
dofs = sum(elems_degs.*dof_deg);
full_dofs = dof_deg*elems;
dofs_diff = (dofs-full_dofs)/dofs*100;
fprintf('elements: %d\n',elems)
fprintf('elements for each degree 1,4: %d %d %d %d\n',elems_degs)
fprintf('total dofs: %d\n',dofs)
fprintf('difference with full degree of 1,4: %d %d %d %d\n',dofs_diff)
