function [t,uuu_mg] = vol_stat(fname,itime,N,outfile)
% function [t,uuu_mg] = vol_stat(fname,itime,N,outfile)
% 
% A posteriori evaluation of integral mean and rms values
% averaged over volume (spatial average).

 Npos = find(fname=='%');
 % time loop
 uuu_mg = zeros(5,length(itime));
 t = zeros(1,length(itime));
 for j=1:length(itime)
   vol = 0;
   uuu_g = zeros(5,1);
   % partition loop
   for i=0:N
     fnameN = [fname(1:Npos-1),num2str(i,'%.3i'),fname(Npos+1:end)];
     Xpos = find(fnameN=='$');
     fnameNX = [fnameN(1:Xpos-1),'grid',fnameN(Xpos+1:end)];


     grid = load(fnameNX);
     fnameNX = [fnameN(1:Xpos-1),'base',fnameN(Xpos+1:end)];
     base = load(fnameNX);
     fnameNX = [fnameN(1:Xpos-1),'res-',num2str(itime(j),'%.4i'), ...
                fnameN(Xpos+1:end)];
     res = load(fnameNX);

     % constants
     uuu_size = size(res.uuu,1);
     part_vol = grid.grid.vol;

     % elements loop
     for ie=1:grid.grid.ne
       uuug = 0;
       wg = grid.grid.e(ie).det_b * base.base.wg;
       for ipk=1:base.base.pk
         uuug = uuug + res.uuu(:,ipk,ie) * base.base.p(ipk,:);
       end
       for iv=1:uuu_size
         uuu_g(iv) = uuu_g(iv) + sum( wg*uuug(iv,:)' );
       end
     end

     % accumulation between partition
     vol = vol + part_vol;
   end

   % divide by total volume
   uuu_mg(:,j) = uuu_g/vol;

   t(j) = res.time;

 end

 out_file = [outfile,'.octxt'];
 save(out_file, 't', 'uuu_mg')

return
