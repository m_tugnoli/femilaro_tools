function [y,t,ui_xz] = rhov(fname,np1,itime)
% function [y,t,ui_xz] = rhov(fname,np1,itime)
%
% This function post-processes a series of field at
% different time in order to evaluate tha spatial mean
% for the rho*v quantity by interpolation technique.
%
% Use in connection with ev_flux.m to make evidence
% of the error interpolation for the shear stress when
% the solution is interpolated inside the element instead of
% in correspondence of planes coincident with cells' subdivision.

 % Build arrays for the original grid
 for i=1:np1
   fname_grid = [fname,'-P',num2str(i-1,'%.3i'),'-grid.octxt'];
   load(fname_grid);
   grid1{i} = grid;
   ddc_grid1{i} = ddc_grid;
   clear grid ddc_grid
 end
 fname_base = [fname,'-P',num2str(0,'%.3i'),'-base.octxt'];
 load(fname_base);
 base1 = base;
 clear base

 % 2D matrix with points for each plane
 N = [10,10];
 limits = [0,2*pi,-2/3*pi,2/3*pi];
 [xx,dummy,dummy] = new2d_grid(N,limits);
 npp = size(xx,2);
 % vector of planes
 N = [10,25,10];
 limits = [0,2*pi,-1,1,-2/3*pi,2/3*pi];
 y1 = 0.01;
 [dummy,dummy,dummy,y] = new3d_grid_stretch(N,limits,y1);

 nplanes = length(y);
 ntimes = length(itime);
 xxx = zeros(3,npp);
 ui_xz = zeros(5,nplanes,ntimes);
 for n=1:ntimes
   % recover the res field file
   for j=1:np1
     fname_res = [fname,'-P',num2str(j-1,'%.3i'),'-res-',num2str(itime(n),'%.4i'),'.octxt'];
     res = load(fname_res);
     uuu1{j} = res.uuu;
     t(n) = res.time;
     clear res
   end
   % interpolate and mean over planes
   for iy=1:nplanes
     xxx(1,:) = xx(1,:);
     xxx(2,:) = y(iy);
     xxx(3,:) = xx(2,:);
     ui = dgcomp_interpolation( grid1,ddc_grid1,base1,uuu1,xxx );
     ui_xz(:,iy,n) = sum(ui,2)/npp;
   end
 end

return
