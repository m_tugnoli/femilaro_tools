function [p,e,t,yp] = new3d_grid_stretch(N,limits,y1)
% [p,e,t,yp] = new3d_grid_stretch(N,limits,y1)
%
% Build a 3D structured grid using N(1) subdivisions in x, N(2)
% subdivisions in y and N(3) subdivisions in z. The domain is the cube
% [limits(1),limits(2)]x[limits(3),limits(4)]x[limits(5),limits(6)].
% Limits(3) and limits(4) must be given in a symmetric way with respect to y=0.
% If all the N(:) are even, it is guaranteed that each element has at
% least one internal node.
% 
% Tangent hyerbolic stretching applied in y-direction. 
% y_j = -tanh(gamma*(1-2j/N(2)))/tanh(gamma)
% Gamma parameter is calculated in order to have the first point
% of the in grid at the specified y1 distance from the wall.

 % Number of vertices
 nv = (N(1)+1)*(N(2)+1)*(N(3)+1);
 p = zeros(3,nv);
 % Numbering of vertices: forall x, fix y and cycle over z
 %     | 0 0 .. 0  0    .. 0   .. 1 .. 1  1    .. |
 % p = | 0 0 .. 0  1    .. 1   .. 0 .. 0  1    .. |
 %     | 0 1 .. N3 N3+1 .. 2N3 .. 0 .. N3 N3+1 .. |
 for i=1:N(1)+1
   for j=1:N(2)+1
     for k=1:N(3)+1
       ijk = (i-1)*(N(2)+1)*(N(3)+1) + (j-1)*(N(3)+1) + k;
       p(:,ijk) = [i,j,k]' - 1; % numbering begins from 0
     end
   end
 end

 % Physical/Geometrical points coordinates. Uniform subdivision in the x-z directions.
 % Tangent hyperbolic stretching in the y direction.

 gamma = find_gamma(y1,N(2)) % stretching parameter
 jj = 0:N(2);

 yp = -tanh(gamma*(1-2*jj/N(2)))/tanh(gamma)*(limits(4)-limits(3))/2;

 for i=1:3
   j = (i-1)*2 + 1;
   k = j+1;

   % y-direction stretching
   if i==2
    % adimensional between -1 and +1 -> multiply by half-channel heigth and translate
    y = -tanh(gamma*(1-2*jj/N(2)))/tanh(gamma)*(limits(k)-limits(j))/2 + (limits(k)+limits(j))/2;
    % 2D (face) extension
    yy = ones(N(3)+1,1)*y;
    % y2 = zeros(1,(N(2)+1)*(N(3)+1));
    y2 = [];
     for kk = 1:N(2)+1
      y2 = [y2, yy(:,kk)'];
     end
    % 3D (volume) extension
    yyy = ones(N(1)+1,1)*y2;
    % y3 = zeros(1,(N(2)+1)*(N(3)+1)*(N(1)+1));
    y3 = [];
     for ii = 1:N(1)+1
      y3 = [y3, yyy(ii,:)];
     end
    % Defining y-vertices in matrix p
    p(i,:) = y3;

   else % x,z directions
    p(i,:) = ((limits(k)-limits(j))/N(i))*p(i,:) + limits(j);
   end

 end

 % Number of elements: 6 tetrahedra for each cube (N1*N2*N3 cubes)
 ne = 6*N(1)*N(2)*N(3);
 t = zeros(4,ne); % tetrahedra (elements)
 e = []; % edges
 for i=1:N(1)
   if(2*floor(i/2)==i) % index i is even
     oj = 'south';
   else
     oj = 'north';
   end
   for j=1:N(2)
     if(2*floor(i/2)==i) % index j is even
       ok = 'down';
     else
       ok = 'up';
     end
     for k=1:N(3)
       % index number of the element
       ijk = (i-1)*N(2)*N(3)*6 + (j-1)*N(3)*6 + (k-1)*6;
       % vertices of the considered cube
       iv(1) = (i-1)*(N(2)+1)*(N(3)+1) + (j-1)*(N(3)+1) + k;
       iv(2) = (i-1)*(N(2)+1)*(N(3)+1) + (j-1)*(N(3)+1) + k+1;
       iv(3) = (i-1)*(N(2)+1)*(N(3)+1) + (j-1+1)*(N(3)+1) + k;
       iv(4) = (i-1)*(N(2)+1)*(N(3)+1) + (j-1+1)*(N(3)+1) + k+1;
       iv(5) = (i-1+1)*(N(2)+1)*(N(3)+1) + (j-1)*(N(3)+1) + k;
       iv(6) = (i-1+1)*(N(2)+1)*(N(3)+1) + (j-1)*(N(3)+1) + k+1;
       iv(7) = (i-1+1)*(N(2)+1)*(N(3)+1) + (j-1+1)*(N(3)+1) + k;
       iv(8) = (i-1+1)*(N(2)+1)*(N(3)+1) + (j-1+1)*(N(3)+1) + k+1;
       % function for meshing a single cube
       [tt,ex,ey,ez] = new3d_grid_singlecube(iv,[ok,'_',oj]);
       % Storing the new tetrahedra in matrix t
       t(:,ijk+1:ijk+6) = tt;
       % Storing the boundary edges
       if(i==1)
         e = [e,ex(:,:,1)];
       endif
       if(i==N(1))
         e = [e,ex(:,:,2)];
       endif
       if(j==1)
         e = [e,ey(:,:,1)];
       endif
       if(j==N(2))
         e = [e,ey(:,:,2)];
       endif
       if(k==1)
         e = [e,ez(:,:,1)];
       endif
       if(k==N(3))
         e = [e,ez(:,:,2)];
       endif
       if(strcmp(ok,'up')==1)
         ok = 'down';
       else
         ok = 'up';
       endif
     end
     if(strcmp(oj,'north')==1)
       oj = 'south';
     else
       oj = 'north';
     endif
   end
 end

return

