function [n] = nll_sympoly(d,k)
% function [n] = nll_sympoly(d,k)
% 
% Function for evaluating the number of degree of freedom
% on each element as function of spatial dimension d and 
% polynomial order k.

   if(d == 0) 
     n = 1;
   elseif(d == 1)
     n = k+1;
   else
     n = 0;
     for i = 0:k
       n = n + nll_sympoly(d-1,i);
     end
   endif
 
endfunction
