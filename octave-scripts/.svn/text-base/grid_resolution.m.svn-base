function [h_eq,h_p,gdl,f] = grid_resolution(L,N,y1,d,k)
% function [h_eq,h_p,gdl,f] = grid_resolution(L,N,y1,d,k)
%
% This function evaluates grid resolution.
% Input.
% L: vector with limits of the domain
%    L = [x0,x1,y0,y1,z0,z1];
% N: vector with the number of the cells
%    N = [Nx, Ny, Nz]
% y1: position of the first cell in y-direction
% k: polynomial degree
% 
% Output.
% h_eq(1),h_eq(2): minimum and maximum equivalent scalar length scale 
% h_p(1),h_p(2),h_p(3),h_p(4): 
%     wall units resolution in x,y (min-max) and z directions
% gdl: total degree of freedom
% f: Lilly1993 correction factor
 
  close all
  Retau = 186;

  % Note
  % h: vector with all the characteristic resolution
  %    h(1) = hx; h(2) = hy_min; h(3) = hy_max; h(4) = hz;

  % Geometric resolution (represented scales)
  hg = zeros(4,1);
  hg(1) = abs(L(2)-L(1))/N(1);
  hg(4) = abs(L(6)-L(5))/N(3);
  gamma = find_gamma(y1,N(2));
  j = 0:N(2);
  y = -tanh(gamma*(1-2*j/N(2)))/tanh(gamma);
  hyg = abs(y(1:end-1)-y(2:end));
  hg(2) = min(hyg); hg(3) = max(hyg);
  % Equivalent geometric scalar length scale
  hg_eq = zeros(2,1);

  % Lilly correction factor (using minimum hy)
  [hg_max, imax] = max(hg);
  if imax==1 
   a1 = hg(2)/hg_max;
   a2 = hg(4)/hg_max;
  elseif imax==3
   a1 = hg(1)/hg_max;
   a2 = hg(4)/hg_max;
  elseif imax==4
   a1 = hg(1)/hg_max;
   a2 = hg(2)/hg_max;
  end 
  f = cosh(sqrt( 4/27*( (log(a1))^2 + (log(a2))^2 - log(a1)*log(a2) ) ));
  hg_eq(1) = f*(hg(1)*hg(2)*hg(4))^(1/3);
  hg_eq(2) = f*(hg(1)*hg(3)*hg(4))^(1/3);

  h_eq = zeros(2,length(k));
  h_p  = zeros(4,length(k));
  gdl  = zeros(1,length(k));
  for i=1:length(k)
   % DG-FEM resolution (resolved scales)
   n_eq = (6*nll_sympoly(d,k(i)))^(1/3);
   gdl(i) = (n_eq^3)*N(1)*N(2)*N(3);
   % Equivalent scalar length scale
   h_eq(:,i) = hg_eq/n_eq;
   % Wall units resolution
   h_p(:,i) = Retau*hg/n_eq;
  end

return
