function uuu2 = dgcomp_grid2grid( grid1,ddc_grid1,base1,uuu1, ...
                                  grid2,base2 )
% uuu2 = dgcomp_grid2grid( grid1,ddc_grid1,base1,uuu1,grid2,base2 )
%
% Interpolate a DG-NS solution from grid1 to grid2. The interpolation
% is done with an L2 projection using the quadrature nodes defined by
% base2.
%
% The input arguments grid1, ddc_grid1 and uuu1 can be arrays in order
% to deal with domain decomposition (base1 is a scalar since we assume
% that all the partitions in grid1 use the same basis).
%
% Note that each quadrature node defined by base2 is located
% separately on the grid1 grids, because we can not rely on the two
% grids to nest exactly. This function thus can be very time
% consuming.

 %---------------------------------------------------------------------
 % Set default data
 xi_default = 1.0/(grid1{1}.d+1); % barycenter
 x_default = 0;
 %---------------------------------------------------------------------

 %---------------------------------------------------------------------
 % Interpolate at the quad. nodes
 igi = 1;
 iei = 1;
 nvars = size(uuu1{1},1); % number of variables
 xg   = zeros(grid2.d,base2.m);
 uue  = zeros(base1.pk,base2.m,nvars);
 xi   = zeros(grid2.d,base2.m);
 PHI  = zeros(base1.pk,base2.m);
 uu   = zeros(nvars,base2.m,grid2.ne);
 e2eg = -ones(grid2.ne,2); % used to speedup serach
 tic
 for ie=1:grid2.ne
   if(mod(ie,floor(grid2.ne/100))==0)
     toc
     disp(['Element ',num2str(ie),' of ',num2str(grid2.ne)]);
     tic
   end
   % Gauss points
   xg = grid2.e(ie).b * base2.xig;
   for id=1:grid2.d
     xg(id,:) = xg(id,:) + grid2.e(ie).x0(id);
   end
   % locate the elements
   % check whether we have located a neighbour already
   for i=1:grid2.d+1
     ie_neig = grid2.e(ie).ie(i);
     if(ie_neig>0)
       if(e2eg(ie_neig,1)>0)
         iei = e2eg(ie_neig,1); % element index
         igi = e2eg(ie_neig,2); % grid index
	 break
       end
     end
   end
   for l=1:base2.m
     [igi,iei,xii] = locate_point(xg(:,l),grid1,ddc_grid1,[igi,iei],1);
     if(iei<0)
       warning(['Unable to locate point ',num2str(xg(:,l)'), ...
             ' in element ',num2str(ie),'; using default value.']);
       xi(:,l) = xi_default;
       uue(:,l,:) = x_default;
       igi = 1; iei = 1;
     else
       for id=1:nvars
         uue(:,l,id) = uuu1{igi}(id,:,iei);
       end
       xi(:,l) = xii(2:grid2.d+1);
       e2eg(ie,:) = [iei,igi];
     end
   end
   % interpolate
   for i=1:base1.pk
     PHI(i,:) = ev_pol(base1.p_s{i},xi);
   end
   for id=1:nvars
     uu(id,:,ie) = sum( uue(:,:,id).*PHI , 1 );
   end
 end
 %---------------------------------------------------------------------

 %---------------------------------------------------------------------
 % Element L2 projections
 % first we need the reference mass matrix (targer grid)
 for i=1:base2.pk
   for j=1:base2.pk
     M(i,j) = sum( base2.wg .* base2.p(i,:) .* base2.p(j,:) );
   end
 end
 Mi = inv(M);
 % local projector
 for i=1:base2.pk
   wgp(i,:) = base2.wg .* base2.p(i,:);
 end
 uMiwgp = Mi * wgp;
 clear M Mi wgp
 % now the element loop
 uuu2 = zeros(nvars,base2.pk,grid2.ne);
 for ie=1:grid2.ne
   for id=1:nvars
     uuu2(id,:,ie) = uMiwgp * uu(id,:,ie)';
   end
 end
 %---------------------------------------------------------------------

return

