function [lcor_mean] = space_correlation(fname,np1,itime,dir,y)
% function [lcor_mean] = space_correlation(fname,np1,itime,dir,y)
%
% This function evaluates space correlation over x or z directions,
% once a particular plane is chosen. Correlations are mean over
% different sampling lines and over different sample times.

 % building xxx
 nl = 5; % considered point-lines on each plane
 switch dir
  case 'x'
    np = 150; % number of points on each line
    for i=1:nl
     xxx{i} = zeros(3,np);
     xxx{i}(1,:) = linspace(0,2*pi,np);
     xxx{i}(2,:) = y;
     xxx{i}(3,:) = -2/3*pi + (4/3*pi)/(nl+1)*i;
    end
    xfft = linspace(0,2*pi,np);
  case 'z'
    np = 100; % number of points on each line
    for i=1:nl
     xxx{i} = zeros(3,np);
     xxx{i}(1,:) = (2*pi)/(nl+1)*i;
     xxx{i}(2,:) = y;
     xxx{i}(3,:) = linspace(-2/3*pi,2/3*pi,np);
    end
    xfft = linspace(-2/3*pi,2/3*pi,np);
 end
 
 % Build arrays for the original grid
 for i=1:np1
   fname_grid = [fname,'-P',num2str(i-1,'%.3i'),'-grid.octxt'];
   load(fname_grid);
   grid1{i} = grid;
   ddc_grid1{i} = ddc_grid;
   clear grid ddc_grid
 end
 fname_base = [fname,'-P',num2str(0,'%.3i'),'-base.octxt'];
 load(fname_base);
 base1 = base;
 clear base

 % time units considered
 lcor_mean = 0;
 ntimes = length(itime);
 for n=1:ntimes
   % recover the res field file
   for j=1:np1
     fname_res = [fname,'-P',num2str(j-1,'%.3i'),'-res-',num2str(itime(n),'%.4i'),'.octxt'];
     res = load(fname_res);
     uuu1{j} = res.uuu;
     clear res
   end
   % interpolate & correlate
   for i=1:nl
     ui = dgcomp_interpolation( grid1,ddc_grid1,base1,uuu1,xxx{i} );
     % correlation of fluctuations of csv variables
     lcor = correlation(ui);
     lcor_mean = lcor_mean + lcor;
   end
 end

 % spatial and temporal mean of the correlation
 lcor_mean = lcor_mean/(nl*ntimes);

return
