function [sim] = sim_stat_load(case_folder_name, ReBulkSim, reTauDns)


 load([case_folder_name, 'Postprocessing/stat_data.mat'])
 
  k_u = 1;
  k_tau = k_u^2;
  % Preliminary cycle to determine y and interpolation factor
  for i = 1:length(data)/2+1
    sim.y(i) = (1+data(i).y)*reTauDns;
  end

  for i=1:length(data)
    sim.yfull(i) = (1+data(i).y)*reTauDns;
    sim.u_media(i) =  data(i).values(3)*k_u;
    sim.v_media(i) =  data(i).values(4)*k_u;
    sim.w_media(i) =  data(i).values(5)*k_u;
    sim.rho_media(i) = data(i).values(1);
    sim.tau_uu(i) =  data(i).values(6)*k_tau; 
    sim.tau_uv(i) =  data(i).values(7)*k_tau;
    sim.tau_uw(i) =  data(i).values(8)*k_tau;
    sim.tau_vv(i) =  data(i).values(10)*k_tau;
    sim.tau_vw(i) =  data(i).values(11)*k_tau; 
    sim.tau_ww(i) =  data(i).values(14)*k_tau;
    sim.rms_u(i) =  sqrt(data(i).values(6))*k_u; 
    sim.rms_v(i) =  sqrt(data(i).values(10))*k_u;   
    sim.rms_w(i) =  sqrt(data(i).values(14))*k_u;      
    %sim.tau_mod_uu(i) =  data(i).values(17)*k_tau; 
    %sim.tau_mod_uv(i) =  data(i).values(18)*k_tau;
    %sim.tau_mod_uw(i) =  data(i).values(19)*k_tau;
    %sim.tau_mod_vv(i) =  data(i).values(21)*k_tau;
    %sim.tau_mod_vw(i) =  data(i).values(22)*k_tau;
    %sim.tau_mod_ww(i) =  data(i).values(25)*k_tau;
    sim.tke(i) = sim.tau_uu(i)+sim.tau_vv(i)+ sim.tau_ww(i);
    %sim.tke(i) = sim.tau_uu(i)+sim.tau_vv(i)+ sim.tau_ww(i)+sim.tau_mod_uu(i)+sim.tau_mod_vv(i)+sim.tau_mod_ww(i);
    %sim.res_tke(i) = sim.tau_uu(i)+sim.tau_vv(i)+ sim.tau_ww(i);
  end



%% -----DATA PROCESSING ---------------

%Anisotropic
  for i=1:length(data)/2+1
    sim.tau_uu_mean(i)=(sim.tau_uu(i)+sim.tau_uu(end +1-i))/2;
    sim.tau_vv_mean(i)=(sim.tau_vv(i)+sim.tau_vv(end +1-i))/2;
    sim.tau_ww_mean(i)=(sim.tau_ww(i)+sim.tau_ww(end +1-i))/2;
    sim.tau_uv_mean(i)=(sim.tau_uv(i)-sim.tau_uv(end +1-i))/2;
    sim.tau_uw_mean(i)=(sim.tau_uw(i)-sim.tau_uw(end +1-i))/2;
    sim.tau_vw_mean(i)=(sim.tau_vw(i)-sim.tau_vw(end +1-i))/2;
	
	%sim.tau_mod_uu_mean(i)=(sim.tau_mod_uu(i)+sim.tau_mod_uu(end +1-i))/2;
    %sim.tau_mod_vv_mean(i)=(sim.tau_mod_vv(i)+sim.tau_mod_vv(end +1-i))/2;
    %sim.tau_mod_ww_mean(i)=(sim.tau_mod_ww(i)+sim.tau_mod_ww(end +1-i))/2;
    %sim.tau_mod_uv_mean(i)=(sim.tau_mod_uv(i)-sim.tau_mod_uv(end +1-i))/2;
    %sim.tau_mod_uw_mean(i)=(sim.tau_mod_uw(i)-sim.tau_mod_uw(end +1-i))/2;
    %sim.tau_mod_vw_mean(i)=(sim.tau_mod_vw(i)-sim.tau_mod_vw(end +1-i))/2;

    sim.rms_u_mean(i)=(sim.rms_u(i)+sim.rms_u(end +1-i))/2;
    sim.rms_v_mean(i)=(sim.rms_v(i)+sim.rms_v(end +1-i))/2;
    sim.rms_w_mean(i)=(sim.rms_w(i)+sim.rms_w(end +1-i))/2;

    sim.u_media_mean(i)=(sim.u_media(i)+sim.u_media(end +1-i))/2;
    sim.w_media_mean(i)=(sim.w_media(i)+sim.w_media(end +1-i))/2;
    sim.rho_media_mean(i)=(sim.rho_media(i)+sim.rho_media(end +1-i))/2;
    sim.tke_mean(i)=(sim.tke(i)+sim.tke(end+1-i))/2;
    %sim.res_tke_mean(i)=(sim.res_tke(i)+sim.res_tke(end+1-i))/2;
  end
  	%get normalizations
	
    sim.deltaRef = 1;
    sim.muWall = 1;
    sim.rhoWall1 = sim.rho_media_mean(1); %first point
    m_rho = (sim.rho_media_mean(2)-sim.rho_media_mean(1))/(sim.y(2)-sim.y(1))*reTauDns;
    sim.rhoWall2 = sim.rho_media_mean(1) - m_rho*sim.y(1)/reTauDns;
    sim.dUdy=sim.u_media_mean(1)/sim.y(1)*reTauDns;%adimensionale
    sim.tauWall = sim.dUdy*sim.muWall; %adimensionale
    sim.ReTau = sqrt(ReBulkSim)*sqrt(sim.rhoWall2*sim.dUdy); %dimensionale
    sim.uTau = sim.ReTau/(ReBulkSim*sim.rhoWall2);
    sim.u_outer_2_inner = 1/sim.uTau;
    sim.u2_outer_2_inner = sim.u_outer_2_inner^2;
	
	
	
