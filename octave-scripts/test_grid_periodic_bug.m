%function test_grid_periodic_bug(gridfile, master, slave)
%fixed input for debug
gridfile = 'adapt1_grid.octxt';
master = 7;
slave = 6;

load(gridfile)

master_ind = find(e(end,:) == master);
slave_ind = find(e(end,:) == slave);

master_e = e(1:3,master_ind);
slave_e = e(1:3,slave_ind);

master_p = unique(vec(master_e));
slave_p = unique(vec(slave_e));

%for each point on the master side
for imp = 1:length(master_p)
   % find all tetra connected to the master point
  [it,jt] = find(t==master_p(imp));
  % make it a vector of points
  interm_p = unique(vec(t(:,jt)));
  % remove the master point
  interm_p = interm_p(interm_p ~= master_p(imp));
  for iip = 1:length(interm_p)
    % find all tetra connected to the intermediate point
    [it,jt] = find(t==interm_p(iip));
    %make it a vector
    end_p = unique(vec(t(:,jt)));
    % remove the master point
    end_p = end_p(end_p ~= master_p(imp));
    %remove all the intermediate points
    end_p = setdiff(end_p,interm_p);
    %see if some of the end points are on the slave side
    eos_p = intersect(end_p,slave_p);
    if(length(eos_p) > 0)
      printf('points on slave side')
    endif
  end
  
end