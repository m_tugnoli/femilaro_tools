function G = grad_h2d_nodalfield(grid,base,f)
% G = grad_h2d_nodalfield(grid,base,f)
%
% Compute the two components of the gradient of the field f.
% Presently, f must be a signe scalar field, although this function
% could be extended to a collection of fields provided as column
% vectors.
%
% The gradient is computed at the internal quadrature points and
% projected on the finite element space using proj_h2d_xi2nodes.

 % Define some working arrays
 i1 = base.bounds(1);
 i2 = base.bounds(2);
 bid( i1:i2 ) = [1:i2-i1+1];

 for ie=1:grid.ne

   ii = bid(grid.e(ie).poly);

   e   = grid.e(ie);
   bme = base.e{ii};
   me  = grid.e(ie).me;
   
   for i=1:2 % loop on the two spatial dimensions
     lg(i,:) = f(e.iv)' * permute( bme.gradp(i,:,:) , [2,3,1] );
   end
   grad(ie).lg = me.scale_gradp( me , bme.xig , ...
                                 permute( lg , [1,3,2] ) );

 end
 
 % Project on the finite element space
 G = proj_h2d_xi2nodes(grid,base,grad,"lg").lg;

return

