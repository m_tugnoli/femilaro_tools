function data = PV_dgcomp_hdf5_2(fname,itime,k,l,outfile,outdir,varargin)
% Next version of the postprocessing utility for hdf5 output results
% In this version there is enhanced compatibility with adaptive 
% simulations.
% data = PV_dgcomp_series(fname,itime,k,l,outfile,outdir)
% data = PV_dgcomp_series(fname,itime,k,l,outfile,outdir,select)
%
% Call PV_dgcomp for a series of files and prepare a .pvd file to
% collect the generated vtk files.
%
% fname nust contain a '$' to indicate the suffix 'grid.octxt', 
% 'base.octxt' or 'res-XXXX.h5'.
%
% EXAMPLE: './Output/basename-$'
%
% itime: label(s) of the selected output times
% EXAMPLE: for 'ch_icT-res-0003.h5', itime=3
% 
% k: FE order
% l: order of interpolation (can be taken less or equal to k), usually
%    l = 1
% outfile: name of the file without extension (string)
% outdir: folder in which to store the results

% The optional argument 'select' can be used as follows:
% C|P : conservative or primitive variables
% D|T : deviations or total values
% A   : add also the adaptive related output (degree, indicator) (not
%       used at the moment, adaptive output is set automatically if 
%       those results are present
%
% Moreover, the collected and/or postprocessed data are collected in
% the output argument data in a format which can be passed to
% dgcomp_interpolation to interpolate at arbitrary points.

 part_format = '%.4i'

 % input arguments
 if(nargin>6)
   select = varargin{1};
 else
   select = '';
 end
 primitive = not(isempty(strfind(select,'P')));
 total     = not(isempty(strfind(select,'T')));
 adaptive  = not(isempty(strfind(select,'A')));

 % Summary file
 fnameNX = [outdir,'/',outfile,'.pvd'];
 fnameVisit = [outdir,'/',outfile,'.visit'];
 fid = fopen(fnameNX,'w');
 fid_visit = fopen(fnameVisit,'w');
 
 line = '<?xml version="1.0"?>';
 fprintf(fid,'%s\n',line);
 line = '<VTKFile type="Collection" version="0.1" byte_order="BigEndian">';
 fprintf(fid,'%s\n',line);
 line = ' <Collection>';
 fprintf(fid,'%s\n',line);
 line = '!NBLOCKS 1'; %should be only one block
 fprintf(fid_visit,'%s\n',line);

 i = 1; %partition index: only one
 for j=1:length(itime)
     fnameN = fname;
     Xpos = find(fnameN=='$');
     fnameNX = [fnameN(1:Xpos-1),'grid',fnameN(Xpos+1:end),'.octxt'];
     grid = load(fnameNX);
		 fprintf('...loaded grid\n');
     fflush(stdout);
     fnameNX = [fnameN(1:Xpos-1),'base',fnameN(Xpos+1:end),'.octxt'];
     bases = load(fnameNX);
	 if (length(bases.base) == 1)
	   base = bases.base(1);
	 else		 
	   base = bases.base(k+1);
	 end
	 fprintf('...loaded base\n');
     fflush(stdout);
     fnameNX = [fnameN(1:Xpos-1),'res-',num2str(itime(j),'%.4i'), ...
                fnameN(Xpos+1:end),'.h5'];
     res = load(fnameNX);
	 fprintf('...loaded results\n');
     fflush(stdout);

     if(primitive)
       varnames = {'pressure','temperature','velocity'};
       for it=1:size(res.uuu,1)-2-(grid.grid.d)
         varnames{end+1} = ['tracer ',num2str(it)];
       end
       %for it=1:size(res.add_diags,1)
       %  varnames{end+1} = ['diag ',num2str(it)];
       %end
     else
       varnames = {'density','energy','momentum'};
       for it=1:size(res.uuu,1)-2-(grid.grid.d)
         varnames{end+1} = ['tracer ',num2str(it)];
       end
       %for it=1:size(res.add_diags,1)
       %  varnames{end+1} = ['diag ',num2str(it)];
       %end
     end

     if(or(total,primitive)) % need some postprocessing
       fnameNX = [fnameN(1:Xpos-1),'init',fnameN(Xpos+1:end),'.octxt'];
       ref = load(fnameNX);
	fprintf('...loaded reference\n');


     fflush(stdout);
     end

     if(total==1)
       if(primitive==1)
	     fprintf('calling postprocessing, total\n');
         fflush(stdout);
         [tmp,tmp,uuu,pitheta] = dgcomp_postp(res.uuu , base , ...
                                ref.uuu_ioref , ref.atm_ref , ref.phc);
	 %diags = [pitheta ; res.add_diags];
       else
         uuu = res.uuu + ref.uuu_ioref;
         uuu(1,:,:) = uuu(1,:,:) + permute(ref.atm_ref.rho,[3 1 2]);
         uuu(2,:,:) = uuu(2,:,:) + permute(ref.atm_ref.e,[3 1 2]);
	 %diags = res.add_diags;
       end
     else
       if(primitive==1)
	     fprintf('calling postprocessing, deviations\n');
         fflush(stdout);
         [tmp,uuu,tmp,pitheta] = dgcomp_postp(res.uuu , base , ...
                                ref.uuu_ioref , ref.atm_ref , ref.phc);
		 if(any(any(any(isnan(uuu)))))
         fprintf('bloody nan incoming\n');
         fflush(stdout);
	     end
	 % This is a trick to compute pi and theta for the reference
	 % state, so that we can plot the deviations.
         [tmp,tmp,tmp,io_pith] = dgcomp_postp(0*res.uuu,base , ...
                                ref.uuu_ioref , ref.atm_ref , ref.phc);
	 %diags = [pitheta-io_pith ; res.add_diags];
       else
         uuu = res.uuu;
	 %diags = res.add_diags;
       end
     end
	 
	 
                  diags = zeros(0,size(uuu,2),size(uuu,3)); % change this!!!

     % Add the Courant numbers
     %diags(end+1,1,:) = 1/base.p(1,1)*permute(res.c_tot,[3 1 2]);
     %diags(end+1,1,:) = 1/base.p(1,1)*permute(res.c_adv(1,:),[3 1 2]);
     %diags(end+1,1,:) = 1/base.p(1,1)*permute(res.c_adv(2,:),[3 1 2]);
     % Following modes are left to 0
     %varnames{end+1} = 'Courant (tot)';
     %varnames{end+1} = 'Courant (adv 1)';
     %varnames{end+1} = 'Courant (adv 2)';

	 % Add the various adaptive related values, if they are present
	 if(any(strcmp(fieldnames(res),'edeg')))
       diags(end+1,1,:) = 1/base.p(1,1)*permute(cast(res.edeg,'double'),[3 1 2]);
	   varnames{end+1} = 'base_degree';
	 end
	 %if(any(strcmp(fieldnames(res),'enbas')))
         %diags(end+1,1,:) = 1/base.p(1,1)*permute(cast(res.enbas,'double'),[3 1 2]);
         %   varnames{end+1} = 'number_basis';
	 %end
	 if(any(strcmp(fieldnames(res),'adaptind_val')))
       diags(end+1,1,:) = 1/base.p(1,1)*permute(res.adaptind_val,[3 1 2]);
	   varnames{end+1} = 'indicator_value';
	 end



     fnameNX = [outfile,'-',num2str(itime(j)), ...
                        '-',num2str(i,part_format),'.vtu'];
	 fnameNXpath = [outdir,'/',outfile,'-',num2str(itime(j)), ...
                        '-',num2str(i,part_format),'.vtu'];
     PV_dgcomp(grid.grid,base,uuu,k,l,fnameNXpath,res.test_name, ...
               diags,varnames);

     line = ['  <DataSet timestep="', num2str(res.time), ...
             '" group="" part="', num2str(i,'%i'), ...
             '" file="',fnameNX,'"/>'];
     fprintf(fid,'%s\n',line);
	 line = [fnameNX];
     fprintf(fid_visit,'%s\n',line);

     if(nargout>0) % collect data
       data(j).grid{i+1}     = grid.grid;
       data(j).ddc_grid{i+1} = grid.ddc_grid;
       data(j).base{i+1}     = base;
       data(j).uuu{i+1}      = [uuu;diags];
     end
 end

 line = ' </Collection>';
 fprintf(fid,'%s\n',line);
 line = '</VTKFile>';
 fprintf(fid,'%s\n',line);

 fclose(fid);
 fclose(fid_visit);

return

