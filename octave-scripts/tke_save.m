function [kin]=tke_save(fname,t0,tf,step,varargin)
% Return the turbulent kinetic energy (TKE) for resolved scale, using stat files.
% fname must be a string (e.g. 'out' for file out-P000-stat-0100.octxt)
% t0 is the first stat file
% tf is the last stat file
% step is distance between each stat files
%
% added an additional argument to mark the use of hdf5 output files, it's 
% not used, just add 'hdf5' for clarity

kin=[];
T=[];

for i=t0:step:tf
    if (nargin > 4) %hdf5 files
        file_res=[fname,'-res-',num2str(i,'%.4i'),'.h5'];
    else
        file_res=[fname,'-P0000-res-',num2str(i,'%.4i'),'.octxt'];
    end
    load(file_res);
    kin=[kin; kinetic_energy];
	T=[T; time];
end



 plot(T, kin,'-*')
 xlabel('time')
 ylabel('TEK')

 fid = fopen('./Postprocessing/tke.dat','w');
 fprintf(fid, '# T TKE\n');
 for i=1:length(T);
    fprintf(fid,'%g %g\n',T(i), kin(i));
 end
 fclose(fid);
return

