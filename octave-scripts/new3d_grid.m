function [p,e,t,edata,edata_legend] = new3d_grid(N,limits)
% [p,e,t] = new3d_grid(N,limits)
%
% Build a 3D structured grid using N(1) subdivisions in x, N(2)
% subdivisions in y and N(3) subdivisions in z. The domain is the cube
% [limits(1),limits(2)]x[limits(3),limits(4)]x[limits(5),limits(6)].
% If all the N(:) are even, it is guaranteed that each element has at
% least one internal node.

 % Number of vertices
 nv = (N(1)+1)*(N(2)+1)*(N(3)+1);
 p = zeros(3,nv);
 % Numbering of vertices: forall x, fix y and cycle over z
 %     | 0 0 .. 0  0    .. 0   .. 1 .. 1  1    .. |
 % p = | 0 0 .. 0  1    .. 1   .. 0 .. 0  1    .. |
 %     | 0 1 .. N3 N3+1 .. 2N3 .. 0 .. N3 N3+1 .. |
 %
 for i=1:N(1)+1
   for j=1:N(2)+1
     for k=1:N(3)+1
       ijk = (i-1)*(N(2)+1)*(N(3)+1) + (j-1)*(N(3)+1) + k;
       p(:,ijk) = [i,j,k]' - 1; % numbering begins from 0
     end
   end
 end
 % Physical/Geometrical points coordinates. Uniform subdivision in the 3 directions.
 for i=1:3
   j = (i-1)*2 + 1;
   k = j+1;
   p(i,:) = ((limits(k)-limits(j))/N(i))*p(i,:) + limits(j);
 end



plot3(p(1,:),p(2,:),p(3,:),'*')
 
 % Number of elements: 6 tetrahedra for each cube (N1*N2*N3 cubes)
 ne = 6*N(1)*N(2)*N(3);
 t = zeros(4,ne);
 e = []; 

 % Set distance
 edata=zeros(4,ne);
 edata(1,:)=1;
 edata(2,:)=0;
 edata(4,:)=0;
 edata_legend=[];
 edata_legend=[blanks(6);blanks(6);blanks(6);blanks(6)];
 edata_legend=["w_dist0";"w_distx";"w_disty";"w_distz"];
 for i=1:N(1)
   if(2*floor(i/2)==i) % index i is even
     oj = 'south';
   else
     oj = 'north';
   end
   for j=1:N(2)
 
     if j<N(2)/2+1
       location=1;
     else
       location=-1;
     end
   
     if(2*floor(i/2)==i) % index j is even
       ok = 'down';
     else
       ok = 'up';
     end

     for k=1:N(3)
       % index number of the element
       ijk = (i-1)*N(2)*N(3)*6 + (j-1)*N(3)*6 + (k-1)*6;
       % vertices of the considered cube
       iv(1) = (i-1)*(N(2)+1)*(N(3)+1) + (j-1)*(N(3)+1) + k;
       iv(2) = (i-1)*(N(2)+1)*(N(3)+1) + (j-1)*(N(3)+1) + k+1;
       iv(3) = (i-1)*(N(2)+1)*(N(3)+1) + (j-1+1)*(N(3)+1) + k;
       iv(4) = (i-1)*(N(2)+1)*(N(3)+1) + (j-1+1)*(N(3)+1) + k+1;
       iv(5) = (i-1+1)*(N(2)+1)*(N(3)+1) + (j-1)*(N(3)+1) + k;
       iv(6) = (i-1+1)*(N(2)+1)*(N(3)+1) + (j-1)*(N(3)+1) + k+1;
       iv(7) = (i-1+1)*(N(2)+1)*(N(3)+1) + (j-1+1)*(N(3)+1) + k;
       iv(8) = (i-1+1)*(N(2)+1)*(N(3)+1) + (j-1+1)*(N(3)+1) + k+1;
       edata(3,ijk+1:ijk +6)=location; %set distance to the wall: y>0 => d=1-y, y<0 => d=1+y
       % function for meshing a single cube
       [tt,ex,ey,ez] = new3d_grid_singlecube(iv,[ok,'_',oj]);
       % Storing the new tetrahedra in matrix t
       t(:,ijk+1:ijk+6) = tt;
       % Storing the boundary edges
       if(i==1)
         e = [e,ex(:,:,1)];
       endif
       if(i==N(1))
         e = [e,ex(:,:,2)];
       endif
       if(j==1)
         e = [e,ey(:,:,1)];
       endif
       if(j==N(2))
         e = [e,ey(:,:,2)];
       endif
       if(k==1)
         e = [e,ez(:,:,1)];
       endif
       if(k==N(3)) 
         e = [e,ez(:,:,2)];
       endif
       if(strcmp(ok,'up')==1)
         ok = 'down';
       else
         ok = 'up';
       endif
     end
     if(strcmp(oj,'north')==1)
       oj = 'south';
     else
       oj = 'north';
     endif
    end
 end

return

