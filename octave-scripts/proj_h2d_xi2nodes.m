function fnodal = proj_h2d_xi2nodes(grid,base,fxi,key,varargin)
% fnodal = plot_h2d_xifield(grid,base,fxi,key)
% fnodal = plot_h2d_xifield(grid,base,fxi,key,expr)
%
% Given the values at the quadrature points, compute the corresponding
% nodal values for the finite element representation.
%
% The field is first projected locally on the finite element space and
% then the values are averaged at each node to ensure C^0 continuity.
%
% fxi:  input fields, prescribed at the quad. nodes. This must be an
%       array of structires, where each structure contains one field
%       and each element of the array corresponds to one element in
%       the grid. This format allows having a different number of
%       quadrature point per each element.
% key:  comma separated list of the fields in fxi which must be
%       interpolated; spaces are ignored
% expr: if present, it must be a function which will be called using
%       the fields specified in key as arguments; in this case, only
%       the expression result will be interpolated at the FE nodes.
%
% For instance, suppose having
% 
% mydata =
%   1xNe struct array containing the fields:
%     f1
%     f2
%     f3
%     f4
%
% where Ne is the number of elements, and suppose that one is
% interested in the interpolation of fields f1 and f4. Then one would
% use
%
%   fnodal = proj_h2d_xi2nodes(grid,base,fxi, "f1, f4" )
%
% and the result fnodal will be structure with fields f1 and f4.
%
% The fields to be interpolated can be scalars, vectors or tensors;
% the only constraint is that the outermost dimension must be used for
% the quadrature nodes.


 % Parse the keys
 ii = find( key == ',' );
 if(isempty(ii)) % there is only one key
   K{1} = deblank(strtrim(key));
 else
   i1 = 1;
   for i=1:length(ii)
     K{i} = deblank(strtrim(key(i1:ii(i)-1)));
     i1 = ii(i)+1;
   end
   % check whether there is one more key
   lastkey = deblank(strtrim(key(ii(end)+1:end)));
   if(not(isempty(lastkey)))
     K{end+1} = lastkey;
   endif
 end

 % Check how many (scalar) fields are required
 for i=1:length(K)
   ii = size( fxi(1).(K{i}) ); % number of dimensions
   nfields_K(i) = prod(ii(1:end-1));
 end

 % Define some working arrays
 i1 = base.bounds(1);
 i2 = base.bounds(2);
 bid( i1:i2 ) = [1:i2-i1+1];

 for i=i1:i2 % allocate space for the local matrices
   ii = bid(i);
   lm{ii}   = zeros( base.e{ii}.pk  , base.e{ii}.pk );
   lf{ii}   = zeros( sum(nfields_K) , base.e{ii}.pk );
   fxik{ii} = zeros( sum(nfields_K) , base.e{ii}.m  );
 end

 fnod_v = zeros(grid.nv,sum(nfields_K));
 nnodal = zeros(grid.nv,      1       );

 for ie=1:grid.ne

   ii = bid(grid.e(ie).poly);

   e = grid.e(ie);
   me = grid.e(ie).me;
   bme = base.e{ii};

   % get the local value, packing them with two indexes: each scalar
   % field is stored as a row
   i1 = 1;
   for i=1:length(K)
     i2 = i1 + nfields_K(i)-1;
     fxik{ii}(i1:i2,:) = reshape( fxi(ie).(K{i}) , ...
                                [nfields_K(i),bme.m] );
     i1 = i2+1;
   end

   % Now the interpolation
   
   wga = me.scale_wg( me , bme.xig , bme.wg );

   lm{ii}(:,:) = 0;
   lf{ii}(:,:) = 0;
   for l=1:bme.m
     for i=1:bme.pk
       for j=1:bme.pk
         % local mass matrix -> L2 projection
         lm{ii}(i,j) = lm{ii}(i,j) + wga(l) * bme.p(i,l)*bme.p(j,l);
       end
       lf{ii}(:,i) = lf{ii}(:,i) + wga(l) * fxik{ii}(:,l)*bme.p(i,l);
     end
   end
   % Lagrangian basis -> dofs are the nodal values
   fe = lm{ii}\lf{ii}';

   fnod_v( e.iv , : ) = fnod_v( e.iv , : ) + fe;
   nnodal( e.iv )     = nnodal( e.iv ) + 1; % counter

 end

 % Consider the counter
 for i=1:size(fnod_v,2)
   fnod_v(:,i) = fnod_v(:,i)./nnodal;
 end

 % Reformat the output
 i1 = 1;
 for i=1:length(K)
   i2 = i1 + nfields_K(i)-1;
   fnodal.(K{i}) = reshape( fnod_v(:,i1:i2) , ...
                       [grid.nv,size(fxi(1).(K{i}))(1:end-1)] );
   i1 = i2+1;
 end

return

