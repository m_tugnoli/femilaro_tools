function [points,rho,energy,u,v,w]=PV_structure(fname,t0,tf,step,N, k,l)

% Return structure containing coordinate points, density,energy and velocity 
% components.
% The structures are organized as follow: 1rt field res<number of file res>,
% 2nd field P<number of partitons
% fname must be a string (e.g. 'out' for file out-P000-stat-0100.octxt)
% t0 is the first stat file
% tf is the last stat file
% step is distance between each stat files
% N: number of partitions minus 1 (the counter starts from 0)
% k: FE order
% l: order of interpolation (ca be taken less or equal to k) ??

warning('off','Octave:load-file-in-path'); % Disable useless warning 
                                               
points=[];rho=[]; energy=[]; u=[];v=[]; w=[];


for i=t0:step:tf
 for j=0:N
   
   % Load grid, base and res structures

   load([fname,'-P', num2str(j,'%.3i'),'-grid.octxt']);
   load([fname,'-P', num2str(j,'%.3i'),'-base.octxt']);
   res=load([fname,'-P', num2str(j,'%.3i'),'-res-',num2str(i,'%.4i'),'.octxt']);

   uuu = res.uuu;

   % Interpolate to degree k

   initialize_Uk = 1;
   for h=1:size(uuu,1)
     [Xi,Ti,Ui,cell_type] = ...
     el_resample(grid,base,permute(uuu(h,:,:),[2 3 1]),k,l);
     if(initialize_Uk)
       Uk = zeros(size(uuu,1),size(Ui,1),size(Ui,2));
       initialize_Uk = 0;
     end
     Uk(h,:,:) = permute(Ui,[3 1 2]);
   end

   % Coordinate point

   npoints = size(Xi,2)*size(Xi,3); % total number of points   
   n_v4e   = size(Xi,2);    % number of vertexes per element
   n_v4c   = size(Ti,1);    % number of vertexes per cell
   n_c4e   = size(Ti,2);    % number of cells per element
   n_cells = n_c4e*grid.ne; % total number of cells

   if(grid.d==2) % add z to the data
     Xi(3,:,:) = 0;
   endif
  
   points.(['res',num2str(i,'%.4i')]).(['P',num2str(j,'%.3i')])=Xi;

   % Density
  
   rho.(['res',num2str(i,'%.4i')]).(['P',num2str(j,'%.3i')])=Uk(1,:,:);

   % Energy
  
   energy.(['res',num2str(i,'%.4i')]).(['P',num2str(j,'%.3i')])=Uk(2,:,:);
     
   % Momentum
   
   if(grid.d==2) % add Uz to the data
     for z=size(Uk,1):-1:5 % move tracer data
       Uk(z+1,:,:) = Uk(z,:,:);
     end
     Uk(5,:,:) = 0;
   endif

   m=Uk(3:5,:,:);
  
    
   % Velocity

   u.(['res',num2str(i,'%.4i')]).(['P',num2str(j,'%.3i')])=m(1,:,:)./Uk(1,:,:);
   v.(['res',num2str(i,'%.4i')]).(['P',num2str(j,'%.3i')])=m(2,:,:)./Uk(1,:,:); 
   w.(['res',num2str(i,'%.4i')]).(['P',num2str(j,'%.3i')])=m(3,:,:)./Uk(1,:,:); 

 end
end

return

