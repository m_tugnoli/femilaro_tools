function my_pdesurf(h,p,t,U)
% my_pdesurf(h,p,t,U)
%
% Analogous to the matlab/octave pdesurf, but also handles the one
% dimensional case. Notice: require the octave package flp.

  figure(h);

  switch size(p,1)
   case 1
    [ps,is] = sort(p);
    plot(ps,U(is));
   case 2
    pdesurf(p,t,U);
  otherwise
   error (["Dimension ",size(p,1)," not supported."]);
 endswitch

return

