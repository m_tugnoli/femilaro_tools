function [kin]=int_kin(fname,t0,tf,step)
% Return the turbulent kinetic energy (TKE) for resolved scale, using stat files.
% fname must be a string (e.g. 'out' for file out-P000-stat-0100.octxt)
% t0 is the first stat file
% tf is the last stat file
% step is distance between each stat files

kin=[];

for i=t0:step:tf
    file_stat=[fname,'-P000-stat-',num2str(i,'%.4i'),'.octxt'];
    load(file_stat);
    kin=[kin;trapz(stat.td.kin(:,stat.td.turb_res))];
end


 plot([t0:step:tf],kin,'-*')
 xlabel('file stat')
 ylabel('TEK')


return

