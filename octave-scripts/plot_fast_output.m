function plot_fast_output(grid,base,infile,x)
% plot_fast_output(grid,base,infile,x)
%
% Plots of the unknowns of the problem.
%
% grid: the computational grid
% base: finite element basis
% infile: input file con i selected results
% x: the point where the interpolation is required (column array)
%

% Reading the selected results
data = collect_fast_output(infile);
% Interpolation
idat = interp_fast_output(grid,base,data,x);

% Plot

% 1. Density
figure(1), hold on
plot(data.times,idat(1,:))
legend('density')
% 2. Total energy
figure(2), hold on
plot(data.times,idat(2,:))
legend('total energy')
% 3. Momentum
figure(3), hold on
plot(data.times,idat(3:5,:))
legend('momentum')

% 4. Tracers
% analytic solution
Lx = 5.0; Ly = 0.5; Lz = 1.5;
k = 1; l = 1; m = 1;
nu = 0.25;
Kx = pi*k/Lx; Ky = pi/2*l/Ly; Kz = pi/2*m/Lz;
K = Kx^2 + Ky^2 + Kz^2;
tau = 1/(nu*K);
c_ex = exp(-data.times./tau)*sin(Kx*x(1))*cos(Ky*x(2))*cos(Kz*x(3));
% numerical solution
if size(idat,1) > grid.d+2
 ntrcs = size(idat,1) - (grid.d+2);
 for j=1:ntrcs
 figure(j+3), hold on
 plot(data.times,idat(grid.d+2+j,:))
 plot(data.times,c_ex,'r')
 title('Tracer')
 legend('numerical','analytic')
 end
end
