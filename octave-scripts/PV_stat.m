function PV_stat(s_grid,stat,outfile)
% function PV_stat(s_grid,stat,outfile)
%
% Note: paraview only understands 3D data, and the usual way to work
% with 1D/2D data is by setting the other coordinates to suitable values.

 % spatial dimension
 d = size(s_grid.p,1);

 % Write the output file
 fid = fopen(outfile,'w');

 % Header 
 line = '<?xml version="1.0"?>';
 fprintf(fid,'%s\n',line);

 %---------------------------------
 % Write RECTILINEAR GRID
 line = '<VTKFile type="RectilinearGrid" version="0.1" byte_order="BigEndian">';
 fprintf(fid,'%s\n',line);
 extent_x0 = 0;
 extent_x1 = 0;
 extent_y0 = 0;
 extent_y1 = s_grid.nplanes - 1;
 extent_z0 = 0;
 extent_z1 = 0;
 extent = [extent_x0, extent_x1, extent_y0, extent_y1, extent_z0, extent_z1];
 line = [' <RectilinearGrid WholeExtent="',num2str(extent),'">'];
 fprintf(fid,'%s\n',line);
 line = ['  <Piece Extent="',num2str(extent),'">'];
 fprintf(fid,'%s\n',line);

 %---------------------------------
 % Write POINT DATA
% line = '   <PointData Scalars="scalars">';
 line = '   <PointData Scalars="density" Vectors="velocity">';
 fprintf(fid,'%s\n',line);

 % Standard variables -------------
 % density
 line = '    <DataArray type="Float32" Name="density" Format="ascii">';
 fprintf(fid,'%s\n',line);
 for i=1:s_grid.nplanes
   fprintf(fid,'%e\n',stat.uuu(1,1,i));
 end
 line = '    </DataArray>';
 fprintf(fid,'%s\n',line);
 % energy
 line = '    <DataArray type="Float32" Name="energy" Format="ascii">';
 fprintf(fid,'%s\n',line);
 for i=1:s_grid.nplanes
   fprintf(fid,'%e\n',stat.uuu(1,2,i));
 end
 line = '    </DataArray>';
 fprintf(fid,'%s\n',line);
 % momentum
 line = '    <DataArray type="Float32" Name="momentum" NumberOfComponents="3" Format="ascii">';
 fprintf(fid,'%s\n',line);
 for i=1:s_grid.nplanes
   fprintf(fid,'%e ',stat.uuu(1,3:5,i));
   fprintf(fid,'\n');
 end
 line = '    </DataArray>';
 fprintf(fid,'%s\n',line);
 % velocity
 line = '    <DataArray type="Float32" Name="velocity" NumberOfComponents="3" Format="ascii">';
 fprintf(fid,'%s\n',line);
 for i=1:s_grid.nplanes
   fprintf(fid,'%e ',stat.uu(1,:,i));
   fprintf(fid,'\n');
 end
 line = '    </DataArray>';
 fprintf(fid,'%s\n',line);
 % temperature
 line = '    <DataArray type="Float32" Name="temperature" Format="ascii">';
 fprintf(fid,'%s\n',line);
 for i=1:s_grid.nplanes
   fprintf(fid,'%e\n',stat.T(1,i));
 end
 line = '    </DataArray>';
 fprintf(fid,'%s\n',line);
 % pressure
 line = '    <DataArray type="Float32" Name="pressure" Format="ascii">';
 fprintf(fid,'%s\n',line);
 for i=1:s_grid.nplanes
   fprintf(fid,'%e\n',stat.p(1,i));
 end
 line = '    </DataArray>';
 fprintf(fid,'%s\n',line);
 % Reynolds shear stress
 line = '    <DataArray type="Float32" Name="rhouv" Format="ascii">';
 fprintf(fid,'%s\n',line);
 for i=1:s_grid.nplanes
   fprintf(fid,'%e\n',stat.uv(i));
 end
 line = '    </DataArray>';
 fprintf(fid,'%s\n',line);
 % Turbulent heat flux
 line = '    <DataArray type="Float32" Name="rhovT" Format="ascii">';
 fprintf(fid,'%s\n',line);
 for i=1:s_grid.nplanes
   fprintf(fid,'%e\n',stat.vT(i));
 end
 line = '    </DataArray>';
 fprintf(fid,'%s\n',line);

 % Turbulence diagnostics ------------
 % diss
 line = '    <DataArray type="Float32" Name="diss" Format="ascii">';
 fprintf(fid,'%s\n',line);
 for i=1:s_grid.nplanes
   fprintf(fid,'%e\n',stat.td.diss(i));
 end
 line = '    </DataArray>';
 fprintf(fid,'%s\n',line);
 % diss_v
 line = '    <DataArray type="Float32" Name="diss_v" Format="ascii">';
 fprintf(fid,'%s\n',line);
 for i=1:s_grid.nplanes
   fprintf(fid,'%e\n',stat.td.diss_v(i));
 end
 line = '    </DataArray>';
 fprintf(fid,'%s\n',line);
 % diss_t
 line = '    <DataArray type="Float32" Name="diss_t" Format="ascii">';
 fprintf(fid,'%s\n',line);
 for i=1:s_grid.nplanes
   fprintf(fid,'%e\n',stat.td.diss_t(i));
 end
 line = '    </DataArray>';
 fprintf(fid,'%s\n',line);
 % stress tensor
 line = '    <DataArray type="Float32" Name="tau" NumberOfComponents="9" Format="ascii">';
 fprintf(fid,'%s\n',line);
 for i=1:s_grid.nplanes
   for id=1:d
     fprintf(fid,'%e ',stat.td.tau(id,:,i));
     fprintf(fid,'\n');
   end
   fprintf(fid,'\n');
 end
 line = '    </DataArray>';
 fprintf(fid,'%s\n',line);
 % heat fluxes
 line = '    <DataArray type="Float32" Name="q" NumberOfComponents="3" Format="ascii">';
 fprintf(fid,'%s\n',line);
 for i=1:s_grid.nplanes
  fprintf(fid,'%e ',stat.td.q(:,i));
  fprintf(fid,'\n');
 end
 line = '    </DataArray>';
 fprintf(fid,'%s\n',line);

 % RMS of standard variables
 % rms_density
 line = '    <DataArray type="Float32" Name="rms_density" Format="ascii">';
 fprintf(fid,'%s\n',line);
 for i=1:s_grid.nplanes
   fprintf(fid,'%e\n',stat.uuu(2,1,i));
 end
 line = '    </DataArray>';
 fprintf(fid,'%s\n',line);
 % rms_energy
 line = '    <DataArray type="Float32" Name="rms_energy" Format="ascii">';
 fprintf(fid,'%s\n',line);
 for i=1:s_grid.nplanes
   fprintf(fid,'%e\n',stat.uuu(2,2,i));
 end
 line = '    </DataArray>';
 fprintf(fid,'%s\n',line);
 % rms_velocity
 line = '    <DataArray type="Float32" Name="rms_velocity" NumberOfComponents="3" Format="ascii">';
 fprintf(fid,'%s\n',line);
 for i=1:s_grid.nplanes
   fprintf(fid,'%e ',stat.uu(2,:,i));
   fprintf(fid,'\n');
 end
 line = '    </DataArray>';
 fprintf(fid,'%s\n',line);
 % rms_temperature
 line = '    <DataArray type="Float32" Name="rms_temperature" Format="ascii">';
 fprintf(fid,'%s\n',line);
 for i=1:s_grid.nplanes
   fprintf(fid,'%e\n',stat.T(2,i));
 end
 line = '    </DataArray>';
 fprintf(fid,'%s\n',line);

 % extra array for plot the x-axis correctly
 % y
 line = '    <DataArray type="Float32" Name="y" Format="ascii">';
 fprintf(fid,'%s\n',line);
 for i=1:s_grid.nplanes
   fprintf(fid,'%e\n',s_grid.yp(i));
 end
 line = '    </DataArray>';
 fprintf(fid,'%s\n',line);

 % end write data
 line = '   </PointData>';
 fprintf(fid,'%s\n',line);

 %---------------------------------
 % Write CELL DATA
 line = '   <CellData>';
 fprintf(fid,'%s\n',line);
 % end write cell data
 line = '   </CellData>';
 fprintf(fid,'%s\n',line);

 %---------------------------------
 % Write GRID COORDINATES
 line = '   <Coordinates>';
 fprintf(fid,'%s\n',line);
 % x
 line = '    <DataArray type="Float32" Name="x" Format="ascii">';
 fprintf(fid,'%s\n',line);
 fprintf(fid,'%e\n',0);
 line = '    </DataArray>';
 fprintf(fid,'%s\n',line);
 % y
 line = '    <DataArray type="Float32" Name="y" Format="ascii">';
 fprintf(fid,'%s\n',line);
 for i=1:s_grid.nplanes
   fprintf(fid,'%e\n',s_grid.yp(i));
 end
 line = '    </DataArray>';
 fprintf(fid,'%s\n',line);
 % z
 line = '    <DataArray type="Float32" Name="z" Format="ascii">';
 fprintf(fid,'%s\n',line);
 fprintf(fid,'%e\n',0);
 line = '    </DataArray>';
 fprintf(fid,'%s\n',line);

 % end write grid coordinates
 line = '   </Coordinates>';
 fprintf(fid,'%s\n',line);

 %---------------------------------
 % Close the grid and the file
 line = '  </Piece>';
 fprintf(fid,'%s\n',line);
 line = ' </RectilinearGrid>';
 fprintf(fid,'%s\n',line);
 line = '</VTKFile>';
 fprintf(fid,'%s\n',line);

 fclose(fid);
return

