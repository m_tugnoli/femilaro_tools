function fastGraph(casename, instant, processors, varargin)
%
% MACHINE DEPENDENT, CHANNEL FLOW ONLY, DEPRECATED
% added an additional argument to mark the use of hdf5 output files, it's 
% not used, just add 'hdf5' for clarity


%% --------- DNS DATA LOAD ------------------
ref_path = '/home/matteo/Dropbox/Poli/Phd/MainProject/Datas/DNS/M0.7/';

reTauDns = 178.77;
%mean profile
dat = importdata([ref_path, 'Umean.dat'],' ',2);
dns.Umean.y = (dat.data(:,1)+1)*reTauDns;
dns.Umean.val = dat.data(:,2);

%Reynolds stresses
dat = importdata([ref_path, 'tauuu.dat'],' ',2);
dns.tauuu.y = (dat.data(:,1)+1)*reTauDns;
dns.tauuu.val = dat.data(:,2);

dat = importdata([ref_path, 'tauuv.dat'],' ',2);
dns.tauuv.y = (dat.data(:,1)+1)*reTauDns;
dns.tauuv.val = dat.data(:,2);

dat = importdata([ref_path, 'tauvv.dat'],' ',2);
dns.tauvv.y = (dat.data(:,1)+1)*reTauDns;
dns.tauvv.val = dat.data(:,2);

dat = importdata([ref_path, 'tauww.dat'],' ',2);
dns.tauww.y = (dat.data(:,1)+1)*reTauDns;
dns.tauww.val = dat.data(:,2);

%Rms
dat = importdata([ref_path, 'rmsu.dat'],' ',2);
dns.rmsu.y = (dat.data(:,1)+1)*reTauDns;
dns.rmsu.val = dat.data(:,2);

dat = importdata([ref_path, 'rmsv.dat'],' ',2);
dns.rmsv.y = (dat.data(:,1)+1)*reTauDns;
dns.rmsv.val = dat.data(:,2);

dat = importdata([ref_path, 'rmsw.dat'],' ',2);
dns.rmsw.y = (dat.data(:,1)+1)*reTauDns;
dns.rmsw.val = dat.data(:,2);


%tke
dat = importdata([ref_path, 'turbKinEn.dat'],' ',2);
dns.tke.y = (dat.data(:,1)+1)*reTauDns;
dns.tke.val = dat.data(:,2);

%% ---- SIMULATIONS DATA LOAD
ReBulkSim = 2800;
reTauDns = 178.77;
np = processors-1;
res = instant;
k_u = 1;
k_tau = k_u^2;
% Anisotropic

  if (nargin > 3)
   basename = casename;
  section_path = ['./Grids/',basename, '_grid-stats.octxt'];
  %cd(case_folder_name)
  [data]=extract_statistics(section_path,['./Output/',basename],res,np,'hdf5');
  else
  basename = casename;
  section_path = ['./Grids/',basename, '_grid-stats.octxt'];
  %cd(case_folder_name)

  [data]=extract_statistics(section_path,['./Output/',basename],res,np);
  %load([case_folder_name, 'Postprocessing/stat_data.mat'])
  end
  % Preliminary cycle to determine y and interpolation factor
  for i = 1:length(data)/2+1
    plotdata.y(i) = (1+data(i).y)*reTauDns;
  end

  for i=1:length(data)
    plotdata.yfull(i) = (1+data(i).y)*reTauDns;
    plotdata.u_media(i) =  data(i).values(3)*k_u;
    plotdata.v_media(i) =  data(i).values(4)*k_u;
    plotdata.w_media(i) =  data(i).values(5)*k_u;
    plotdata.rho_media(i) = data(i).values(1);
    plotdata.tau_uu(i) =  data(i).values(6)*k_tau; 
    plotdata.tau_uv(i) =  data(i).values(7)*k_tau;
    plotdata.tau_uw(i) =  data(i).values(8)*k_tau;
    plotdata.tau_vv(i) =  data(i).values(10)*k_tau;
    plotdata.tau_vw(i) =  data(i).values(11)*k_tau; 
    plotdata.tau_ww(i) =  data(i).values(14)*k_tau;
    plotdata.rms_u(i) =  sqrt(data(i).values(6))*k_u; 
    plotdata.rms_v(i) =  sqrt(data(i).values(10))*k_u;   
    plotdata.rms_w(i) =  sqrt(data(i).values(14))*k_u;      
    plotdata.tau_mod_uu(i) =  data(i).values(17)*k_tau; 
    plotdata.tau_mod_uv(i) =  data(i).values(18)*k_tau;
    plotdata.tau_mod_uw(i) =  data(i).values(19)*k_tau;
    plotdata.tau_mod_vv(i) =  data(i).values(21)*k_tau;
    plotdata.tau_mod_vw(i) =  data(i).values(22)*k_tau;
    plotdata.tau_mod_ww(i) =  data(i).values(25)*k_tau;
    plotdata.tke(i) = plotdata.tau_uu(i)+plotdata.tau_vv(i)+ plotdata.tau_ww(i);
  end
  
  for i=1:length(data)/2+1
    plotdata.tau_uu_mean(i)=(plotdata.tau_uu(i)+plotdata.tau_uu(end +1-i))/2;
    plotdata.tau_vv_mean(i)=(plotdata.tau_vv(i)+plotdata.tau_vv(end +1-i))/2;
    plotdata.tau_ww_mean(i)=(plotdata.tau_ww(i)+plotdata.tau_ww(end +1-i))/2;
    plotdata.tau_uv_mean(i)=(plotdata.tau_uv(i)-plotdata.tau_uv(end +1-i))/2;
    plotdata.tau_uw_mean(i)=(plotdata.tau_uw(i)-plotdata.tau_uw(end +1-i))/2;
    plotdata.tau_vw_mean(i)=(plotdata.tau_vw(i)-plotdata.tau_vw(end +1-i))/2;
	
	plotdata.tau_mod_uu_mean(i)=(plotdata.tau_mod_uu(i)+plotdata.tau_mod_uu(end +1-i))/2;
    plotdata.tau_mod_vv_mean(i)=(plotdata.tau_mod_vv(i)+plotdata.tau_mod_vv(end +1-i))/2;
    plotdata.tau_mod_ww_mean(i)=(plotdata.tau_mod_ww(i)+plotdata.tau_mod_ww(end +1-i))/2;
    plotdata.tau_mod_uv_mean(i)=(plotdata.tau_mod_uv(i)-plotdata.tau_mod_uv(end +1-i))/2;
    plotdata.tau_mod_uw_mean(i)=(plotdata.tau_mod_uw(i)-plotdata.tau_mod_uw(end +1-i))/2;
    plotdata.tau_mod_vw_mean(i)=(plotdata.tau_mod_vw(i)-plotdata.tau_mod_vw(end +1-i))/2;

    plotdata.rms_u_mean(i)=(plotdata.rms_u(i)+plotdata.rms_u(end +1-i))/2;
    plotdata.rms_v_mean(i)=(plotdata.rms_v(i)+plotdata.rms_v(end +1-i))/2;
    plotdata.rms_w_mean(i)=(plotdata.rms_w(i)+plotdata.rms_w(end +1-i))/2;

    plotdata.u_media_mean(i)=(plotdata.u_media(i)+plotdata.u_media(end +1-i))/2;
    plotdata.w_media_mean(i)=(plotdata.w_media(i)+plotdata.w_media(end +1-i))/2;
    plotdata.rho_media_mean(i)=(plotdata.rho_media(i)+plotdata.rho_media(end +1-i))/2;
    plotdata.tke_mean(i)=(plotdata.tke(i)+plotdata.tke(end+1-i))/2;
  end
  
  	%get normalizations
	
	plotdata.deltaRef = 1;
	plotdata.muWall = 1;
	plotdata.rhoWall1 = plotdata.rho_media_mean(1); %first point
	m_rho = (plotdata.rho_media_mean(2)-plotdata.rho_media_mean(1))/(plotdata.y(2)-plotdata.y(1))*reTauDns;
	plotdata.rhoWall2 = plotdata.rho_media_mean(1) - m_rho*plotdata.y(1)/reTauDns;
	plotdata.dUdy=plotdata.u_media_mean(1)/plotdata.y(1)*reTauDns;%adimensionale
	plotdata.tauWall = plotdata.dUdy*plotdata.muWall; %adimensionale
	plotdata.ReTau = sqrt(ReBulkSim)*sqrt(plotdata.rhoWall2*plotdata.dUdy); %dimensionale
	plotdata.uTau = plotdata.ReTau/(ReBulkSim*plotdata.rhoWall2);
	plotdata.u_outer_2_inner = 1/plotdata.uTau;
	plotdata.u2_outer_2_inner = plotdata.u_outer_2_inner^2;
	
	
	
	
	
	%======PLOT=============
% U mean	
figure()
hold on
plot(plotdata.yfull,plotdata.u_media, '-xg')
plot(dns.Umean.y,dns.Umean.val,'--k')
legend('sim','DNS')
xlabel('y^+')
ylabel('<U>')


%% --- tau_uu
figure()
hold on
plot(plotdata.yfull,plotdata.tau_uu, '-xg')
plot(dns.tauuu.y,dns.tauuu.val,'--k')
legend('sim','DNS')
xlabel('y^+')
ylabel('\tau_{xx}')

%% --- tau_vv
figure()
hold on
plot(plotdata.yfull,plotdata.tau_vv, '-xg')
plot(dns.tauvv.y,dns.tauvv.val,'--k')
legend('sim','DNS')
xlabel('y^+')
ylabel('\tau_{yy}')

%% --- tau_ww
figure()
hold on
plot(plotdata.yfull,plotdata.tau_ww, '-xg')
plot(dns.tauww.y,dns.tauww.val,'--k')
legend('sim','DNS')
xlabel('y^+')
ylabel('\tau_{xx}')

%% --- tau_uv
figure()
hold on
plot(plotdata.yfull,-plotdata.tau_uv*plotdata.u2_outer_2_inner, '-xg')
plot(dns.tauuv.y,dns.tauuv.val,'--k')
legend('sim','DNS')
xlabel('y^+')
ylabel('\tau_{xy}')
