function quadrature_plot(fname,d)
% function quadrature_plot(fname,d)
% 
% Spatial distribution of quadrature points
% for 2D and 3D quadrature formulas in 
% QUADRATURE branch.
% 
% fname: string for the file name containing the points
% (i.e. 'Q04.octxt')
% d: spatial dimension

 load(fname);
 close all
 set_plot_defaults
 
 if (d==3) 
  l1 = [0 1; 0 0; 0 0];
  l2 = [1 0; 0 1; 0 0];
  l3 = [0 0; 0 1; 0 0];
  l4 = [0 0; 0 0; 0 1];
  l5 = [0 1; 0 0; 1 0];
  l6 = [0 0; 0 1; 1 0];

  % altri lati
  figure
  hold on
  plot3(x(1,:),x(2,:),x(3,:),'bx')
  plot3(l1(1,:),l1(2,:),l1(3,:),'k')
  plot3(l2(1,:),l2(2,:),l2(3,:),'k')
  plot3(l3(1,:),l3(2,:),l3(3,:),'k')
  plot3(l4(1,:),l4(2,:),l4(3,:),'k')
  plot3(l5(1,:),l5(2,:),l5(3,:),'k')
  plot3(l6(1,:),l6(2,:),l6(3,:),'k')

  xlabel('x')
  ylabel('y')
  zlabel('z')
  axis([-0.1 1.1 -0.1 1.1 -0.1 1.1]) 
  

 elseif (d==2)

  l = [0 1 0 0; 0 0 1 0];
  figure
  hold on
  plot(x(1,:),x(2,:),'bx')
  plot(l(1,:),l(2,:),'k')
  xlabel('x')
  ylabel('y')
  axis([-0.1 1.1 -0.1 1.1]) 
  print -depsc2 quadr

 end

return
