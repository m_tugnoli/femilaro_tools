function confronto_DNS(fname,grid,caso,res,np)

clc
close all

%DNS

 k_u=1/15.679 
 k_tau=k_u^2;
 % reystress
load('/home/michele/DNS/Kim, Moi, Moser/chan180/profiles/chan180.reystress')
 y_DNS=chan180(:,2);
 tau_uu_DNS=chan180(:,3);
 tau_vv_DNS=chan180(:,4);
 tau_ww_DNS=chan180(:,5);
 tau_uv_DNS=chan180(:,6);
 tke_DNS=tau_uu_DNS+tau_vv_DNS+tau_ww_DNS;
 % k075s
load('/home/michele/DNS/Kim, Moi, Moser/chan180/profiles/chan180.means')
 u_media_DNS=chan180(:,3);
 w_media_DNS=chan180(:,5);
 fm_DNS=trapz(y_DNS,u_media_DNS);

k_u= fm_DNS/178.77;
k_tau=k_u^2;

[data]=extract_statistics(grid,fname,res,np);

for i = 1:length(data)
  y(i) = 180.*(1+data(i).y);
end

for i=1:length(data)
  u_media(i) =  data(i).values(3)*k_u;
  v_media(i) =  data(i).values(4)*k_u;
  w_media(i) =  data(i).values(5)*k_u;
  tau_uu(i) =  data(i).values(6)*k_tau; 
  tau_uv(i) =  data(i).values(7)*k_tau;
  tau_vv(i) =  data(i).values(10)*k_tau;   
  tau_ww(i) =  data(i).values(14)*k_tau;   
  tau_mod_uu(i) =  data(i).values(17)*k_tau; 
  tau_mod_uv(i) =  data(i).values(18)*k_tau;
  tau_mod_vv(i) =  data(i).values(21)*k_tau;   
  tau_mod_ww(i) =  data(i).values(25)*k_tau;
  tke(i) = tau_uu(i)+tau_vv(i)+ tau_ww(i)+tau_mod_uu(i)+tau_mod_vv(i)+tau_mod_ww(i);
end



%%Plot


figure()
plot(y,tau_uu + tau_mod_uu,'-ro','LineWidth',2,y_DNS,tau_uu_DNS,'--k')
legend(caso,'DNS')

figure()
plot(y,tau_vv + tau_mod_vv,'-ro','LineWidth',2,y_DNS,tau_vv_DNS,'--k')
legend(caso,'DNS')

figure()
plot(y,tau_ww + tau_mod_ww,'-ro','LineWidth',2,y_DNS,tau_ww_DNS,'--k')
legend(caso,'DNS')

figure()
plot(y,-tau_uv - tau_mod_uv,'-ro','LineWidth',2,y_DNS,-tau_uv_DNS,'--k')
legend(caso,'DNS')

figure()
plot(y,tke,'-ro','LineWidth',2,y_DNS,tke_DNS,'--k')
legend(caso,'DNS')

figure()
plot(y,u_media,'-ro','LineWidth',2,y_DNS,u_media_DNS,'--k')
legend(caso,'DNS')

figure()
plot(y,w_media,'-ro','LineWidth',2,y_DNS,w_media_DNS,'--k')
legend(caso,'DNS')






