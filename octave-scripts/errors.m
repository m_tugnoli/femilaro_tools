function [e_L2, e_A, e_H1] = errors(grid_file,base_file,output_file)
% function [e_L2, e_A, e_H1] = errors(grid_file,base_file,output_file)
%
% Calculation of the L2-norm, H1-norm and DG-norm error
% for a scalar variable (i.e. tracer).
%
% References to src/ADR/mod-error-norms.f90 line 277
%
% grid_file: the computational grid file (string)
% base_file: finite element basis file (string)
% output_file: results file (string)

%{ 
NOTE DI CALCOLO.

Metodo di Gauss, L2 error
\begin{equation*}
 \int_K (u-u_h)^2 = \sum_g w_g (u(x_g)-u_h(x_g))^2
\end{equation*}
con 
\begin{equation*}
 u_h(x_g) = \sum_j u_j \phi_j(x_g)
\end{equation*}

Metodo di Gauss. Seminorma H1 error
\begin{equation*}
 \int_K (u-u_h)^2 = \sum_g w_g (\nabla u(x_g)- \nabla u_h(x_g))^2
\end{equation*}
con 
\begin{equation*}
 \nabla u_h(x_g) = \sum_j u_j \nabla \phi_j(x_g)
\end{equation*}
dove il calcolo di $\nabla \phi_j$ richiede di ricorrere alla 
trasformazione di coordinate $\xi - x$ nel seguente modo:
\begin{equation*}
 \dfrac{\de \phi}{\de x_j} = \dfrac{\de \phi}{\de \xi_i} \dfrac{\de \xi_i}{\de x_j} = 
 \dfrac{\de \xi}{\de x}_{ij} \dfrac{\de \phi}{\de \xi_i} =  
\dfrac{\de \xi}{\de x}^T_{ji} \dfrac{\de \phi}{\de \xi_i}
\end{equation*}

Scalatura pesi di Gauss per gli integrali di bordo $\de K$: l'esigenza nasce da
\begin{equation*}
 \int_e f(\sigma) d\sigma = \int_{\hat{e}} f(\hat{\sigma}) |J(\hat{e} \rightarrow e)| \de \hat{\sigma}
\end{equation*}
determinante costante perchè i lati della griglia sono dritti, quindi
\begin{equation*}
 \int_e f(\sigma) d\sigma = |J(\hat{e} \rightarrow e)| \int_{\hat{e}} f(\hat{\sigma}) \de \hat{\sigma} = 
 \dfrac{|e|}{|\hat{e}|} \sum_s w_{gs} f(\xi_s)
\end{equation*}
%}

% Reading files
load(grid_file);
load(base_file);
output = load(output_file);

% Allocations
% 1. Gauss nodes: grid.m -> dimensions of the grid, base.m -> # of nodes in each element K
xg = zeros(grid.m,base.m);
% 2. Gauss weights
wg = zeros(base.m,1);
% 3. Exact solution
uuue = zeros(base.m,1);
% 4. Coefficients of the basis functions
uuuhk = zeros(base.pk,1);
% 5. Numerical solution at the nodes of each element from FEM coefficients and basis
uuuhe = zeros(base.m,1);

% Initialization of the errors
e_L2 = 0.0; e_H1 = 0.0; e_A = 0.0;

% Element loop --------------
for ie = 1:grid.ne

 % From reference element to the real grid one
 xg = affmap(grid.e(ie),base.xig);
 wg = grid.e(ie).det_b*base.wg;

 % Analytic solution and gradient at the GRID Gauss nodes
 [uuue, grad_uuue] = uuu_analytical_icT(xg);

 % Numerical solution (tracer) at the REFERENCE Gauss nodes
 uuuhk = output.uuu(6,:,ie);
 uuuhe = uuuhk*base.p;

 % Gradient of the numerical solution (tracer) at the REFERENCE Gauss nodes
 grad_uuuhe = zeros(grid.d,base.m);
 for i = 1:grid.d
  base_gradp = permute(base.gradp(i,:,:),[2 3 1]);
  grad_uuuhe(i,:) = uuuhk*base_gradp;
 end
 % Gradient at the GRID Gauss nodes (inverse affine transformation)
 grad_uuuhe = (grid.e(ie).bi') * grad_uuuhe; % transposition is necessary (see report)

 % L2 error
 L2 = sum(wg.*(uuue-uuuhe).^2);
 e_L2 = e_L2 + L2;
 % H1 error
 H1x = sum(wg.*(grad_uuue(1,:)-grad_uuuhe(1,:)).^2);
 H1y = sum(wg.*(grad_uuue(2,:)-grad_uuuhe(2,:)).^2);
 H1z = sum(wg.*(grad_uuue(3,:)-grad_uuuhe(3,:)).^2);
 H1 = L2 + H1x + H1y + H1z;
 e_H1 = e_H1 + H1;
 % semi-norm A (article priori analysis pag. 1685, first term) coincident with
 % semi-norm DG (article unified analysis pag. 1762, first term of the triple norm)
 e_A = e_A + H1x + H1y + H1z;

end
% --------------------------

% Internal side loop -------------- ref: mod_dgcomp_rhs.f90 linea 1136
for is = 1:grid.ni

 % indexes of the adiacent elements
 ie_s = grid.s(is).ie;
 % side local indexes on the two elements
 is_loc = grid.s(is).isl;

 % base function evaluations on the side quad. nodes
 for ie = 1:2 % loop on the two elements of the side
  % index of the element -> side permutation (perm. pi)
  % Permutation is necessary since each element must know how its faces are mapped
  % on the reference element; obviously, there are different ways of mapping: from
  % this, the necessity of index permutation
  i_perm = grid.e(ie_s(ie)).pi(is_loc(ie));
  % 1st index unchanged, 2nd index reordered with pi, 3rd index local side
  pb(:,:,ie) = base.pb(:,base.stab(i_perm,:),is_loc(ie));
 end
 % rescale the quadrature weights: surface/reference_volume * wgs (see report)
 wgsa = (grid.s(is).a/base.me.voldm1) * base.wgs;

 % evaluate the solution in the quadrature nodes
 for ie = 1:2
  uuu_s(:,:,ie) = output.uuu(:,:,ie_s(ie)) * pb(:,:,ie);
 end

 % jump for the tracer solution
 jump = uuu_s(6,:,1) - uuu_s(6,:,2);

 % seminorm A error
 A = sum(wgsa.*(jump).^2);
 e_A = e_A + A;

end
% --------------------------

% Boundary side loop --------------
for is = grid.ni+1:grid.ns

 % element of the side (just one because side is a face of a tetrahedra in 3D)
 ie_s(1) = grid.s(is).ie(1);

 % Consider only the physical boundary, not the ones due to domain decomposition
 % ddc_grid.ddc_marker is the index for boundary sides due to domain decomposition
 if grid.s(is).ie(2) ~= -ddc_grid.ddc_marker 

   % side local indexes on the element
   is_loc(1) = grid.s(is).isl(1);

   % base function evaluations on the side quad. nodes
   % index of the element -> side permutation (perm. pi)
   i_perm = grid.e(ie_s(1)).pi(is_loc(1));
   % 1st index unchanged, 2nd index reordered with pi, 3rd index local side
   pb(:,:,1) = base.pb(:,base.stab(i_perm,:),is_loc(1));
   % rescale the quadrature weights
   wgsa = (grid.s(is).a/base.me.voldm1) * base.wgs;

   % evaluate the solution in the quadrature nodes: the values
   % in the ghost element are defined according to the boundary condition
   uuu_s(:,:,1) = output.uuu(:,:,ie_s(1)) * pb(:,:,1);
   uuu_s(:,:,2) = 0; % dirichlet condition u_bc = 0.0

   % jump for the tracer solution
   jump = uuu_s(6,:,1) - uuu_s(6,:,2);

   % seminorm A error
   A = sum(wgsa.*(jump).^2);
   e_A = e_A + A;

 end

end
% --------------------------

% Check for negative error
if e_L2 <= 0
 disp('Negative errors -> e_L2 set to zero');
end
e_L2 = max(e_L2,0.0);
if e_H1 <= 0
 disp('Negative errors -> e_H1 set to zero');
end
e_H1 = max(e_H1,0.0);
if e_A <= 0
 disp('Negative errors -> e_A set to zero');
end
e_A = max(e_A,0.0);

% Square root of the errors
e_L2 = sqrt(e_L2); e_H1 = sqrt(e_H1); e_A = sqrt(e_A);

return
