function lcor = correlation(ui)
% function lcor = correlation(ui)
% 
% This function evaluates the correlation length starting 
% from the solution point-wise interpolated.

 % size of the sample
 nsample = size(ui,2);
 nvar = size(ui,1);
 % compute fluctuations
 for i=1:nvar
   ui_f(i,:) = ui(i,:) - mean(ui(i,:));
 end
 % duplicate the sample
 ui_f = [ui_f,ui_f];
 % compute the correlation
 %ntau = ceil(nsample/2);
 ntau = nsample;
 lcor = zeros(nvar,ntau);
 for tau=0:ntau
   %lcor(:,tau+1) = 0;
   %for it=1:nsample-tau
   %  lcor(:,tau+1) = lcor(:,tau+1) + ui(:,it).*ui(:,it+tau);
   %end
   lcor(:,tau+1) = sum( ui_f(:,1:ntau) .* ui_f(:,1+tau:ntau+tau) ,2) / ...
                   ntau;
   % normalization
   %lcor(:,tau+1) = lcor(:,tau+1) ./ ( sum( ui_f(:,1:ntau).^2 ,2 )/ntau );
 end

return
