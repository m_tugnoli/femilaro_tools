% once errors are evaluated using the functions convergence_order.m
% and errors.m, load the results here and plot.

close all
clear all
set_plot_defaults

load('convergence-results.octxt');
h = [1/2,1/4,1/8];

% L2
figure, hold on
loglog(1./h,eL2(:,1),'-k^')
loglog(1./h,eL2(:,2),'-bo')
loglog(1./h,eL2(:,3),'-rs')
xlim([1.6 10])
xlabel('log(h)')
ylabel('log(e_{L^2})')
legend('k1','k2','k3')
print -depsc2 ch-L2err

% DG
figure, hold on
loglog(1./h,eA(:,1),'-k^')
loglog(1./h,eA(:,2),'-bo')
loglog(1./h,eA(:,3),'-rs')
xlim([1.7 10])
xlabel('log(h)')
ylabel('log(e_{DG})')
legend('k1','k2','k3')
print -depsc2 ch-DGerr
