function [t,ex,ey] = new2d_grid_singlesquare(iverts,orientation)
% This function should be only called from new2d_grid. It computes the
% two triangles used to represent a square. The input parameters are
% the vertices of the square, and a string specifying the desired
% orientation, which must be one of the following: north, south.

 ex = zeros(5,1,2); ey = ex;

 switch orientation
  case 'north'
   tt = [ 1 1;
          3 4;
          4 2 ];
  case 'south'
   tt = [ 1 2;
          3 3;
          2 4 ];
  otherwise
   error(['Wrong orientation "',orientation,'"']);
 endswitch

 t = iverts(tt);
 ex(1:2,:,1) = iverts([1,2]'); ex(5,:,1) = 2;
 ex(1:2,:,2) = iverts([3,4]'); ex(5,:,2) = 4;
 ey(1:2,:,1) = iverts([1,3]'); ey(5,:,1) = 3;
 ey(1:2,:,2) = iverts([2,4]'); ey(5,:,2) = 1;

return

