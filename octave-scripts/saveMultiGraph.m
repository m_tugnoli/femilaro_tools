function saveMultiGraph(filename, varargin)

%not finished, quite usless since all columns need to be of the same length
ncol = nargin-1;
if (mod(ncol,2)~=0)
    error('wrong number of inputs, insert an array and a label for each column' )
end
ncol = ncol/2;
data = varargin(1:2:end-1);
labels = varargin(2:2:end);

arraylen = length(data{1});
fid = fopen(filename, "w");
fprintf(fid,'# ')
for j=1:ncol
    fprintf(fid, ' %s  ', labels{j})
end
fprintf(fid, "\n")

for i=1:arraylen
    for j = 1:ncol
        fprintf(fid, '%g  ', data{j}(i))
    end
    fprintf(fid,'\n')
end

fclose(fid)
