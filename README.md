# README #


This repository is mainly to save, track and mantain personal scripts that I use for pre and post processing simuations performed with FEMilaro (https://bitbucket.org/mrestelli/femilaro/wiki/Home).

At the moment the collection of scripts is quite messy:
-They are often a duplication of scripts already present in FEMilaro repository, updated or tailored to my needs. 
-They are in most cases specific to a single application, a single machine or a single mesh or testcase
-They are poorly documented (to be generous)

I am trying to clean up, but sadly it is not the first priority.
