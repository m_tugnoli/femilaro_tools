#!/bin/bash

#take a file, and a series of files, with a number in the name.
#link all the series after the first one, looking like it is
#a single time history
#
#WARNING: the number positions have been set by hand
# so the script is far from being universal!

declare -i st=-7
declare -i en=-3
#this should work with the standard format textxxxx-0000.h5
echo "appending to history of $1"
beg_n=${1:$st:$en}
beg_n=$((10#$beg_n)) #force decimal
for f in "${@:2}"
do
  num=${f:$st:$en} #extract number
  num=$((10#$num)) #force decimal
  sumnum=$((beg_n + num))
  sumnum=$(printf "%04d" $sumnum)
  newname=${1:0:st}$sumnum${1:(en)}
  ln -s $f $newname
done
