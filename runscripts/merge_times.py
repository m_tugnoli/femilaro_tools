#!/usr/bin/env python3
#merge time histories incoming from different simulations
#first damned version, suitable only for forces, will make it more 
#general later
import sys
import numpy as np
import scipy.io as sio
import h5py

files = sys.argv[1:] #the first element is (should be) the file itself
nfiles = len(sys.argv)-1

print('number of files to merge', nfiles)
print('merging files (in this precise order):')
for ifile in files:
  print(ifile)

i=0
equal = True
maxlen = max([len(x) for x in files])
minlen = max([len(x) for x in files])
while (equal and i <= minlen):
  char = [x[i] for x in files]
  equal = all(x==char[0] for x in char)
  i = i+1
beginind = i-2
i=0
equal = True
while (equal and i <= minlen):
  ind = [len(x)-i-1 for x in files]
  char = [files[j][ind[j]] for j in range(0,nfiles)]
  equal = all(x==char[0] for x in char)
  i = i+1
endind = len(files[0])-i+1

outfilename = files[0][0:beginind+1]+'merged'+files[0][endind:]
print('the output name will be:')
print(outfilename)

tottime = []
for ifile in range(0,nfiles):
  print('opening file',files[ifile])
  fh = h5py.File(files[ifile],'r')
  time = fh.get('time')
  print(time)
  tottime = np.concatenate((tottime,time))
print('total length',tottime.shape)

